from os import system as sys

environments = ["LUAD", "LUSC"] #["Tumour", Background", "Healthy"]
conditions = ["original_stratified50"]
for env in environments:
    for cond in conditions:
        fname = f"CellPhoneDB_{env}_{cond}"
        fl = open(fname+".sh", 'w')
        fl.write("source /homes/sefzaira/.bashrc\n")
        fl.write("cd /nfs/research/gerstung/zaira/lung/scRNA-Seq/PythonPipeline/CellPhoneDB/Results/Cell_types/\n")
        fl.write("conda activate minimal_env\n")
        fl.write("export LC_ALL=C.UTF-8\nexport LANG=C.UTF-8\n")
        fl.write(f"cellphonedb method statistical_analysis {env}_meta_{cond}.txt {env}_counts_{cond}.txt  --project-name={env}_CellPhoneDB_{cond} --threads=64 --threshold=0.3 --debug-seed=42 --counts-data=gene_name\n")

        print('bsub -n 1 -q bigmem -R "rusage[mem=800000]" -M 800000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
        sys('bsub -n 1 -q bigmem -R "rusage[mem=800000]" -M 800000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
