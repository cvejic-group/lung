import os
import numpy as np
import scanpy as sc

from cell2location import run_colocation

GENE_COUNT_THRESH = 800
ENV_LST = ["Healthy", "Background", "Tumour"]

OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/STx_results_filtered{GENE_COUNT_THRESH}_q05/"

if __name__ == "__main__":
    for env in ENV_LST:
        os.mkdir(f"{OUT_DIR}colocation_model_{env}/")
        adata = sc.read_h5ad(f"{OUT_DIR}{env}_sp_cleaned.h5ad")
        res_dict, adata_vis = run_colocation(adata, model_name="CoLocatedGroupsSklearnNMF",
                                             train_args={"n_fact": np.arange(7, 11),
                                                         "sample_name_col": "sample",
                                                         "n_restarts": 5},
                                             export_args={"path": f"{OUT_DIR}colocation_model_{env}/"})
