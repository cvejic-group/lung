import sys
import pandas as pd
import scanpy as sc

SMP_DICT = {"LUAD": ["Patient 5", "Patient 6", "Patient 7", "Patient 9", "Patient 10",
                     "Patient 13", "Patient 14", "Patient 15", "Patient 16", "Patient 21",
                     "Patient 22", "Patient 24", "Patient 25"],
            "LUSC": ["Patient 2", "Patient 3", "Patient 4", "Patient 8", "Patient 11",
                     "Patient 18", "Patient 19", "Patient 20"]}

if __name__ == "__main__":
    adata = sc.read_h5ad(f"/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/10X_Lung_Tumour_Annotated_v2.h5ad")
    a = [col[12:] for col in adata.obs.columns if 'Cell types v' in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"Cell types v{str(max_num)}"
    print(f"=====> Using {col_name} as the latest clustering column.")
    adata.obs.rename(columns={col_name: "leiden_latest"}, inplace=True)
    
    for dset, smplist in SMP_DICT.items():
        adata_dset = adata[adata.obs["patient"].isin(smplist), :]
        gene_exp = pd.DataFrame.sparse.from_spmatrix(adata_dset.X, index=adata_dset.obs.index, columns=adata_dset.var.index)
        gene_exp = pd.concat([(gene_exp > 0), adata_dset.obs["leiden_latest"]], axis=1)
        print(dset, ":", smplist, "\t", gene_exp.shape, file=sys.stderr)
        genes = gene_exp.columns[:-1]
        genes_exp_pct = pd.DataFrame(index=genes)
        for grp in gene_exp["leiden_latest"].unique():
            print(grp, file=sys.stderr)
            gene_exp_sub = gene_exp.loc[gene_exp["leiden_latest"] == grp, genes]
            n_cells_grp = gene_exp_sub.shape[0]
            assert n_cells_grp == (gene_exp["leiden_latest"] == grp).sum()
            genes_exp_pct = pd.concat([genes_exp_pct,
                                       pd.DataFrame(gene_exp_sub.sum(axis=0) / n_cells_grp, columns=[grp])],
                                      axis=1)
        genes_exp_pct.to_csv(f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/scRNA-seq/{dset}_genes_exp_pct_bysubtype.csv")
