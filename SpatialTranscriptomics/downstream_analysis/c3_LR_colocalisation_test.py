import os
import sys
import pandas as pd

from scipy.stats import chi2_contingency
from statsmodels.stats.multitest import multipletests

ABD_FIELD = "means_cell_abundance_w_sf"
ENV_LST = ["Healthy", "Background", "Tumour"]
GENE_COUNT_THRESH = 800
OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/STx_results_{GENE_COUNT_THRESH}/LRres/"
LR_LST = [("CTLA4", "CD86"), ("LGALS9", "HAVCR2"), ("NECTIN2", "TIGIT"),
          ("NECTIN2", "CD226"), ("CD96", "NECTIN1"), ("PDCD1", "CD274"),
          ("HLA-F", "LILRB1"), ("HLA-F", "LILRB2"), ("FLT1", "VEGFA"),
          ("FLT1", "VEGFB"), ("KDR", "VEGFA"), ("NRP1", "VEGFA"),
          ("NRP1", "VEGFB"), ("NRP2", "VEGFA"), ("EGFR", "EREG"),
          ("EGFR", "AREG"), ("EGFR", "HBEGF"), ("EGFR", "MIF"),
          ("CD40", "CD40LG"), ("CD2", "CD58"), ("CD28", "CD86"),
          ("CCL21", "CCR7"), ("TNFRSF13B", "TNFSF13B"), ("TNFRSF13C", "TNFSF13B")]


if __name__ == "__main__":
    # Load metadata
    metadata = pd.read_csv("../../Metadata/spatial_metadata.csv")
    metadata["File"] = metadata["Label"] + "_" + metadata["SpaceRanger"]
    # Load spot num
    spotnum = pd.read_csv(f"{OUT_DIR}LRcolocalisation_SpotNumbers.csv", index_col=0)
    # Test LR colocalisation by patient
    chi_pval = dict()
    for patient in metadata["Patient"].unique():
        if patient.startswith("Patient"):
            normal_lst = metadata["File"].loc[(metadata["Patient"] == patient) & (metadata["Label"] == "Background")].tolist()
            tumour_lst = metadata["File"].loc[(metadata["Patient"] == patient) & (metadata["Label"] != "Background")].tolist()
            chi_pval[patient] = dict()
            for l, r in LR_LST:
                ctab = pd.concat([spotnum.loc[normal_lst, ].sum(axis=0)[[f"{l}_{r}", f"not({l}_{r})"]],
                                  spotnum.loc[tumour_lst, ].sum(axis=0)[[f"{l}_{r}", f"not({l}_{r})"]]],
                                 axis=1)
                try:
                    chi_pval[patient][f"{l}_{r}"] = chi2_contingency(ctab)[1]
                except:
                    chi_pval[patient][f"{l}_{r}"] = None
    pval = pd.DataFrame.from_dict(chi_pval, orient="index").reset_index().melt(value_name="p-value", var_name="L_R", id_vars="index")
    pval["p-adj"] = multipletests(pval["p-value"], method="bonferroni")[1]
    pval.sort_values(["p-adj", "p-value"], inplace=True)
    sum_pval = pd.merge(pval, metadata.loc[metadata["Label"] != "Background", ["Patient", "Label"]].drop_duplicates(),
                        left_on="index", right_on="Patient").drop("index", axis=1)
    sum_pval.to_csv(f"{OUT_DIR}LRcolocalisation_Chi2_TvsB.csv")
    del chi_pval, normal_lst, tumour_lst, pval, sum_pval
    # Test LR colocalisation between LUAD & LUSC
    spotnum_luad = spotnum.loc[[x.startswith("LUAD") for x in spotnum.index], :].sum(axis=0)
    spotnum_lusc = spotnum.loc[[x.startswith("LUSC") for x in spotnum.index], :].sum(axis=0)
    spotnum_cmp = pd.concat([spotnum_luad, spotnum_lusc], axis=1)
    chi_pval2 = dict()
    for l, r in LR_LST:
        ctab = spotnum_cmp.loc[[f"{l}_{r}", f"not({l}_{r})"], :]
        try:
            chi_pval2[f"{l}_{r}"] = chi2_contingency(ctab)[1]
        except:
            chi_pval2[f"{l}_{r}"] = None
    pval2 = pd.DataFrame.from_dict(chi_pval2, orient="index").reset_index().rename(columns={"index": "L_R", 0: "p-value"})
    pval2["p-adj"] = multipletests(pval2["p-value"], method="bonferroni")[1]
    pval2.sort_values(["p-adj", "p-value"], inplace=True)
    pval2.to_csv(f"{OUT_DIR}LRcolocalisation_Chi2_LUADvsLUSC.csv")
