import gc
import sys
import scanpy as sc

IN_DIR = "/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/"
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/scRNA-seq/trajectory_res/"
CTYPE_DIC = {"myeloids": {"Pro-inflammatory macrophages": "#b15928",
                          "Anti-inflammatory alveolar macrophages": "#1f78b4",
                          "Cycling anti-inflammatory alveolar macrophages": "#a6cee3",
                          "Anti-inflammatory macrophages": "#b2df8a",
                          "Cycling anti-inflammatory macrophages": "#33a02c",
                          "STAB1+ anti-inflammatory macrophages": "#e31a1c",
                          "CAMLs": "#fdbf6f", "mo-DC2": "#ff7f00", "Cycling mo-DC2": "#fdbf6f",
                          "Immature myeloid cells": "#6a3d9a", "Odd immature cells": "#cab2d6"},
             "stroma": {"AT2 cells": "#3E54AC", "Cycling AT2 cells": "#93BFCF",
                        "Fibroblasts": "#FCC2FC",
                        "Transitioning epithelial cells": "#BFDB38",
                        "Ciliated epithelial cells": "#CCD5AE",
                        "Atypical epithelial cells": "#7DB9B6",
                        "Cycling epithelial cells": "#617143"}}


def restructure(adata2restruct):
    # Parse latest clustring results and remove previous versions
    a = [col[12:]
         for col in adata2restruct.obs.columns if 'Cell types v' in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"Cell types v{str(max_num)}"
    print(f"=====> Using {col_name} as the latest clustering column.",
          file=sys.stderr)
    adata2restruct.obs.rename(columns={col_name: "latest_annot", "batch": "sample batch"},
                              inplace=True)
    # Only keep relevant columns in observations and variables
    adata2restruct.obs.drop([col
                             for col in adata2restruct.obs.columns
                             if "Cell types" in col],
                            axis=1, inplace=True)
    adata2restruct.obs.drop([col
                             for col in adata2restruct.obs.columns
                             if "leiden" in col],
                            axis=1, inplace=True)
    print(f"Cell number: {adata2restruct.n_obs}", file=sys.stderr)


if __name__ == "__main__":
    env = "Tumour"
    adata_env = sc.read_h5ad(f"{IN_DIR}10X_Lung_Tumour_Annotated_v2.h5ad")
    restructure(adata_env)
    print(adata_env.obs["latest_annot"].unique().tolist(), file=sys.stderr)
    for grp, ctype_dict in CTYPE_DIC.items():
        adata_sub = adata_env[adata_env.obs["latest_annot"].isin(ctype_dict.keys()), :].copy()
        sc.pp.neighbors(adata_sub, n_neighbors=15,
                        n_pcs=adata_sub.obsm["X_pca_harmonize"].shape[1],
                        use_rep="X_pca_harmonize", knn=True,
                        random_state=0, method="umap", metric="euclidean")
        sc.tl.paga(adata_sub, groups="latest_annot")
        adata_sub.write_h5ad(f"{OUT_DIR}{env}_{grp}.h5ad")
        del adata_sub
        gc.collect()
