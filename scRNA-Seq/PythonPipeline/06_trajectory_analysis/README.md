# Trajectory Analysis

This folder contains scripts for trajectory analysis.
The script `a_submit_compute.bsub` organises the 3 scripts `a1_compute_paga.py`, `a2_compute_fle.py` and `a3_plot_pagafle.py` to submit to LSF system.
