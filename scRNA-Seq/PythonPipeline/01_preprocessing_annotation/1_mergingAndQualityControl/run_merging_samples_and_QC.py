import warnings
warnings.simplefilter("ignore")

import sys
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import scrublet as scr
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors
from scipy.sparse import issparse

# IMPORTANT: CHANGE THIS TO THE CLONED FUNCTIONS DIRECTORY CONTAINING THE scRNA_functions CLASS
sys.path.append("/nfs/research1/gerstung/nelson/lung/Functions/")

# Get the bespoke analysis functions
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

# Dictionary of all sample imformation
from sample_metadata_full import sample_metadata

# Global colour settings
myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
        '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
        '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
        '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
        '#307D7E', '#000000', "#DDEFFF", "#000035", "#7B4F4B",
        "#A1C299", "#300018", "#C2FF99", "#0AA6D8", "#013349",
        "#00846F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1",
        "#1E6E00", "#DFFB71", "#868E7E", "#513A01", "#CCAA35"]

myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']

# Define a nice colour map for gene expression
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Inputs and outputs: IMPORTANT CHANGE THESE
folder = "/nfs/research1/gerstung/nelson/data/lung/10X/" # TOP DIRECTORY CONTAINING THE CELLRANGER OUTPUTS, AFTER EMPTY DROPS
outDir = "/nfs/research1/gerstung/nelson/outputs/lung/1_mergingAndQualityControl/" # DIRECTORY TO STORE OUTPUTS

# Return an annotated anndata object, specific to the particular sample/patient/title specified
def add_sample_anndata(patient, title, sample, chromosome_dir, folder_mtx):
    # Instantiate the anndata object
    adata  = fc.load10XSanger(folder+sample, title=title, patient=patient, folder_mtx=folder_mtx, chromosome_dir=chromosome_dir)

    # Annotate with additional observations
    infos = ["sex", "age", "sorting", "smoking history", "cancer stage", "tumour type", "# isolated cells", "# estimated cells", "sangerID"]
    for info in infos:
        adata.obs[info] = sample_metadata[patient][title][info]
    adata.obs["cellranger"]          = "Cellranger 3.1.0"
    adata.obs["genome"]              = "GRCh38 3.0.0"


    # Annotate with additional variables, particularly if we are including chromosome decorations
    adata.var = adata.var[['gene_ids', 'feature_types', 'n_cells', 'chromosome']] if chromosome_dir is not None else adata.var[['gene_ids', 'feature_types', 'n_cells']]
    adata.var.rename(columns=
            {"gene_ids"     : "gene_ids_{0}".format(title),
            "feature_types" : "feature_types_{0}".format(title),
            "n_cells"       : "n_cells_{0}".format(title)}, inplace=True
    )

    if chromosome_dir is not None:
        adata.var.rename(columns=
                {"chromosome"      : "chromosome_{0}".format(title)}, inplace=True
        )

    return adata

def main():
    pdf = mpdf.PdfPages(outDir+"Lung_mergingAndQualityControl.pdf")
    # Each tuple of (patient, title, sample, folder_mtx) corresponds to one collection
    collections = [(i, j, sample_metadata[i][j]['sample'],sample_metadata[i][j]["folder_mtx"]) for i in sample_metadata.keys() for j in sample_metadata[i].keys()]
    print(collections)
    # Loop over all samples and append to the adatas list
    adatas = [add_sample_anndata(patient=col[0], title=col[1], sample=col[2], chromosome_dir=None, folder_mtx=col[3]) for col in collections]

    # Perform the sample merging
    merged = adatas[0].concatenate(adatas[1:], join='outer', index_unique="-") if len(adatas) > 1 else adatas[0]
    del adatas

    # Annotation of the merged object
    new_cols = [col.split("-")[0] for col in merged.var.columns]
    merged.var.columns = new_cols
    merged.X   = np.nan_to_num(merged.X)
    merged.var = merged.var.fillna(value=0)
    merged.obs['patient_sample'] = merged.obs['patient']+' '+merged.obs['sample']

    # Add Ensembl IDs as a variable on the merged object
    ens_columns = [name for name in merged.var.columns.tolist() if name.startswith(('gene_ids')) ]
    ens = merged.var[ens_columns]
    ens = ens.T
    ensIDs = [np.trim_zeros(np.unique(list(ens[idx])))[-1] for idx in ens.columns.tolist()]
    merged.var["Ensembl"] = ensIDs

    # Remove features
    to_delete = ['gene_ids', 'feature_types']
    for dels in to_delete:
        column_name = [name for name in merged.var.columns.tolist() if name.startswith((dels)) ]
        for c in column_name:
            del merged.var[c]

    merged.var["feature_types"] = "Gene Expression"
    merged.obs["exp"] = "10X"
    merged.obs["patient_sample"] = merged.obs["patient_sample"].astype("category")
    merged.obs["patient_sample"].cat.categories

    # Quality control
    # 1. All cell
    samples = len(np.unique(merged.obs["patient_sample"]))
    y = 2
    x = int(samples/2)
    if x*y < samples:
        x+=1

    f, axs = plt.subplots(x,y,figsize=(30,60), squeeze=False)
    sns.set(font_scale=2)
    sns.set_style("white")

    for idx, sample in enumerate(merged.obs["patient_sample"].cat.categories):
        reduced = merged[merged.obs["patient_sample"]==sample]
        sns.distplot(reduced.obs['n_genes_by_counts'], bins=200, kde=False, ax=axs[int(idx/2)][idx%2], color="black")
        axs[int(idx/2)][idx%2].set_xlim(0, 10000)
        axs[int(idx/2)][idx%2].set_title(sample)

    for i in range(x):
        for j in range(y):
            if i*y+j >= samples:
                f.delaxes(axs[i][j])

    plt.tight_layout()
    pdf.savefig()
    plt.close("all")

    f, axs = plt.subplots(x,y,figsize=(30,60), squeeze=False)
    sns.set(font_scale=2)
    sns.set_style("white")

    for idx, sample in enumerate(merged.obs["patient_sample"].cat.categories):
        reduced = merged[merged.obs["patient_sample"]==sample]

        sns.distplot(reduced.obs['total_counts'], bins=200, kde=False, ax=axs[int(idx/2)][idx%2], color="black")
        axs[int(idx/2)][idx%2].set_xlim(0, 100000)
        axs[int(idx/2)][idx%2].set_title(sample)

    for i in range(x):
        for j in range(y):
            if i*y+j >= samples:
                f.delaxes(axs[i][j])

    plt.tight_layout()
    pdf.savefig()
    plt.close("all")

    # 2. Make Violin plot for genes, counts, ribosomal counts, and mitocondrial counts resulting from e.g. mitocondrial RNA leakage
    f, axs = plt.subplots(1,4,figsize=(40,8))
    fc.plotViolinVariableGroup(merged, variable='n_genes_by_counts', group_by="exp", cut=0, width=4, ax=axs[0], pdf=None)
    fc.plotViolinVariableGroup(merged, variable='total_counts',      group_by="exp", cut=0, width=4, ax=axs[1], pdf=None)
    fc.plotViolinVariableGroup(merged, variable='pct_counts_rb',     group_by="exp", cut=0, width=4, ax=axs[2], pdf=None)
    fc.plotViolinVariableGroup(merged, variable='pct_counts_mt',     group_by="exp", cut=0, width=4, ax=axs[3], pdf=None)

    plt.tight_layout()
    pdf.savefig()
    plt.close("all")

    fc.plotScatter(merged, None, x="pct_counts_mt", y="n_genes_by_counts", palette=sns.color_palette("deep"), pdf=pdf)
    fc.plotScatter(merged, "pct_counts_mt", x="total_counts", y="n_genes_by_counts", colormap="viridis", pdf=pdf)

    # 3. Number of cell and Median number of genes before filtering cells
    fc.plotBarPlotFeatures(merged,
        group_by1="patient_sample",
        features=["n_genes_by_counts", "total_counts"],
        show_df=False,
        height=20,
        width=60,
        palette=None,
        pdf=pdf)

    nCells = merged.n_obs
    merged_tmp   = merged.copy()

    min_genes    = 180
    max_genes    = 6000
    min_counts   = 400
    max_counts   = 100000
    max_pctMito  = 20

    # Filter cells according to identified QC thresholds:
    print('Total number of cells: %d'%(merged_tmp.n_obs))

    sc.pp.filter_cells(merged_tmp, min_genes = min_genes)
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after min gene filter: %d, lost %3.2f%% of cells'%(merged_tmp.n_obs, loosing))

    sc.pp.filter_cells(merged_tmp, max_genes = max_genes)
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after max gene filter: %d, lost %3.2f%% of cells'%(merged_tmp.n_obs, loosing))

    sc.pp.filter_cells(merged_tmp, min_counts = min_counts)
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after min count filter: %d, lost %3.2f%% of cells'%(merged_tmp.n_obs, loosing))

    sc.pp.filter_cells(merged_tmp, max_counts = max_counts)
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after max count filter: %d, lost %3.2f%% of cells'%(merged_tmp.n_obs, loosing))

    merged_tmp = merged_tmp[merged_tmp.obs['pct_counts_mt'] < max_pctMito]
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after MT filter: %d, lost %2.f%% of cells'%(merged_tmp.n_obs, loosing))


    sc.pp.filter_genes(merged_tmp, min_cells=10)
    print(" * Initial 10X Object: %d genes across %d single cells"%(merged_tmp.n_vars, merged_tmp.n_obs))

    beforeRemovingS  = merged.obs.index
    afterRemovingS   = merged_tmp.obs.index
    indices          = np.invert(beforeRemovingS.isin(afterRemovingS.tolist()))
    removedCellsS    = beforeRemovingS[indices]


    merged.obs['Removed'] = 'No'
    removed = merged.obs['Removed'][removedCellsS]
    merged.obs['Removed'].replace(removed, "Yes", inplace=True)
    merged.obs['Removed'] = merged.obs.Removed.astype('category')
    fc.plotScatter(merged, "Removed", x="total_counts", y="n_genes_by_counts", palette=["gray", "red"], pdf=pdf)

    f, axs = plt.subplots(1,4,figsize=(40,8))
    sns.set(font_scale=1.2)
    sns.set_style("white")

    # Add violin plot after removal of low gene counts
    fc.plotViolinVariableGroup(merged_tmp, variable='n_genes_by_counts', cut=0, width=4, ax=axs[0], pdf=None)
    fc.plotViolinVariableGroup(merged_tmp, variable='total_counts',      cut=0, width=4, ax=axs[1], pdf=None)
    fc.plotViolinVariableGroup(merged_tmp, variable='pct_counts_rb',     cut=0, width=4, ax=axs[2], pdf=None)
    fc.plotViolinVariableGroup(merged_tmp, variable='pct_counts_mt',     cut=0, width=4, ax=axs[3], pdf=None)

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    pdf.savefig()
    plt.close("all")

    # 4. Removing doublets
    # Doublet removal achieved using scrublet: https://www.cell.com/cell-systems/pdfExtended/S2405-4712(18)30474-5
    merged_tmp = fc.scrublet_doublet_removal_10X(merged_tmp)

    fc.plotScatter(merged_tmp,
               x="total_counts",
               y="n_genes_by_counts",
               variable="predicted_doublets",
               palette=["gray", "red"],
               pdf=pdf)

    merged_tmp = merged_tmp[merged_tmp.obs['predicted_doublets'] != True]
    merged_tmp.obs.drop("predicted_doublets", axis=1, inplace=True)

    # Numbers after applying scrublet
    print(" * 10X Object (after scrublet): %d genes across %d single cells"%(merged_tmp.n_vars,
                                                                         merged_tmp.n_obs))
    # Now reassign to the original merged object
    merged = merged_tmp.copy()
    del merged_tmp

    # Number of cell and Median number of genes after filtering cells
    fc.plotBarPlotFeatures(merged,
        group_by1="patient_sample",
        features=["n_genes_by_counts", "total_counts"],
        show_df=False,
        height=20,
        width=60,
        palette=None,
        pdf=pdf)

    # 5. Batch assignment - each batch corresponds to a "patient" sample
    merged.obs['patient_sample'].value_counts()
    merged.obs['batch'] = 'No'

    for idx, sample in enumerate(merged.obs["patient_sample"].cat.categories):
        cells = merged.obs['batch'][merged.obs['patient_sample'] == sample]
        merged.obs['batch'].replace(cells, idx, inplace=True)

    merged.obs['batch'] = merged.obs['batch'].astype('category')
    merged.obs['batch'].value_counts()
    merged.obs['patient_sample'].value_counts()

    # 6. Add additional sample-specific information
    info_dict = {}

    for idx, sample in enumerate(merged.obs["patient_sample"].cat.categories):
        reduced = merged[merged.obs["patient_sample"]==sample].copy()
        sc.pp.filter_genes(reduced, min_cells=10)

        info_dict[idx] = [sample,
            np.unique(reduced.obs["sorting"])[0],
            np.unique(reduced.obs["# isolated cells"])[0],
            np.unique(reduced.obs["# estimated cells"])[0],
            reduced.n_obs,
            reduced.n_vars,
            int(np.median(reduced.obs["n_genes_by_counts"])),
            int(np.median(reduced.obs["total_counts"]))]

        df = pd.DataFrame.from_dict(info_dict, orient='index')

        df.rename(
            {0:"Sample",
             1:"Sorting strategy",
             2:"# isolated cells",
             3:"# estimated cells",
             4:"# cells after QC",
             5:"# genes after QC",
             6:"Median # genes per cell after QC",
             7:"Median # UMIs per cell after QC"}, axis="columns", inplace=True
        )
        del reduced

    # Only run the lines below if running on DS only
    df["Success rate (%) - Estimated"] = round(df["# estimated cells"]/df["# isolated cells"]*100, 2)
    df["Success rate (%) - after QC"]  = round(df["# cells after QC"]/df["# isolated cells"]*100, 2)
    print("Mean success rate - Estimated = %s%%"%round(np.average(df["Success rate (%) - Estimated"]),2))
    print("Mean success rate - After QC  = %s%%"%round(np.average(df["Success rate (%) - after QC"]),2))

    df.set_index("Sample", inplace=True)
    df.index.name = None

    # Write this to a CSV file. IMPORTANT: THIS STORES VALUABLE QC OUTPUTS
    df.to_csv(outDir+"QC_summary_Lung.csv", sep="\t")

    # 8. Saving layers of the anndata objects

    # Save counts into the layer
    merged.layers["counts"] = np.asarray(merged.X)
    # IMPORTANT: the normalisation step is included here, but generally should be applied
    # at the batch correction stage, where we first split the environments. After splitting environments,
    # apply lines like those below to correctly normalise.
    sc.pp.normalize_per_cell(merged, counts_per_cell_after=1e4)
    sc.pp.log1p(merged)
    sc.pp.scale(merged, max_value=10)

    # 9. Adding cell cycle scoring
    # IMPORTANT: UPDATE THIS PATH TO WHERE YOU STORE THE regev_lab_cell_cycle_genes.txt information
    pathCellCycle = '/nfs/research1/gerstung/nelson/lung/scRNA-Seq/MetaData/Resources/regev_lab_cell_cycle_genes.txt'
    if "10X" in folder:
        fc.CellCycleScoring10x(pathCellCycle, merged)
    else:
        fc.CellCycleScoring(pathCellCycle, merged)
    pd.DataFrame(merged.obs["phase"].value_counts())
    print('Completed cell cycle scring')

    # I would recommend only saving the raw counts if the final data has O(10^6) or more cells, otherwise the
    # final matrix becomes interminably large to process
    merged.X = merged.layers["counts"]
    del merged.layers["counts"]

    # 10.Adding generalised PHASE
    phase_list = ["G1" if i == "G1" else "G2M+S" for i in merged.obs["phase"].tolist()]
    merged.obs["PHASE"] = phase_list
    pd.DataFrame(merged.obs["PHASE"].value_counts()).sort_index()
    print('PHASE added')

    # Close pdf
    pdf.close()
    print('Closing PDF')

    # 11. Save object (need sufficient memory for the saving to work)
    path = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged/10X_Lung.h5ad" # IMPORTANT: UPDATE TO WHERE YOU WANT TO SAVE THE MERGED H5AD OBJECT
    merged.write(path, compression='gzip')
    print('Finished writing. Exciting gracefully.')

if __name__=="__main__":
    main()
