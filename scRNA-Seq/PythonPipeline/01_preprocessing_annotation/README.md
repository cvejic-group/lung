# Preprocessing and Cell Type Annotations

This folder comprises scripts for dataset preprocessing till cell type annotation. The workflow follows the order of subfolders from empty droplet filtration to clustering.

These scripts produce finally two Scanpy objects stored in h5ad format:
- 10X_Lung_Healthy_Background_Annotated_v2.h5ad from healthy and background tissue.
- 10X_Lung_Tumour_Annotated_v2.h5ad containing from tumour tissue.

The final version of cell type annotations are stored in "Cell types v12" for healthy and background and "Cell Types v25" for tumour.
