import sys
import scanpy as sc
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import linkage, dendrogram

ABD_FIELD = "q05_cell_abundance_w_sf"
ENV_LST = ["Healthy", "Background", "Tumour"]
GENE_COUNT_THRESH = 800
METHOD, METRIC = "complete", "correlation"

OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/STx_results_filtered{GENE_COUNT_THRESH}_q05/"


def plot_dendro(df, metric, method, ax, title):
    dist = pdist(df.T, metric=metric)
    dendrogram(linkage(dist, method=method, metric=metric),
               labels=df.columns, orientation="left",
               leaf_font_size=12, ax=ax)
    xlbls = ax.get_ymajorticklabels()
    for lbl in xlbls:
        if lbl.get_text() in ["CAMLs", "AT2 cells", "Anti-inflammatory macrophages"]:
            lbl.set_color("red")
        elif lbl.get_text() in ["Hepatocytes", "Hepatic stellate cells", "Activated stellate cells",
                                "LSECs", "Vascular endothelial cells"]:
            lbl.set_color("blue")
        else:
            lbl.set_color("black")
    ax.spines.right.set_visible(False)
    ax.spines.top.set_visible(False)
    ax.spines.left.set_visible(False)
    ax.spines.bottom.set_visible(True)
    ax.tick_params(left=False, right=False, labelleft=False,
                   labelbottom=True, bottom=True)
    ax.set_title(title, horizontalalignment="left")


if __name__ == "__main__":
    pdf = mpdf.PdfPages(f"{OUT_DIR}celltype_colocalisation.pdf")
    sns.set_theme(font_scale=1.5, style="white")
    _, axes = plt.subplots(1, 3, figsize=(20, 12))
    for i, env in enumerate(ENV_LST):
        # Load cleaned data, transform counts, and harmonisation
        adata = sc.read_h5ad(f"{OUT_DIR}{env}_sp_cleaned.h5ad")
        # Compute cell type percentage
        ctype_abd = adata.obsm[ABD_FIELD].rename(columns={col: col.replace(f"{ABD_FIELD.replace('_cell', 'cell')}_", "")
                                                          for col in adata.obsm[ABD_FIELD].columns})
        ctype_pct_spot = ctype_abd.copy()
        perspot_sum = ctype_pct_spot.sum(axis=1)
        for col in ctype_pct_spot.columns:
            ctype_pct_spot[col] *= (100 / perspot_sum)
        ctype_pct_spot.to_csv(f"{OUT_DIR}celltype_percentage_perspot_{env}.csv")
        # Plot dendrogram for cell type colocalisation
        plot_dendro(ctype_pct_spot, METRIC, METHOD, axes[i], env)
    plt.tight_layout()
    pdf.savefig()
    plt.close()
    pdf.close()
