# Onco-foetal Reprogramming Analysis

This folder contains scripts to analyse onco-foetal reprogramming.

The foetal data was downloaded from [here](https://cellxgene.cziscience.com/collections/2d2e2acd-dade-489f-a2da-6c11aa654028).

The two python scripts are organised and launched to LSF system with `a_combinedfoetalanalysis.bsub`, with supportive information from `celltype_dict.csv`.

- `a1_combine.py` combines our lung Tumour + Background (without Healthy) with the downloaded foetal lung data. Only myeloid cells are considered.
- `a2_pegasus-analysis.py` runs the usual downstream analysis on the combined dataset and plots the figures.
