from os import system as sys

environments = ["Tumour", "Healthy_Background"]
for env in environments:
    fname = f"DataCreation_{env}_stratified50"
    fl = open(fname+".sh", 'w')
    fl.write("source /homes/sefzaira/.bashrc\n")
    fl.write("cd /nfs/research/gerstung/zaira/lung/scRNA-Seq/PythonPipeline/CellPhoneDB/\n") # CHANGE TO TOP DIRECTORY CONTAINING CELLPHONEDB CODE
    fl.write("conda activate minimal_env\n")
    fl.write('python data_creation.py --environment="{0}" --stratified\n'.format(env))
    fl.close()

    print('bsub -n 1 -q bigmem -R "rusage[mem=800000]" -M 800000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
    sys('bsub -n 1 -q bigmem -R "rusage[mem=800000]" -M 800000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
