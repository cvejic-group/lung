from visium_qc_and_visualisation import read_and_qc
import numpy as np
import scanpy as sc
from scipy.spatial import cKDTree
from anndata import AnnData
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf
from matplotlib.colors import LogNorm
import seaborn as sns
from sklearn.preprocessing import normalize

sns.set_style("white")
inDir = "/nfs/research1/gerstung/jp27/AC_LNG/3_spaceranger_output/"
outDir = "/nfs/research1/gerstung/nelson/lung/SpatialTranscriptomics/cell2location/"
model = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/annotation_broad/LocationModelLinearDependentWMultiExperiment_8experiments_9clusters_15107locations_2735genes/sp.h5ad"
model_env = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/environment/LocationModelLinearDependentWMultiExperiment_8experiments_3clusters_15107locations_2735genes/sp.h5ad"
sample_names = [
                'spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A',
                'spaceranger110_count_36209_OTAR_LNGsp9476040_GRCh38-2020-A',
                'spaceranger110_count_36210_OTAR_LNGsp9476042_GRCh38-2020-A',
                'spaceranger110_count_36210_OTAR_LNGsp9476044_GRCh38-2020-A',
                'spaceranger110_count_36209_OTAR_LNGsp9476039_GRCh38-2020-A',
                'spaceranger110_count_36209_OTAR_LNGsp9476041_GRCh38-2020-A',
                'spaceranger110_count_36210_OTAR_LNGsp9476043_GRCh38-2020-A',
                'spaceranger110_count_36210_OTAR_LNGsp9476045_GRCh38-2020-A'
]

slides = [read_and_qc(sample_name, path=inDir) for sample_name in sample_names]
adata_vis = slides[0].concatenate(slides[1:], batch_key="sample", uns_merge="unique", batch_categories=sample_names, index_unique=None)
adata_vis.layers["counts"] = np.asarray(adata_vis.X.toarray())
sc.pp.normalize_per_cell(adata_vis, counts_per_cell_after=1e4)
# Normalised counts for each aligned gene per spot
adata_vis.layers["norm"] = np.asarray(adata_vis.X.toarray()).copy()

# Tuples of relevant ligand-receptors from CellPhoneDB for investigation
ligand_receptor_pairs = [
                            ("SPP1", "FGFR2"), ("CCL5", "CCR5"),
                            ("CCL4", "CCR1"), ("CCL3", "CNR2"),
                            ("CCL3L1", "CD44"), ("HGF", "CCR3"),
                            ("CXCL10", "CD44"), ("CXCL17", "GPR35")
]

def main(slide="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A", nn_distance=300, threshold=0.001):

    adata_slide = adata_vis[adata_vis.obs["sample"]==slide, :]
    all_spots = adata_slide.obsm['spatial']
    tree = cKDTree(all_spots)

    lr_all, lr_spot_all = list(), list()
    for LR in ligand_receptor_pairs:
        adata = adata_slide[:, adata_slide.var["SYMBOL"].isin(LR)]
        lr, lr_spot = list(), list()
        for idx in adata.obs.index:
            spot = adata[adata.obs.index==idx,:].obsm['spatial']
            nn = tree.query_ball_point(spot, nn_distance)[0] # Indices of the nearest neighbours
            nn_spots = adata.obs.index[nn] # This also includes the central spot
            count = 0
            count_raw = 0
            if ((adata[adata.obs.index==idx, adata.var["SYMBOL"]==LR[0]].layers["norm"][0][0] < threshold) and (adata[adata.obs.index==idx, adata.var["SYMBOL"]==LR[1]].layers["norm"][0][0] < threshold)):
                lr.append(0) # No expressed ligand or receptor in the spot, so skip it
            else:
                for nn in nn_spots:
                    # CCI calculation
                    # Check for ligand expression
                    if adata[adata.obs.index==idx, adata.var["SYMBOL"]==LR[0]].layers["norm"][0][0] > threshold:
                        # Scan for receptors
                        if adata[adata.obs.index==nn, adata.var["SYMBOL"]==LR[1]].layers["norm"][0][0] > threshold:
                            count += adata[adata.obs.index==nn, adata.var["SYMBOL"]==LR[1]].layers["norm"][0][0]
                            count_raw += 1

                    # Check for receptor expression
                    if adata[adata.obs.index==idx, adata.var["SYMBOL"]==LR[1]].layers["norm"][0][0] > threshold:
                        # Scan for ligands
                        if adata[adata.obs.index==nn, adata.var["SYMBOL"]==LR[0]].layers["norm"][0][0] > threshold:
                            count += adata[adata.obs.index==nn, adata.var["SYMBOL"]==LR[0]].layers["norm"][0][0]
                            count_raw += 1
                lr.append(count)
            # Save the single L-R expression in each spot
            if ((adata[adata.obs.index==idx, adata.var["SYMBOL"]==LR[0]].layers["norm"][0][0] > threshold) and (adata[adata.obs.index==idx, adata.var["SYMBOL"]==LR[1]].layers["norm"][0][0] > threshold)):
                lr_spot.append(adata[adata.obs.index==nn, adata.var["SYMBOL"]==LR[0]].layers["norm"][0][0] + adata[adata.obs.index==nn, adata.var["SYMBOL"]==LR[1]].layers["norm"][0][0])
        lr_all.append(lr)
        lr_spot_all.append(lr_spot)

    # Arrays for spots and LR pairs
    spots = pd.DataFrame(adata_slide.obs.index.values, columns=["Spots"])
    pairs = pd.DataFrame(["{0}-{1}".format(LR[0], LR[1]) for LR in ligand_receptor_pairs], columns=["Ligand-Receptor"])
    cci_count = np.array(lr_all).T
    cci_count_spot = np.array(lr_spot_all).T
    adata_lr = AnnData(X=cci_count, obs=spots, var=pairs)
    adata_lr.layers["spot"] = cci_count_spot
    # Copy across the extra information
    adata_lr.uns = adata_slide.uns
    adata_lr.obsm['spatial'] = adata_slide.obsm['spatial']

    threshold_name = str(threshold).replace(".", "p")
    adata_lr.write(outDir+"AnnData/updated/"+slide+"_LR_pairs_thresh{0}.h5ad".format(threshold_name), compression="gzip")



if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-s", "--slide", help="Visium slide", type=str, default="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A")
    parser.add_argument("-d", "--nn_distance", help="Distance for nearest neighbours", type=int, default=500)
    parser.add_argument("-t", "--threshold", help="Expression threshold", type=float, default=0.001)

    options = parser.parse_args()
    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)

