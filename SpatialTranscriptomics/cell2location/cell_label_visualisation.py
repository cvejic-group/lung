import sys
import pickle
import scanpy as sc
import anndata
import numpy as np
import os
import cell2location
from cell2location.plt.mapping_video import plot_spatial

import matplotlib as mpl
mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42

from matplotlib import pyplot as plt
import matplotlib.backends.backend_pdf as mpdf
from visium_qc_and_visualisation import read_and_qc, select_slide

# Silence scanpy that prints a lot of warnings
import warnings
warnings.filterwarnings('ignore')

annotations = "annotation_broad"
#visDir = "/nfs/research1/gerstung/jp27/AC_LNG/3_spaceranger_output/"
visDir = "/nfs/research1/gerstung/nelson/data/visium_downsyndrome/second_run/"
#inDir = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/{0}/".format(annotations)
inDir = "/nfs/research1/gerstung/nelson/outputs/downsyndrome/cell2location/DS/cell2location/{0}/".format(annotations)
outDir = "./DS_Healthy/"
#r = {"run_name": "LocationModelLinearDependentWMultiExperiment_8experiments_9clusters_15107locations_2735genes"}
r = {"run_name": "LocationModelLinearDependentWMultiExperiment_4experiments_16clusters_12124locations_2178genes"}
#r = {"run_name": "LocationModelLinearDependentWMultiExperiment_4experiments_12clusters_9030locations_2239genes"}
if annotations=="environment":
    r = {"run_name": "LocationModelLinearDependentWMultiExperiment_8experiments_3clusters_15107locations_2735genes"}

# Make a spatial plot based on the provided cell types and plotting metrics
def spatial_plot(slide, sel_clust, sel_clust_col):
    with mpl.rc_context({'figure.figsize': (15, 15)}):
        fig = plot_spatial(slide.obs[sel_clust_col],
                            labels=sel_clust,
                            coords=slide.obsm['spatial'] * list(slide.uns['spatial'].values())[0]['scalefactors']['tissue_hires_scalef'],
                            show_img=True,
                            img_alpha=0,
                            style='dark_background', # Use fast or dark_background here
                            img=list(slide.uns['spatial'].values())[0]['images']['hires'],
                            circle_diameter=6,
                            colorbar_position='right',
                            max_color_quantile=1.0
                            )
        return fig

def main():
    # Pick up the cell2location model
    sp_data_file = inDir + r['run_name']+'/sp.h5ad'
    adata_vis = anndata.read(sp_data_file)

    # Cell type clusters to visually map to
    #sel_clust = ["Myeloid cells", "B cells", "Dendritic cells", "NK cells", "T cells", "Epithelial cells", "Mast cells"]
    sel_clust = [
            "MEMPs",
            "Erythroid cells",
            "HSC",
            "B cells",
            "Neutrophils",
            #"NK cells",
            "T cells",
    ]
    if annotations=="environment":
        sel_clust = ["Healthy", "Background", "Tumour"]
    sel_clust_col_spot = ['q05_spot_factors' + str(i) for i in sel_clust]
    sel_clust_col_UMI = ['q05_nUMI_factors' + str(i) for i in sel_clust]


    for sample in adata_vis.obs['sample'].unique():
        pdf = mpdf.PdfPages(outDir+"DS_Cell2location_{0}_Mapping_{1}.pdf".format(annotations, sample))
        slide = select_slide(adata_vis, sample)

        fig = spatial_plot(slide, ["Total mRNA counts"], ["total_counts"])
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        fig = spatial_plot(slide, ["Genes per spot"], ["n_genes_by_counts"])
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        # Visualise locations of multiple cell types in one figure using absolute cell density (5% quantile)
        fig = spatial_plot(slide, sel_clust, sel_clust_col_spot)
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        # Now visualise using the estimated mRNA abundance for each cell type
        fig = spatial_plot(slide, sel_clust, sel_clust_col_UMI)
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        # Plot individual cell locations over histology images
        slide.obs[sel_clust] = (slide.obs[sel_clust_col_spot])
        sc.pl.spatial(slide,
                      cmap='magma',
                      color=sel_clust,
                      ncols=3,
                      size=0.8,
                      img_key='hires',
                      alpha_img=0.9,
                      vmin=0,
                      vmax='p99.2'
                     )
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        pdf.close()

if __name__=="__main__":
    main()
