# Cell-to-cell Interaction analysis

This folder comprises scripts analysing cell-to-cell intercations.

The `CellPhoneDB` folder contains scripts for L-R identifications using `CellPhoneDB` software:

- The scripts `data_creation.py` and `data_creation_by_cancer.py` creates input files for `CellPhoneDB` (launched to LSF system by `run_data_creation_by_cancer.py` and `run_data_creation.py`).
- The script `run_CellPhoneDB.py` lauches `CellPhoneDB` computation on LSF clusters.

The `LRdotplots` folder contains scripts to plot gene expression dotplots for identified L-R pairs:

- `a1_plotdots_sharedLR.py` for Fig S4E-F.
- `a2_plotdots_LR.py` for Fig 2D.
