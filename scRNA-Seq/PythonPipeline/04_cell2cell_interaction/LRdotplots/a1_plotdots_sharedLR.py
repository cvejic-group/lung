import gc
import sys
import pandas as pd
import scanpy as sc
import matplotlib.pyplot as plt
import seaborn as sns


CELLTYPES = {"EGFR": ["Anti-inflammatory alveolar macrophages", "Anti-inflammatory macrophages",
                      "STAB1+ anti-inflammatory macrophages", "CAMLs", "Pro-inflammatory macrophages",
                      "mo-DC2", "cDC2", "pDCs", "Fibroblasts", "AT2 cells", "Cycling AT2 cells",
                      "Cycling epithelial cells"],
             "VEGF": ["Anti-inflammatory alveolar macrophages", "Anti-inflammatory macrophages",
                      "STAB1+ anti-inflammatory macrophages", "CAMLs", "Pro-inflammatory macrophages",
                      "mo-DC2", "cDC2", "pDCs", "Mast cells", "AT2 cells", "Cycling AT2 cells",
                      "Ciliated epithelial cells", "Fibroblasts", "Activated adventitial fibroblasts",
                      "Lymphatic endothelial cells"]}
GENES = {"EGFR": ["AREG", "EREG", "HBEGF", "MIF", "EGFR"],
         "VEGF": ["VEGFA", "VEGFB", "FLT1", "KDR", "NRP1", "NRP2"]}
LUAD_SAMPLES = ["Patient 5", "Patient 6", "Patient 7", "Patient 9", "Patient 10", "Patient 13", "Patient 14", "Patient 15", "Patient 16",
                "Patient 21", "Patient 22", "Patient 24", "Patient 25"]
LUSC_SAMPLES = ["Patient 2", "Patient 3", "Patient 4", "Patient 8", "Patient 11", "Patient 18", "Patient 19", "Patient 20"]


def restructure(adata2restruct):
    # Parse latest clustring results and remove previous versions
    a = [col[12:] for col in adata2restruct.obs.columns if 'Cell types v' in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"Cell types v{str(max_num)}"
    print(f"=====> Using {col_name} as the latest clustering column.", file=sys.stderr)
    adata2restruct.obs.rename(columns={col_name: "latest_annot"}, inplace=True)
    # Only keep relevant columns in observations and variables
    adata2restruct.obs = adata2restruct.obs[["patient", "latest_annot"]]
    print(f"Cell number: {adata2restruct.n_obs}", file=sys.stderr)
    del adata2restruct.var, adata2restruct.obsm
    gc.collect()


if __name__ == "__main__":
    adata = sc.read_h5ad("/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/10X_Lung_Tumour_Annotated_v2.h5ad")
    restructure(adata)
    sc.pp.normalize_total(adata, target_sum=1e4)
    sc.pp.log1p(adata)
    for grp in ["EGFR", "VEGF"]:
        adata_sub = adata[adata.obs["latest_annot"].isin(CELLTYPES[grp]) & adata.obs["patient"].isin(LUAD_SAMPLES + LUSC_SAMPLES), :].copy()
        gc.collect()
        adata_sub.obs["latest_annot"] = adata_sub.obs["latest_annot"].cat.reorder_categories(new_categories=CELLTYPES[grp])
        adata_sub.obs["environment"] = "None"
        adata_sub.obs.loc[[x in LUAD_SAMPLES for x in adata_sub.obs["patient"]], "environment"] = "LUAD"
        adata_sub.obs.loc[[x in LUSC_SAMPLES for x in adata_sub.obs["patient"]], "environment"] = "LUSC"
        vmax_both = pd.concat([pd.DataFrame(adata_sub[:, GENES[grp]].X.todense(), columns=GENES[grp], index=adata_sub.obs.index),
                               adata_sub.obs[["latest_annot", "environment"]]],
                              axis=1).groupby(["latest_annot", "environment"]).mean().max().max()
        sns.set_theme(style="white", font_scale=1.5)
        _, axes = plt.subplots(2, 1, figsize=(15, 15))
        sc.pl.dotplot(adata_sub[adata_sub.obs["patient"].isin(LUAD_SAMPLES), :], var_names=GENES[grp], groupby="latest_annot", log=False, figsize=(12, 9), 
                      dot_max=1, vmax=vmax_both, ax=axes[0], show=False, swap_axes=False, cmap=sns.color_palette("light:#D95F02", as_cmap=True), title="LUAD")
        sc.pl.dotplot(adata_sub[adata_sub.obs["patient"].isin(LUSC_SAMPLES), :], var_names=GENES[grp], groupby="latest_annot", log=False, figsize=(12, 9),
                      dot_max=1, vmax=vmax_both, ax=axes[1], show=False, swap_axes=False, cmap=sns.color_palette("light:#1B9E77", as_cmap=True), title="LUSC")
        plt.tight_layout()
        plt.savefig(f"../../../pj_NSCLC/results/scRNA-seq/dots_LUADvsLUSC/dotplots.{grp}.sharedLR.pdf", bbox_inches="tight", facecolor="white")
