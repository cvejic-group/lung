###################################################################
#Script Name    : scVI - scArches reference fetal liver                                                                                     
#Description    : across all cell types
#Args           :
#Author         : Jon Bezney (adapted by Haoliang Xue)
#Email          : jbezney@stanford.edu, hx2@sanger.ac.uk
###################################################################

import os
import gc
import sys
import scanpy as sc
import scarches as sca
import matplotlib.pyplot as plt


WK_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/labeltrans_res/"

MERGE_DICT = {"Anti-inflammatory Mac": ["Anti-inflammatory macrophages", "STAB1+ anti-inflammatory macrophages",
                                        "Cycling anti-inflammatory macrophages", "Anti-inflammatory alveolar macrophages",
                                        "Cycling anti-inflammatory alveolar macrophages"],
              "Pro-inflammatory Mac": ["Pro-inflammatory macrophages"],
              "CAMLs": ["CAMLs"],
              "B cells": ["B cells", "Downregulated B cells", "LYZ+ B cells", "TNF+ B cells", "Plasma B cells",
                          "Immature plasma B cells", "Cycling plasma B cells"],
              "T cells": ["Cycling T cells", "Downregulated T cells", "Exhausted T cells", "Naive T cells", "Gamma delta T cells",
                          "Tregs", "Cycling cytotoxic T cells", "Cytotoxic T cells", "Exhausted cytotoxic T cells",
                          "Cycling exhausted cytotoxic T cells"],
              "DCs": ["cDC2", "Cycling cDC2", "mo-DC2", "Cycling mo-DC2", "pDCs"],
              "NK cells": ["NK cells (higher cytotoxicity)", "NK cells (lower cytotoxicity)", "NKB cells"],
              "Mast cells": ["Mast cells", "Cycling mast cells"],
              "Immature cells": ["Immature myeloid cells", "Odd immature cells"]}
MERGE_DICT = {vi: k for k, v in MERGE_DICT.items() for vi in v}

if __name__ == "__main__":
    ref_name, qry_name = "Tumour", "Healthy_Background"
    print(f"Query {qry_name} on {ref_name} reference...", file=sys.stderr)
    cgrp = "immune"

    out_dir = f"{WK_DIR}TwoLevelQuery_{qry_name}_on_{ref_name}_reference/"
    if not os.path.exists(out_dir):
        print(f"Creating folder: {out_dir}", file=sys.stderr)
        os.mkdir(out_dir)

    # Reference dataset
    adata_ref = sc.read_h5ad(f"{WK_DIR}{ref_name}_{cgrp}.h5ad")
    ref_name = ref_name.replace("Healthy_Background", "H/B").replace("Tumour", "T")
    adata_ref.obs["celltype"] = (adata_ref.obs["latest_annot"].map(MERGE_DICT).astype("str") + f"-{ref_name}").astype("category")
    print(adata_ref.X[adata_ref.X > 5], file=sys.stderr)
    ## Batch correction and model training
    sca.models.SCVI.setup_anndata(adata_ref, batch_key="patient_sample")
    vae = sca.models.SCVI(adata_ref, n_layers=2, encode_covariates=True, 
                          deeply_inject_covariates=False, use_layer_norm="both", use_batch_norm="none")
    vae.train(use_gpu=True)
    ref_path = f"{out_dir}ref_{cgrp}.scVI.mdl/"
    vae.save(ref_path, overwrite=True)
    ## Reference latent
    reference_latent = sc.AnnData(vae.get_latent_representation())
    reference_latent.obs["celltype"] = adata_ref.obs["celltype"].tolist()
    reference_latent.obs["batch"] = adata_ref.obs["patient_sample"].tolist()
    reference_latent.obs["study"] = ref_name
    reference_latent.write(f"{ref_path}ref_latent.h5ad")
    ## Learn and save reference tree model
    tree_ref, mp_ref = sca.classifiers.scHPL.learn_tree(data=reference_latent,
                                                        batch_key="study",
                                                        batch_order=[ref_name],
                                                        cell_type_key="celltype",
                                                        classifier="knn",
                                                        n_neighbors=100,
                                                        dynamic_neighbors=True,
                                                        dimred=True,
                                                        print_conf=False)
    
    # Query dataset
    adata_qry = sc.read_h5ad(f"{WK_DIR}{qry_name}_{cgrp}.h5ad")
    qry_name = qry_name.replace("Healthy_Background", "H/B").replace("Tumour", "T")
    adata_qry.obs["celltype"] = (adata_qry.obs["latest_annot"].map(MERGE_DICT).astype("str") + f"-{qry_name}").astype("category")
    print(adata_qry.X[adata_qry.X > 5], file=sys.stderr)
    ## Load model
    model = sca.models.SCVI.load_query_data(adata_qry, ref_path, freeze_dropout=True)
    model.train(max_epochs=200, plan_kwargs=dict(weight_decay=0.0), use_gpu=True)
    surgery_path = f"{out_dir}surgery_model_{cgrp}/"
    model.save(surgery_path, overwrite=True)
    ## Query latent
    query_latent = sc.AnnData(model.get_latent_representation())
    query_latent.obs["celltype"] = adata_qry.obs["celltype"].tolist()
    query_latent.obs["batch"] = adata_qry.obs["patient_sample"].tolist()
    query_latent.obs["study"] = qry_name
    
    # Predict and compare
    for thres in [0.2, 0.35, 0.5]:
        print(f"Predicting with threshold {thres}...", file=sys.stderr)
        query_pred = sca.classifiers.scHPL.predict_labels(query_latent.X, tree=tree_ref, threshold=thres)
        query_pred[query_pred == "Rejection (dist)"] = "Rejected"
        query_pred[query_pred == "Rejected (RE)"] = "Rejected"
        query_latent.obs[f"pred_celltype-thres{thres}"] = query_pred

    # Save query latent
    query_latent.write(f"{surgery_path}query_latent.h5ad")

    # Plot Heatmap
    sca.classifiers.scHPL.evaluate.heatmap(true_labels=query_latent.obs["celltype"],
                                           pred_labels=query_latent.obs[f"pred_celltype-thres0.2"],
                                           shape=(7, 5), cmap="Purples")
    plt.savefig(f"{out_dir}Heatmap_FirstQuery_HBonT_thres0.2.pdf", facecolor="white", bbox_inches="tight")

    # Clean
    del adata_ref, adata_qry, reference_latent, query_latent
    gc.collect()
