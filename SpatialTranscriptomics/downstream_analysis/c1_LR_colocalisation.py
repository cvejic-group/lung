import os
import gc
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from sklearn.metrics import matthews_corrcoef
from _functions import select_slide

ABD_FIELD = "means_cell_abundance_w_sf"
ENV_LST = ["Healthy", "Background", "Tumour"]
GENE_COUNT_THRESH = 800
WK_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/STx_results_{GENE_COUNT_THRESH}/"
OUT_DIR = f"{WK_DIR}LRres/"

LR_LST = [("CTLA4", "CD86"), ("LGALS9", "HAVCR2"), ("NECTIN2", "TIGIT"),
          ("NECTIN2", "CD226"), ("CD96", "NECTIN1"), ("PDCD1", "CD274"),
          ("HLA-F", "LILRB1"), ("HLA-F", "LILRB2"), ("FLT1", "VEGFA"),
          ("FLT1", "VEGFB"), ("KDR", "VEGFA"), ("NRP1", "VEGFA"),
          ("NRP1", "VEGFB"), ("NRP2", "VEGFA"), ("EGFR", "EREG"),
          ("EGFR", "AREG"), ("EGFR", "HBEGF"), ("EGFR", "MIF"),
          ("CD40", "CD40LG"), ("CD2", "CD58"), ("CD28", "CD86"),
          ("CCL21", "CCR7"), ("TNFRSF13B", "TNFSF13B"), ("TNFRSF13C", "TNFSF13B")]
GENE_LST = list({g for lr in LR_LST for g in lr})


if __name__ == "__main__":
    # Make label dictionary from metadata
    metadata = pd.read_csv("../../Metadata/spatial_metadata.csv")
    label_dict = metadata.set_index("SpaceRanger")["Label"]
    # Initialisation
    mdn_gexp = dict()
    lr_coloc = dict()
    lr_mcc = dict()
    # Analyse by environment
    for env in ENV_LST:
        print(env)
        ## Load data
        adata_env = sc.read_h5ad(f"{WK_DIR}{env}_sp_cleaned.h5ad")
        adata_env.obs = adata_env.obs[["in_tissue", "array_row", "array_col", "sample"]]
        ## Compute binarisation threshold
        for sec in adata_env.obs["sample"].unique():
            adata_sec = select_slide(adata_env, sec)
            ### Initialise for section-wise evaluation
            mdn_gexp[f"{label_dict[sec]}_{sec}"] = dict()
            lr_coloc[f"{label_dict[sec]}_{sec}"] = dict()
            lr_mcc[f"{label_dict[sec]}_{sec}"] = dict()
            ### Compute gene presence
            adata_sec.obs = pd.concat([adata_sec.obs,
                                       pd.DataFrame(None, index=adata_sec.obs.index,
                                                    columns=[f"{g}_{t}" for t in ["expr", "pres"] for g in GENE_LST])],
                                      axis=1)
            for g in GENE_LST:
                adata_sec.obs[f"{g}_expr"] = adata_sec[:, g].X.todense().flatten().tolist()[0]
                gexp_mdn = adata_sec.obs[f"{g}_expr"].median()
                adata_sec.obs[f"{g}_pres"] = (adata_sec.obs[f"{g}_expr"] > gexp_mdn)
                mdn_gexp[f"{label_dict[sec]}_{sec}"][g] = gexp_mdn
            ### Compute LR colocalisation
            adata_sec.obs = pd.concat([adata_sec.obs,
                                       pd.DataFrame(None, index=adata_sec.obs.index,
                                                    columns=[f"{l}_{r} status" for l, r in LR_LST])],
                                      axis=1)
            for l, r in LR_LST:
                adata_sec.obs[f"{l}_{r} status"] = adata_sec.obs[f"{l}_pres"].astype("str") + "_" + adata_sec.obs[f"{r}_pres"].astype("str")
                lr_coloc[f"{label_dict[sec]}_{sec}"][f"{l}_{r}"] = (adata_sec.obs[f"{l}_{r} status"] == "True_True").sum()
                lr_coloc[f"{label_dict[sec]}_{sec}"][f"not({l}_{r})"] = adata_sec.n_obs - (adata_sec.obs[f"{l}_{r} status"] == "True_True").sum()
                lr_mcc[f"{label_dict[sec]}_{sec}"][f"{l}_{r}"] = matthews_corrcoef(adata_sec.obs[f"{l}_pres"], adata_sec.obs[f"{r}_pres"])
            ### Write section-wise anndata and free RAM usage
            adata_sec.write_h5ad(f"{OUT_DIR}obj_by_smp/{label_dict[sec]}_{sec}.h5ad")
            del adata_sec
            gc.collect()
    pd.DataFrame.from_dict(lr_coloc, orient="index").to_csv(f"{OUT_DIR}LRcolocalisation_SpotNumbers.csv")
    pd.DataFrame.from_dict(lr_mcc, orient="index").to_csv(f"{OUT_DIR}LRcolocalisation_MCC.csv")
    pd.DataFrame.from_dict(mdn_gexp, orient="index").to_csv(f"{OUT_DIR}LRcolocalisation_BinarisationThresholds.csv")
