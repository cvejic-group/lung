import gc
import pegasus as pg

IN_DIR = "/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/"
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/scRNA-seq/trajectory_res/"
CTYPE_DIC = {"myeloids": {"Pro-inflammatory macrophages": "#b15928",
                          "Anti-inflammatory alveolar macrophages": "#1f78b4",
                          "Cycling anti-inflammatory alveolar macrophages": "#a6cee3",
                          "Anti-inflammatory macrophages": "#b2df8a",
                          "Cycling anti-inflammatory macrophages": "#33a02c",
                          "STAB1+ anti-inflammatory macrophages": "#e31a1c",
                          "CAMLs": "#fdbf6f", "mo-DC2": "#ff7f00", "Cycling mo-DC2": "#fdbf6f",
                          "Immature myeloid cells": "#6a3d9a", "Odd immature cells": "#cab2d6"},
             "stroma": {"AT2 cells": "#3E54AC", "Cycling AT2 cells": "#93BFCF",
                        "Fibroblasts": "#FCC2FC",
                        "Transitioning epithelial cells": "#BFDB38",
                        "Ciliated epithelial cells": "#CCD5AE",
                        "Atypical epithelial cells": "#7DB9B6",
                        "Cycling epithelial cells": "#617143"}}


if __name__ == "__main__":
    env = "Tumour"
    for grp, ctype_dict in CTYPE_DIC.items():
        adata_env = pg.read_input(f"{OUT_DIR}{env}_{grp}.h5ad")
        pg.neighbors(adata_env, K=15, rep="pca_harmonize", random_state=0,
                     n_comps=adata_env.obsm["X_pca_harmonize"].shape[1])
        pg.diffmap(adata_env, n_components=15,
                   rep="pca_harmonize", random_state=0)
        pg.fle(adata_env, rep="diffmap", K=15,
               random_state=25, target_steps=8000)
        pg.write_output(adata_env, f"{OUT_DIR}{env}_{grp}.fle.h5ad")
        del adata_env
        gc.collect()
