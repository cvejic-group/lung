import gc
import pandas as pd
import scanpy as sc

if __name__ == "__main__":
    for dataset in ["Tumour", "Healthy_Background"]:
        adata = sc.read_h5ad(f"/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/data/lung/scRNAseq/outputs/10X_Lung_{dataset}_Annotated_v2.h5ad")
        a = [col[12:] for col in adata.obs.columns if 'Cell types v' in col]
        max_num = max([int(x) for x in a if x.isdigit()])
        col_name = f"Cell types v{str(max_num)}"
        print(f"=====> Using {col_name} as the latest clustering column.")
        adata.obs.rename(columns={col_name: "leiden_latest"},
                         inplace=True)
        gene_exp = pd.DataFrame.sparse.from_spmatrix(adata.X, index=adata.obs.index, columns=adata.var.index)
        gene_exp = pd.concat([(gene_exp > 0), adata.obs["leiden_latest"]], axis=1)
        del adata
        gc.collect()
        genes = gene_exp.columns[:-1]
        genes_exp_pct = pd.DataFrame(index=gene_exp.columns[:-1])
        for grp in gene_exp["leiden_latest"].unique():
            gene_exp_sub = gene_exp.loc[gene_exp["leiden_latest"] == grp, genes]
            n_cells_grp = gene_exp_sub.shape[0]
            print(grp, n_cells_grp == (gene_exp["leiden_latest"] == grp).sum())
            genes_exp_pct = pd.concat([genes_exp_pct,
                                       pd.DataFrame(gene_exp_sub.sum(axis=0) / n_cells_grp,
                                                    columns=[grp])],
                                      axis=1)
        genes_exp_pct.to_csv(f"/lustre/scratch119/casm/team-cvejic/haoliang/results/NSCLC/scRNA-seq/{dataset}_genes_exp_pct.csv")
