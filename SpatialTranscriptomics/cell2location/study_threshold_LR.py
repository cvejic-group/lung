import sys
import scanpy as sc
import anndata
import pandas as pd
import numpy as np
import os
import gc

import cell2location
import scvi
import torch
import matplotlib as mpl
from matplotlib import rcParams
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf
from scipy.spatial import cKDTree

def main(environment="Tumour", slide="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A", threshold_pct=None, threshold=1.0, cell_type_1="", cell_type_2=""):
    #out_dir = "/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/outputs/lung/cell2location/spatial_model/{}/binary_model/".format(environment)
    #csv_dir = "/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/outputs/lung/cell2location/spatial_model/{}/csvs/updated/algo_check/".format(environment)
    csv_dir = "/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/outputs/lung/cell2location/spatial_model/{}/LR_test/".format(environment)
    adata = sc.read_h5ad("/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/outputs/lung/cell2location/spatial_model/{}/sp.h5ad".format(environment))
    #cell_types = adata_model.uns['mod']['factor_names']
    cell_types = [cell_type_1, cell_type_2]
    # Let's pick up the cell types and convert abundances to cell type proportions
    adata_slide = adata[adata.obs["sample"]==slide, :]
    adata_slide.obs["counter"] = [i for i in range(adata_slide.n_obs)]
    adata_slide.obs.sort_values(by=["array_col", "array_row"], inplace=True) # Order the Visium spots from bottom left to top right
    updated_coords = adata_slide.obsm["spatial"][adata_slide.obs["counter"].tolist(), :]
    spatial_dict = {adata_slide.obs_names[i]:updated_coords[i,:] for i in range(len(adata_slide.obs_names))}
    thresh_name = str(threshold).replace(".","_")

    for ct in cell_types:
        adata_slide.obs[f"Mean/Std abundance {ct}"] = adata_slide.obsm["means_cell_abundance_w_sf"]["meanscell_abundance_w_sf_" + ct]/adata_slide.obsm["stds_cell_abundance_w_sf"]["stdscell_abundance_w_sf_" + ct]
    # Save binarized cells to a dictionary
    binarized_dict = dict()
    for idx in adata_slide.obs.index:
        #thresholds = {ct: np.percentile(adata_slide.obsm["q05_cell_abundance_w_sf"]["q05cell_abundance_w_sf_" + ct].values, threshold_pct) for ct in cell_types}
        #thresholds = {ct: np.max(adata_slide.obsm["q05_cell_abundance_w_sf"]["q05cell_abundance_w_sf_" + ct].values)*threshold_pct/100 for ct in cell_types}
        thresholds = {ct: np.percentile(adata_slide.obsm["q05_cell_abundance_w_sf"]["q05cell_abundance_w_sf_" + ct].values, 95)*threshold_pct/100 for ct in cell_types}
        binarized_dict[idx] = {ct: 1 if adata_slide[idx,:].obsm["q05_cell_abundance_w_sf"]["q05cell_abundance_w_sf_" + ct].values[0] > thresholds[ct] else 0 for ct in cell_types}
    for ct in cell_types:
        abundances = adata_slide.obsm["q05_cell_abundance_w_sf"]["q05cell_abundance_w_sf_" + ct].values
        upper = abundances > thresholds[ct]
        lower = abundances <= thresholds[ct]
        abundances[upper] = 1
        abundances[lower] = 0
        adata_slide.obs[f"{ct} in spot?"] = abundances

    cell_type_1_name = cell_type_1.replace(" ", "_")
    cell_type_2_name = cell_type_2.replace(" ", "_")
    adata_slide.obs[[f"{cell_type_1} in spot?", f"{cell_type_2} in spot?"]].to_csv(csv_dir+"{0}_{1}_interactions_{2}.csv".format(cell_type_1_name, cell_type_2_name, slide))
    adata_slide.write(csv_dir+"{0}_{1}_interactions_{2}.h5ad".format(cell_type_1_name, cell_type_2_name, slide), compression="gzip")
    thresh_name = str(threshold_pct).replace(".","_")
    all_spots = adata_slide.obsm["spatial"]
    tree = cKDTree(all_spots)
    interactions_list = []

    double_counted_spots = []

    # Here we now need to convert the two abundances into matrices, then run the
    # searching and double-counting.
    cell_1 = np.empty((np.max(adata_slide.obs["array_row"]), np.max(adata_slide.obs["array_col"])))
    cell_1[:] = -1
    values_1 = {(adata_slide.obs["array_row"].values[i], adata_slide.obs["array_col"].values[i]): adata_slide.obs[cell_type_1+" in spot?"].values[i] for i in range(adata_slide.n_obs)}
    for key, value in values_1.items():
        cell_1[key[0]-1, key[1]-1] = value
    cell_2 = np.empty((np.max(adata_slide.obs["array_row"]), np.max(adata_slide.obs["array_col"])))
    cell_2[:] = -1
    values_2 = {(adata_slide.obs["array_row"].values[i], adata_slide.obs["array_col"].values[i]): adata_slide.obs[cell_type_2+" in spot?"].values[i] for i in range(adata_slide.n_obs)}
    for key, value in values_2.items():
        cell_2[key[0]-1, key[1]-1] = value

    pair_1_2 = pd.DataFrame(columns=["Coord_1", "Coord_2"])
    for j in range(cell_1.shape[1]-2):
        for i in range(cell_1.shape[0]-2):
            if cell_1[i+1, j+1]==1:
                spot_1 = cell_2[i,j]
                spot_2 = cell_2[i+1, j]
                spot_3 = cell_2[i, j+1]
                spot_4 = cell_2[i+2, j+1]
                spot_5 = cell_2[i, j+2]
                spot_6 = cell_2[i+1, j+2]
                spot_7 = cell_2[i+1, j+1]

                if spot_1==1:
                    pair_1_2 = pair_1_2.append({"Coord_1": str(i+1)+"_"+str(j+1), "Coord_2": str(i)+"_"+str(j)}, ignore_index=True)

                if spot_2==1:
                    pair_1_2 = pair_1_2.append({"Coord_1": str(i+1)+"_"+str(j+1), "Coord_2": str(i+1)+"_"+str(j)}, ignore_index=True)

                if spot_3==1:
                    pair_1_2 = pair_1_2.append({"Coord_1": str(i+1)+"_"+str(j+1), "Coord_2": str(i)+"_"+str(j+1)}, ignore_index=True)

                if spot_4==1:
                    pair_1_2 = pair_1_2.append({"Coord_1": str(i+1)+"_"+str(j+1), "Coord_2": str(i+2)+"_"+str(j+1)}, ignore_index=True)

                if spot_5==1:
                    pair_1_2 = pair_1_2.append({"Coord_1": str(i+1)+"_"+str(j+1), "Coord_2": str(i)+"_"+str(j+2)}, ignore_index=True)

                if spot_6==1:
                    pair_1_2 = pair_1_2.append({"Coord_1": str(i+1)+"_"+str(j+1), "Coord_2": str(i+1)+"_"+str(j+2)}, ignore_index=True)

                if spot_7==1:
                    pair_1_2 = pair_1_2.append({"Coord_1": str(i+1)+"_"+str(j+1), "Coord_2": str(i+1)+"_"+str(j+1)}, ignore_index=True)

    # Double-count correction.
    # Adds temporary column merging the two coordinates after ordering them from min to max
    double_counted_pairs = []
    for i in range(len(pair_1_2)):
        if int(pair_1_2["Coord_1"].values[i].split("_")[0]) < int(pair_1_2["Coord_2"].values[i].split("_")[0]):
            double_counted_pairs.append(pair_1_2["Coord_1"].values[i] + pair_1_2["Coord_2"].values[i])
        elif int(pair_1_2["Coord_1"].values[i].split("_")[0]) == int(pair_1_2["Coord_2"].values[i].split("_")[0]):
            if int(pair_1_2["Coord_1"].values[i].split("_")[1]) <= int(pair_1_2["Coord_2"].values[i].split("_")[1]):
                double_counted_pairs.append(pair_1_2["Coord_1"].values[i] + pair_1_2["Coord_2"].values[i])
            else:
                double_counted_pairs.append(pair_1_2["Coord_2"].values[i] + pair_1_2["Coord_2"].values[i])
        else:
            double_counted_pairs.append(pair_1_2["Coord_2"].values[i] + pair_1_2["Coord_1"].values[i])
    pair_1_2["Temp"] = double_counted_pairs

    pair_1_2.to_csv(csv_dir+"{0}_{1}_interactions_beforeDC.csv".format(cell_type_1_name, cell_type_2_name))
    pair_1_2.drop_duplicates(subset=["Temp"], inplace=True)
    pair_1_2.to_csv(csv_dir+"{0}_{1}_interactions_afterDC.csv".format(cell_type_1_name, cell_type_2_name))
    '''
    for idx in adata_slide.obs.index:
        nn_distance=300
        #spot = adata_slide[adata_slide.obs.index==idx,:].obsm["spatial"] # Convert idx to central spot coordinates
        spot = [spatial_dict[idx]] # Loop over the spots in the correct order
        nn = tree.query_ball_point(spot, nn_distance)[0] # Indices of the nearest neighbours
        nn_spots = adata_slide.obs.index[nn] # This also includes the central spot
        nn_spots = [n for n in nn_spots if n not in double_counted_spots] # Remove double counted spots
        # For each spot and surrounding spots:
        #   -- Calculate the binary cell type
        #   -- For a given cell type pair, ask the number of interactions between
        #      the central spot and the surrounding spots
        interactions = 0
        if binarized_dict[idx][cell_type_1] != 0:
            for n in nn_spots:
                interactions += binarized_dict[n][cell_type_2]
        interactions_list.append(interactions)
        # Now we add the central spot so that it is not double-counted
        double_counted_spots.append(idx)
    adata_slide.obs[f"{cell_type_1} - {cell_type_2} interactions"] = interactions_list

    cell_type_1_name = cell_type_1.replace(" ", "_")
    cell_type_2_name = cell_type_2.replace(" ", "_")
    # Let's save the interactions to a CSV, to make a bar plot from later
    adata_slide.obs[f"{cell_type_1} - {cell_type_2} interactions"].to_csv(csv_dir+"{0}_{1}_interactions_{2}_{3}.csv".format(cell_type_1_name, cell_type_2_name, slide, thresh_name))

    #import json
    #with open(csv_dir+"binarized_dict_{0}_{1}_interactions_{2}_{3}.json".format(cell_type_1_name, cell_type_2_name, slide, thresh_name), "w") as fp:
    #    json.dump(binarized_dict, fp)

    pdf = mpdf.PdfPages(csv_dir+"{0}_{1}_interactions_{2}_{3}.pdf".format(cell_type_1_name, cell_type_2_name, slide, thresh_name))
    with mpl.rc_context({'figure.figsize': [6,7],'axes.facecolor': 'white'}):
        sc.pl.spatial(adata_slide,
                        img_key="hires",
                        color=[f"{cell_type_1} - {cell_type_2} interactions"],
                        library_id=list(adata_slide.uns['spatial'].keys())[0],
                        alpha_img=0,
                        size=1,
                        vmin=0,
                        cmap='magma',
                        show=False,
                        return_fig=True)
        plt.tight_layout()
        pdf.savefig()
        plt.close()
    pdf.close()
    '''
    types = [f"{ct} in spot?" for ct in cell_types]
    pdf = mpdf.PdfPages(csv_dir+"{0}_{1}_binary_{2}_{3}.pdf".format(cell_type_1_name, cell_type_2_name, slide, thresh_name))
    with mpl.rc_context({'figure.figsize': [6,7],'axes.facecolor': 'white'}):
        sc.pl.spatial(adata_slide,
                        img_key="hires",
                        color=types,
                        library_id=list(adata_slide.uns['spatial'].keys())[0],
                        alpha_img=0,
                        size=1,
                        vmin=0,
                        cmap='magma',
                        show=False,
                        return_fig=True)
        plt.tight_layout()
        pdf.savefig()
        plt.close()
    pdf.close()

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-e", "--environment", type=str, help="Tumour, Background, Healthy",default="Tumour")
    parser.add_argument("-s", "--slide", type=str, help="Slide",default="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A")
    parser.add_argument("-ct1", "--cell_type_1", type=str, help="Cell type 1",default="")
    parser.add_argument("-ct2", "--cell_type_2", type=str, help="Cell type 2",default="")
    parser.add_argument("-t", "--threshold", type=float, help="Abundance threshold",default=1.0)
    parser.add_argument("-tp", "--threshold_pct", type=float, help="Abundance threshold pct",default=None)

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
