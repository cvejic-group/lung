# Spatial Transcriptomics analysis

The spatial transcriptomics analysis uses scRNA-seq and 10X Visium data to map and analyse the cell types on
tissue. A major motivation for considering the spatial phenotyping of cells is to predict and calculate the 
different cell-cell interactions. Statistically significant cell-cell interactions can be inferred by investigating 
several quantities:

1) The cell type abundances, as predicted by e.g. cell2location (https://cell2location.readthedocs.io/en/latest/).
2) Statistically significant ligand-receptor pairs, and associated cell types, from CellPhoneDB (see scRNA-seq analysis code).
3) The spatial correlation between cell types and co-expressed genes (e.g. ligand-receptor pairs), evaluated using on of the
many spatial statistics measures e.g. Moran's I (https://en.wikipedia.org/wiki/Moran%27s_I).
4) The co-location of different cell types, calculated by factorising the learned Visium spot - Cell abudances matrix, using e.g.
non-negative matrix factorisation (https://en.wikipedia.org/wiki/Non-negative_matrix_factorization).

The necessary conda dependences can be installed by carefully following the instructions outlined in https://github.com/BayraktarLab/cell2location#installation.
## 1) Cell type mapping with cell2location
The cell2location procedure is a hierarchical Bayesian model which learms posterior probabilities of cell types per Visium spot,
which has a capture area of typically 5-20 cells. There are three stages to this procedure.

### a) Learn gene expression posterior probabilities with negative Binomial regression
This is implemented by calling
```
python estimate_expression_signatures.py --environment=<specific_environment>
```
where the ```Tumour```, ```Healthy```, or ```Background``` environment is specified.

### b) Use the learned gene expression profiles, combined with 10X Visium, to map cell types
This is implemented by calling
```
python run_cell2location.py --environment=<specific_environment>
```
where the ```Tumour```, ```Healthy```, or ```Background``` environment is specified.

### c) Call non-negative matrix factorisation, calculate cell type co-location, and save cell type Visium maps
This is implemented by calling
```
python run_cell_abundances.py --environment=<specific_environment>
```
where the ```Tumour```, ```Healthy```, or ```Background``` environment is specified.

## 2) Ligand-receptor spatial analysis
These correspond to series of additional scripts. Please note that code development is ongoing here and will be continuously 
cleaned and developed on. This README will be updated continuously. For now I summarise the current analyses:

```cell_type_LR_plotting.py```: plots the average ligand-receptor coexpression for different cell types, as a heatmap.

```plot_CCI_vs_cell_proportion.py```: plots the cell-cell interaction measure for ligand-receptor pairs against the cell abundances, allowing
correlation to be quantified. 

```spatial_autocorrelation_pysal.py```: uses the Moran's in both local and global format to calculate the spatial correlation between different
ligand-receptor pairs on the Visium slide. 

```visium_qc_and_visualisation.py```: generic script for plotting gene expressions on Visium, and visualising cell abundances. 

```cell_type_LR_matching.py```: matches cell types to ligand-receptor pairs.

```LR_construct_null.py```: builds a randomised ligand-receptor coexpression null hypothesis distribution, to be used in the Moran's I statistical
test on spatial correlations. 

```plot_interactions_bars.py```: script to plot cell type interactions as a bar chart. Under development. 	  

```study_threshold_LR.py```: script to plot the ligand-receptor and cell type abundances as a binary distribution. Under development. 
