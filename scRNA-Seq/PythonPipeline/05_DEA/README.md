# Differential Expression Analysis with Pseudo-bulking

The Jupyter Notebook `5_Pseudobulk_DE_Analysis.ipynb` contains workflow for DEA with pseudo-bulking.

The comparison was made between Background and Tumour cells excluding Healthy donors. In addition, two patients `Patient 1` and `Patient 4` were excluded too, due to their unclear status at the time of analysis.

Each annotated cell type is manually selected one by one to analyse. In the script the example is `NK cells (lower cytotoxicity)`. 
