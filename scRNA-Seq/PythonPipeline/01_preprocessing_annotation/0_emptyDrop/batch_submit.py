from os import system as sys

codeDir = '/nfs/research/gerstung/nelson/lung/scRNA-Seq/PythonPipeline/0_emptyDrop/' # CHANGE ME TO THE DIRECTORY WHERE run_EmptyDrop.R IS CLONED

# All CellRanger samples for the lung data
samples = [
    'cellranger310_count_27007_Myeloid7648476_GRCh38-3_0_0',     'cellranger310_count_36610_OT10xRNA9505056_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_B0648_B_GRCh38-3_0_0',
    'cellranger310_count_27007_Myeloid7648477_GRCh38-3_0_0',     'cellranger310_count_36610_OT10xRNA9505057_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_B210086B_GRCh38-3_0_0',
    'cellranger310_count_32143_OT10xRNA8615182_GRCh38-3_0_0',    'cellranger310_count_36610_OT10xRNA9505058_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_B210086D_GRCh38-3_0_0',
    'cellranger310_count_32349_OT10xRNA8615190_GRCh38-3_0_0',    'cellranger310_count_36610_OT10xRNA9505059_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_B21063B_GRCh38-3_0_0',
    'cellranger310_count_32349_OT10xRNA8626581_GRCh38-3_0_0',    'cellranger310_count_36610_OT10xRNA9505060_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_B21063D_GRCh38-3_0_0',
    'cellranger310_count_32349_OT10xRNA8626589_GRCh38-3_0_0',    'cellranger310_count_36610_OT10xRNA9505061_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_T0026_C_GRCh38-3_0_0',
    'cellranger310_count_32771_scATAC-seq8620774_GRCh38-3_0_0',  'cellranger310_count_37328_OT10xRNA9505163_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_T0026E_GRCh38-3_0_0',
    'cellranger310_count_32771_scATAC-seq8620782_GRCh38-3_0_0',  'cellranger310_count_37328_OT10xRNA9505164_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_T0648_A_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8730834_GRCh38-3_0_0',    'cellranger310_count_37328_OT10xRNA9505165_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_T210086A_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8730842_GRCh38-3_0_0',    'cellranger310_count_37328_OT10xRNA9505166_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_T210086C_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8757840_GRCh38-3_0_0',    'cellranger310_count_37328_OT10xRNA9505167_GRCh38-3_0_0',  'cellranger310_count_H3FGTDSX2_T21063A_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8757848_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_B21_0012_B_GRCh38-3_0_0',   'cellranger310_count_H3FGTDSX2_T21063C_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8768111_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_B21_0012_F_GRCh38-3_0_0',   'cellranger310_count_HCCVYDSX2_B21106B_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8768119_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_B2106_A_GRCh38-3_0_0',      'cellranger310_count_HCCVYDSX2_B21106D_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8768127_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_B2108_E_GRCh38-3_0_0',      'cellranger310_count_HCCVYDSX2_T21106A_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8768135_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_B2108_G_GRCh38-3_0_0',      'cellranger310_count_HCCVYDSX2_T21106C_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8768399_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_L591_D_GRCh38-3_0_0',   'cellranger310_count_HVJYLDMXX_SIGAA7_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8768407_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_L591_H_GRCh38-3_0_0',   'cellranger310_count_HVJYLDMXX_SIGAB7_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8782629_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_L591_J_GRCh38-3_0_0',   'cellranger310_count_HVJYLDMXX_SIGAG8_GRCh38-3_0_0',
    'cellranger310_count_33701_OT10xRNA8782637_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_T21_0012_A_GRCh38-3_0_0',   'cellranger310_count_HVJYLDMXX_SIGAH8_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469340_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_T21_0012_E_GRCh38-3_0_0',   'cellranger310_count_SIGAA7_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469341_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_T2106B_B_GRCh38-3_0_0',     'cellranger310_count_SIGAB7_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469342_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_T2108_D_GRCh38-3_0_0',      'cellranger310_count_SIGAC7_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469343_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_T2108_F_GRCh38-3_0_0',      'cellranger310_count_SIGAD10_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469344_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_T2108_H_GRCh38-3_0_0',      'cellranger310_count_SIGAD7_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469356_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_U591_C_GRCh38-3_0_0',   'cellranger310_count_SIGAE10_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469357_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_U591_G_GRCh38-3_0_0',   'cellranger310_count_SIGAE12_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469358_GRCh38-3_0_0',    'cellranger310_count_H2NVKDSX2_U591_I_GRCh38-3_0_0',   'cellranger310_count_SIGAF12_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469359_GRCh38-3_0_0',    'cellranger310_count_H3FGTDSX2_B0026_D_GRCh38-3_0_0',      'cellranger310_count_SIGAG11_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469360_GRCh38-3_0_0',    'cellranger310_count_H3FGTDSX2_B0026_F_GRCh38-3_0_0',      'cellranger310_count_SIGAH11_GRCh38-3_0_0',
    'cellranger310_count_35975_OT10xRNA9469361_GRCh38-3_0_0',    'cellranger310_count_H3FGTDSX2_B0026_H_GRCh38-3_0_0',
]

for sample in samples:
    fname = "EmptyDrop_"+sample+".sh"
    f = open(fname, 'w')
    f.write("cd /nfs/research1/gerstung/nelson/outputs/lung/0_emptyDrop\n") # CHANGE ME TO A PLACE YOU WOULD LIKE TO WRITE OUTPUTS TO
    f.write("conda activate minimal_env\n" )
    f.write("Rscript " + codeDir + "run_EmptyDrop.R {0}\n".format(str(sample)))
    f.close()

    # Example batch calling on the codon LSF cluster (this will vary depending on the system, bsub vs. qsub etc)
    print('bsub -n 1 -q standard -R "rusage[mem=20000]" -M 20000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
    sys('bsub -n 1 -q standard -R "rusage[mem=20000]" -M 20000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
