import os
import sys
import gc
import scanpy as sc
import pandas as pd
import matplotlib.backends.backend_pdf as mpdf
import matplotlib.pyplot as plt
import seaborn as sns

IN_DIR = "/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/data/lung/scRNAseq/outputs/"
OUT_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/NSCLC/scRNA-seq/check_res/"
if not os.path.exists(OUT_DIR):
    print(f"Create folder {OUT_DIR}...", file=sys.stderr)
    os.makedirs(OUT_DIR)

CELLTYPES_2_CHECK = ["CAMLs", "AT2 cells", "Anti-inflammatory macrophages"]


def transform_counts(adata):
    adata.layers["raw"] = adata.X
    # Normalised counts
    sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)
    # Logarithmised counts
    sc.pp.log1p(adata)
    # Scaled counts
    sc.pp.scale(adata, max_value=10)
    return adata

def plot_contours(df, gene1, gene2, to_colour="Cell types v25", pdf=None):
    sns.set(rc={"figure.figsize": (8, 8)})
    sns.set_theme(font_scale=1.5, style="white")
    g = sns.kdeplot(data=df, x=gene1, y=gene2, hue=to_colour, common_norm = False)
    if pdf is not None:
        pdf.savefig()
        plt.close()

def plot_boxviolin(gene_mat, to_colour="Cell types v25", pdf=None):
    gene_counts_long = gene_mat.melt(id_vars=to_colour, value_name="norm-log-scale counts", var_name="genes")
    gene_counts_long["norm-log-scale counts"] = gene_counts_long["norm-log-scale counts"].astype("double")
    sns.set(rc={"figure.figsize": (22, 12)})
    sns.set_theme(style="white", font_scale=2)
    _, axs = plt.subplots(2, 1)
    sns.violinplot(data=gene_counts_long, x="genes", y="norm-log-scale counts", hue=to_colour,
                   scale_hue=True, saturation=1, ax=axs[0])
    sns.boxplot(data=gene_counts_long, x="genes", y="norm-log-scale counts", hue=to_colour, ax=axs[1])
    if pdf is not None:
        pdf.savefig()
        plt.close()

if __name__=="__main__":
    gene_set1 = ["LYZ", "APOE", "CD68", "MRC1", "PTPRC"]
    gene_set2 = ["EPCAM", "KRT8", "KRT19"]
    for env in ["Tumour", "Healthy_Background"]:
        # Load data
        adata = transform_counts(sc.read_h5ad(f"{IN_DIR}10X_Lung_{env}_Annotated_v2.h5ad"))
        print(adata, file=sys.stderr)
        gc.collect()
        # Parse latest clustering results
        a = [columnName[12:] for columnName in adata.obs.columns if 'Cell types v' in columnName]
        col_name = "Cell types v" + str(max([int(x) for x in a if x.isdigit()]))
        print(f"Use column {col_name} as the latest clustering version.", file=sys.stderr)
        # Select gene subset
        adata = adata[adata.obs[col_name].isin(CELLTYPES_2_CHECK), gene_set1 + gene_set2]
        adata.obs = adata.obs[["patient_sample", col_name]]
        # Write adata for record
        adata.write(f"{OUT_DIR}adata_{env}_{'_'.join(CELLTYPES_2_CHECK).replace(' ', '_')}.h5ad",
                    compression="gzip")
        # Extract gene count matrix
        gene_mat = pd.DataFrame(adata.X, columns=adata.var.index, index=adata.obs.index)
        gene_mat = pd.concat([gene_mat, adata.obs[col_name]], axis=1)
        del adata
        gc.collect()
        # Plot contours
        pdf = mpdf.PdfPages(f"{OUT_DIR}contour_plots_{env}_3celltypes.pdf")
        for g1 in gene_set1:
            for g2 in gene_set2:
                plot_contours(gene_mat, g1, g2, to_colour=col_name, pdf=pdf)
        pdf.close()
        # Plot violin & box
        pdf = mpdf.PdfPages(f"{OUT_DIR}violinbox_plots_{env}_3celltypes.pdf")
        plot_boxviolin(gene_mat, to_colour=col_name, pdf=pdf)
        pdf.close()

