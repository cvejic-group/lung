from visium_qc_and_visualisation import read_and_qc
import numpy as np
import scanpy as sc
from scipy.spatial import cKDTree
from scipy.stats import skew, skewnorm
from anndata import AnnData
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf
from matplotlib.colors import LogNorm
import seaborn as sns
from sklearn.preprocessing import normalize

sns.set_style("white")
inDir = "/nfs/research1/gerstung/jp27/AC_LNG/3_spaceranger_output/"
outDir = "/nfs/research1/gerstung/nelson/lung/SpatialTranscriptomics/cell2location/"
model = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/annotation_broad/LocationModelLinearDependentWMultiExperiment_8experiments_9clusters_15107locations_2735genes/sp.h5ad"
model_env = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/environment/LocationModelLinearDependentWMultiExperiment_8experiments_3clusters_15107locations_2735genes/sp.h5ad"
sample_names = [
                'spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A',
                'spaceranger110_count_36209_OTAR_LNGsp9476040_GRCh38-2020-A',
                'spaceranger110_count_36210_OTAR_LNGsp9476042_GRCh38-2020-A',
                'spaceranger110_count_36210_OTAR_LNGsp9476044_GRCh38-2020-A',
                'spaceranger110_count_36209_OTAR_LNGsp9476039_GRCh38-2020-A',
                'spaceranger110_count_36209_OTAR_LNGsp9476041_GRCh38-2020-A',
                'spaceranger110_count_36210_OTAR_LNGsp9476043_GRCh38-2020-A',
                'spaceranger110_count_36210_OTAR_LNGsp9476045_GRCh38-2020-A'
]

slides = [read_and_qc(sample_name, path=inDir) for sample_name in sample_names]
adata_vis = slides[0].concatenate(slides[1:], batch_key="sample", uns_merge="unique", batch_categories=sample_names, index_unique=None)
adata_vis.layers["counts"] = np.asarray(adata_vis.X.toarray())
sc.pp.normalize_per_cell(adata_vis, counts_per_cell_after=1e4)
# Normalised counts for each aligned gene per spot
adata_vis.layers["norm"] = np.asarray(adata_vis.X.toarray()).copy()

def main(slide="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A", nn_distance=300, threshold=0.001):

    adata_slide = adata_vis[adata_vis.obs["sample"]==slide, :]
    all_spots = adata_slide.obsm['spatial']
    tree = cKDTree(all_spots)
    n_bootstrap = 300

    # Loop over all spots, from the null distribution of mean gene expression, then sum in neighbours
    # to get a null estimate for the random total expression profile
    spot_nulls = list()
    for idx in adata_slide.obs.index:
        spot = adata_slide[adata_slide.obs.index==idx,:].obsm['spatial']
        nn = tree.query_ball_point(spot, nn_distance)[0] # Indices of the nearest neighbours
        adata_t = adata_slide[adata_slide.obs.index==idx,:]
        bootstrap = np.array([np.random.choice(a=adata_t.layers["norm"].reshape(adata_t.layers["norm"].shape[1],), size=350, replace=False).mean() for i in range(n_bootstrap)])
        # Construct the bootstrapped CCI null estimate in the following way:
        # 1. By the CLT, bootstrap follows a Normal distribution, with a mean, std, and skew
        # 2. Assuming iid gene expression profiles at each spot, so we can estimate the total CCI by all pairwise Normal sums at each spot
        # 3. To simplify, assume differentces between Normals at neighbouring spots are negligible => modify the mean, std, and skew by
        #    2 x the number of spots (factor of 2 for the pairwise combinations)
        # 4. For N spots in total, including the central spot and neighbours:
        #    mean --> N * mean; std --> sqrt(N) * std; skewness --> (1/sqrt(N)) * skewness; kurtosis --> (1/N) * kurtosis
        spot_nulls.append([bootstrap.mean()*len(nn*2), bootstrap.std()*np.sqrt(len(nn*2)), skew(bootstrap)/np.sqrt(len(nn*2))])
    adata_slide.obs["Null CCI Params Mean"] = np.array([s[0] for s in spot_nulls])
    adata_slide.obs["Null CCI Params Std"] = np.array([s[1] for s in spot_nulls])
    adata_slide.obs["Null CCI Params Skew"] = np.array([s[2] for s in spot_nulls])

    # Load the ligand-receptor pairs
    lr_data = sc.read_h5ad(outDir+"AnnData/{0}_LR_pairs_thresh0p001.h5ad".format(slide))
    lrs = lr.var["Ligand-Receptor"].unique()
    p_values = dict()
    for lr in lrs:
        lr_data_t = lr_data[:, lr_data.var["Ligand-Receptor"]==lr]
        spot_values = list()
        for spot in lr_data_t.obs["Spots"].unique():
            mean = adata_slide[adata_slide.obs.index==spot, :].obs["Null CCI Params Mean"].values
            std = adata_slide[adata_slide.obs.index==spot, :].obs["Null CCI Params Std"].values
            skews = adata_slide[adata_slide.obs.index==spot, :].obs["Null CCI Params Skew"].values
            spot_values.append(1-skewnorm.cdf(x=lr_data_t[lr_data_t.obs["Spots"]==spot,:].X[0][0], a=skews, loc=mean, scale=std)[0])
        lr_data.obs["p-values {0}".format(lr)] = np.array(spot_values)

    # Study the behaviour wrt the model parameters
    # UPDATE SPOT VALUES SO THEY ARE NORMALISED (PROBS AND NORMALISED TO TISSUE)
    adata_env = sc.read_h5ad(model_env)
    adata_env_slide = adata_env[adata_env.obs["sample"]==slide, :]
    pdf = mpdf.PdfPages(outDir+"Tumour_LR_significances.pdf")
    for lr in lrs:
        plt.scatter(adata_env_slide.obs["q95_spot_factorsTumour"].values, np.log(lr_data.obs["p-values {0}".format(lr)].values))
        plt.xlabel("Tumour spot factor")
        plt.ylabel("{0} log p-value".format(lr))
        pdf.savefig()
        plt.close()
    pdf.close()

    pdf = mpdf.PdfPages(outDir+"Background_LR_significances.pdf")
    for lr in lrs:
        plt.scatter(adata_env_slide.obs["q95_spot_factorsBackground"].values, np.log(lr_data.obs["p-values {0}".format(lr)].values))
        plt.xlabel("Background spot factor")
        plt.ylabel("{0} log p-value".format(lr))
        pdf.savefig()
        plt.close()
    pdf.close()

    pdf = mpdf.PdfPages(outDir+"Healthy_LR_significances.pdf")
    for lr in lrs:
        plt.scatter(adata_env_slide.obs["q95_spot_factorsHealthy"].values, np.log(lr_data.obs["p-values {0}".format(lr)].values))
        plt.xlabel("Healthy spot factor")
        plt.ylabel("{0} log p-value".format(lr))
        pdf.savefig()
        plt.close()
    pdf.close()


