# CopyKat Analysis for Aneuploidy Detection

The scripts for this task can be found in the GitHub repo [here](https://github.com/sdentro/copykat_pipeline).
It is also added as a submodule in this GitLab repo (use `git clone --recurse-submodules` to directly include it when cloning the current repo).
