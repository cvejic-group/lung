import sys
import scanpy as sc
import anndata
import pandas as pd
import numpy as np
import os
import gc

#data_type = 'float32'

# This line forces theano to use the GPU and should go before importing cell2location
#os.environ["THEANO_FLAGS"] = 'device=cuda0,floatX=' + data_type + ',force_device=True'
# If using the CPU uncomment this:
os.environ["THEANO_FLAGS"] = 'device=cpu,floatX=float32,openmp=True,force_device=True'

import cell2location

import matplotlib as mpl
from matplotlib import rcParams
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

# Silence scanpy that prints a lot of warnings
import warnings
warnings.filterwarnings('ignore')

# Local version of run_colocation
sys.path.append("/nfs/research1/gerstung/nelson/lung/SpatialTranscriptomics/cell2location/")
from run_colocation_local import *

annotations = "environment"
inDir = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/{0}/".format(annotations)
r = {
        'run_name': 'LocationModelLinearDependentWMultiExperiment_8experiments_9clusters_15107locations_2735genes',
        'top_n'   : 9,
     }
if annotations=="environment":
    r = {
            'run_name': 'LocationModelLinearDependentWMultiExperiment_8experiments_3clusters_15107locations_2735genes'
            'top_n'   : 3,
        }

'''
def NMF_Colocation(adata_vis=adata_vis, n_fact=n_fact, n_iter=10000), n_type='restart', n_restarts=5, sample_id=sample_id):
    plotDir = outDir+f'/{sample_id}/{n_fact}_factors/'
    # Select the sample of interest
    adata_vis = adata_vis[adata_vis.obs['sample']==sample_id,:]
    # Get the data for that sample
    X_data = adata_vis.uns['mod']['post_sample_q05']['spot_factors']

    # Instantiate model class
    mod_sk = cell2location.models.CoLocatedGroupsSklearnNMF(n_fact, X_data, n_iter=n_iter, verbose=True,
                                                            var_names=adata_vis.uns['mod']['fact_names'],
                                                            obs_names=adata_vis.obs_names,
                                                            fact_names=['fact_' + str(i) for i in range(n_fact)],
                                                            sample_id=adata_vis.obs['sample'],
                                                            init='random', random_state=0,
                                                            nmf_kwd_args={'tol':0.0001})

    # Train several times to  evaluate stability
    mod_sk.fit(n=n_restarts, n_type=n_type)

    # Evaluate stability by comparing training restarts
    with mpl.rc_context({'figure.figsize': (10, 8)}):
        mod_sk.evaluate_stability('cell_type_factors', align=True)
        pdf = mpdf.PdfPages(plotDir+'StabilityEvaluation.pdf')
        plt.tight_layout()
        pdf.savefig()
        plt.close()
        pdf.close()

    # Evaluate accuracy of the model
    mod_sk.compute_expected()
    mod_sk.plot_posterior_mu_vs_data()
    pdf = mpdf.PdfPages(plotDir+'Posterior_mu_vs_data.pdf')
    plt.tight_layout()
    pdf.savefig()
    plt.close()
    pdf.close()

    # Extract parameters into DataFrames
    mod_sk.sample2df(node_name='nUMI_factors', ct_node_name = 'cell_type_factors')

    # Export results to scanpy object
    adata_vis = mod_sk.annotate_adata(adata_vis) # As columns to .obs
    adata_vis = mod_sk.export2adata(adata_vis, slot_name='mod_sklearn') # As a slot in .uns

    # Save the fraction of cells of each type located to each combination
    top_n = r['top_n']
    df = mod_sk.print_gene_loadings(loadings_attr='cell_type_fractions', gene_fact_name='cell_type_fractions', top_n=top_n)
    df.to_csv(plotDir+'Cell_Fractions.csv')

    from re import sub
    mod_sk.cell_type_fractions.columns = [sub('mean_cell_type_factors', '', i)
                                                  for i in mod_sk.cell_type_fractions.columns]

    # Plot co-occuring cell type combinations
    mod_sk.plot_gene_loadings(mod_sk.var_names_read, mod_sk.var_names_read, fact_filt=mod_sk.fact_filt,
                                loadings_attr='cell_type_fractions', gene_fact_name='cell_type_fractions',
                                cmap='RdPu', figsize=[10, 15])

    pdf = mpdf.PdfPages(plotDir+'Cell_fractions_per_factor.pdf')
    plt.tight_layout()
    pdf.savefig()
    plt.close()
    pdf.close()

    with mpl.rc_context({'figure.figsize': (5, 6), 'axes.facecolor': 'black'}):
        # Select one section correctly subsetting histology image data
        slide = select_slide(adata_vis, sample_id)

        sc.pl.spatial(slide, cmap='magma', color=mod_sk.location_factors_df.columns,
                      ncols=6, size=1, img_key='hires', alpha_img=0, vmin=0, vmax='p99.2')

        pdf = mpdf.PdfPages(plotDir+'nUMI_per_factor_spatial.pdf')
        plt.tight_layout()
        pdf.savefig()
        plt.close()
        pdf.close()
'''

def main():
    # Pick up the cell2location model
    sp_data_file = inDir + r['run_name']+'/sp.h5ad'
    adata_vis = anndata.read(sp_data_file)

    res_dict, adata_vis = run_colocation(
                            adata_vis,
                            model_name='CoLocatedGroupsSklearnNMF',
                            verbose=False,
                            return_all=True,

                            train_args={
                                'n_fact': np.arange(3, 40), # Range of number of factors in the NMF
                                'n_iter': 20000, # Maximum number of training iterations
                                'sample_name_col': 'sample', # Columns in adata_vis.obs that identifies samples
                                'mode': 'normal',
                                'n_type': 'restart',
                                'n_restarts': 5 # number of training restarts
                            },


                            model_kwargs={
                                'init': 'random',
                                'random_state': 0,
                                'nmf_kwd_args': {'tol': 0.00001}
                            },

                            posterior_args={},

                            export_args={
                                'path': inDir+r['run_name']+'/colocation_model/',
                                'run_name_suffix': ''
                            }
                        )

if __name__=="__main__":
    main()
