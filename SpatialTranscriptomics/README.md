# Spatial transcriptomics scripts

This folder contains scripts for spatial transcriptomics analysis.

## 1. `cell2location/`

The `cell2location` folder contains scripts to train the reference model from `scRNA-seq` annotation and deconvolute cell type abundances in Visium data.

## 2. `downstream_analysis/`

The `downstream_analysis` folder contains scripts for cell type abundance pattern analysis and figure plotting.

A `_functions.py` is provided for general usage, several data tables are also included for usage.

### 2.1 Pre-processing of the `cell2location` deconvolution results

The script `a1_spatial_qc.py` and its launcher script `a_submit_qc.bsub` preprocess the `cell2location` deconvolution results.

### 2.2 Analyses of cell type compostion and colocation

The scripts `b1_summarise_frequency.py` and `b4_Spatial_R_filtered.R` plot the cell type abundances on H&E staining images in Fig. 3A, Fig. 4G, Fig 4C(partially), and the bar plots in Fig. 3B.

The script `b3_retrieve_colocalisation.py` and `b4_NMF_colocation.py` plot the dendrogram and NMF dot plot in Fig. 4H-I.

### 2.3 Validation of L-R Usage on Spatial Data

The script `c1_LR_colocalisation.py` extracts the statistics of the given L-R list related to STx data.

The script `c2_LR_colocalisation_plot.py` plots the L-R co-expression on H&E staining images as shown in Fig 3E.

The scripts `c3_LR_colocalisation_test.py` and `c4_LR_heatmap_and_boxplot.R` do Chi2 test and plot Fig. 3C-D.

## 3 Cell type composition between STx and scRNA-seq data (additional)

An additional script `Visium_scRNA_comparison.R` is provided to compare cell type abundances between STx and scRNA-seq data.
