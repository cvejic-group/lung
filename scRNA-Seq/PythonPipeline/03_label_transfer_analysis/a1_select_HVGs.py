import gc
import sys
import scanpy as sc


DATA_DIR = "/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/"
DATA_TUMOUR = f"{DATA_DIR}10X_Lung_Tumour_Annotated_v2.h5ad"
DATA_NORMAL = f"{DATA_DIR}10X_Lung_Healthy_Background_Annotated_v2.h5ad"
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/labeltrans_res/"

CELLTYPES = {"immune": ["Anti-inflammatory alveolar macrophages", "Cycling anti-inflammatory alveolar macrophages",
                        "Anti-inflammatory macrophages", "Cycling anti-inflammatory macrophages", "CAMLs",
                        "Pro-inflammatory macrophages", "STAB1+ anti-inflammatory macrophages",
                        "B cells", "Cycling plasma B cells", "Downregulated B cells", "LYZ+ B cells", "TNF+ B cells",
                        "Plasma B cells", "Immature plasma B cells",
                        "Cycling T cells", "Cycling cytotoxic T cells", "Cytotoxic T cells", "Downregulated T cells",
                        "Exhausted T cells", "Exhausted cytotoxic T cells", "Cycling exhausted cytotoxic T cells",
                        "Gamma delta T cells", "Naive T cells", "Tregs",
                        "cDC2", "Cycling cDC2", "mo-DC2", "Cycling mo-DC2", "pDCs",
                        "NK cells (higher cytotoxicity)", "NK cells (lower cytotoxicity)", "NKB cells", 
                        "Mast cells", "Cycling mast cells", "Immature myeloid cells", "Odd immature cells"],
             "niche": ["AT2 cells", "Cycling AT2 cells", "Activated adventitial fibroblasts",
                       "Atypical epithelial cells", "Cycling epithelial cells", "Transitioning epithelial cells",
                       "Ciliated epithelial cells", "Club cells", "Fibroblasts", "Lymphatic endothelial cells"]}


def restructure(adata2restruct):
    # Parse latest clustring results and remove previous versions
    a = [col[12:] for col in adata2restruct.obs.columns if 'Cell types v' in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"Cell types v{str(max_num)}"
    print(f"=====> Using {col_name} as the latest clustering column.",
          file=sys.stderr)
    adata2restruct.obs.rename(columns={col_name: "latest_annot"},
                              inplace=True)
    # Only keep relevant columns in observations and variables
    adata2restruct.obs.drop([col for col in adata2restruct.obs.columns if "Cell types" in col],
                            axis=1, inplace=True)
    adata2restruct.obs.drop([col for col in adata2restruct.obs.columns if "leiden" in col],
                            axis=1, inplace=True)
    adata2restruct.obs.drop(["batch"], axis=1, inplace=True)
    del adata2restruct.obsm
    print(f"Cell number: {adata2restruct.n_obs}", file=sys.stderr)
    print(adata2restruct, file=sys.stderr)
    return adata2restruct


def PreprocessDataset(adata2pp):
    print(f"* Initial data:", file=sys.stderr)
    print(adata2pp, file=sys.stderr)
    batch_size = adata2pp.obs.patient_sample.value_counts()
    batch2consider = batch_size[batch_size > 3]
    adata2pp = adata2pp[adata2pp.obs["patient_sample"].isin(batch2consider.index), :].copy()
    gc.collect()
    print(f"* Filtered data by removing patient_sample with cells no more than 3:", file=sys.stderr)
    print(adata2pp, file=sys.stderr)
    return adata2pp


if __name__ == "__main__":
    for cgrp in ["immune", "niche"]:
        # Read tumour and H/B datasets and select interested cell types
        adata_hb = restructure(sc.read_h5ad(DATA_NORMAL))
        adata_hb = adata_hb[adata_hb.obs["latest_annot"].isin(CELLTYPES[cgrp]), :]
        adata_hb = PreprocessDataset(adata_hb)
        adata_t = restructure(sc.read_h5ad(DATA_TUMOUR))
        adata_t = adata_t[adata_t.obs["latest_annot"].isin(CELLTYPES[cgrp]), :]
        adata_t = PreprocessDataset(adata_t)

        # Combine dataset to recompute 10000 HVGs
        adata_full = adata_hb.concatenate(adata_t, join="inner")
        sc.pp.filter_genes(adata_full, min_cells=100)
        sc.pp.normalize_total(adata_full)
        sc.pp.log1p(adata_full)
        sc.pp.highly_variable_genes(adata_full, n_top_genes=10000, batch_key="patient_sample", subset=True)

        # Save subsets of anndata
        adata_hb[:, adata_full.var_names].write_h5ad(f"{OUT_DIR}Healthy_Background_{cgrp}.h5ad")
        adata_t[:, adata_full.var_names].write_h5ad(f"{OUT_DIR}Tumour_{cgrp}.h5ad")
        
        del adata_full, adata_hb, adata_t
        gc.collect()
