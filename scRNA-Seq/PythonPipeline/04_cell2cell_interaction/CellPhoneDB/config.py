lung_config = {
        "Tumour": {
            "remove": ["Cycling myeloid cells,2,0 (to remove)", "Mast cells,3 (to remove)", "Mast cells,9 (to remove)"],
            "name": "Cell types v25"
        },
        "Healthy_Background": {
            "remove": ["Mast cells,7 (to remove)", "Mast cells,9 (to remove)", "8,1"],
            "name": "Cell types v12"
        },
}
