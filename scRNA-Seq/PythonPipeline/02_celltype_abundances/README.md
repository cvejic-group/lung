# Estimate Cell Type Abundances

This folder comprises scripts to estimate cell type abundances and apply statistical tests.
- `calculate_abundances.py` and `submit_calc.bsub` for abundance estimation.
- `test-diff.ipynb` for statistical tests.
