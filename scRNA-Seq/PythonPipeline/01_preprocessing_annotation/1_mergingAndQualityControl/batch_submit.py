from os import system as sys

fname = "Merging_Data"
f = open(fname+".sh", 'w')
f.write("cd /nfs/research1/gerstung/nelson/lung/scRNA-Seq/PythonPipeline/1_mergingAndQualityControl/\n") # IMPORTANT: CHANGE TO A DIRECTORY WHERE YOU CLONE THE MERGING PYTHON CODE
f.write("conda activate minimal_env\n")
f.write("python run_merging_samples_and_QC.py")
f.close()

# You typically need bigmem allocation to run this job on an LSF
print('bsub -n 1 -q bigmem -R "rusage[mem=1000000]" -M 1000000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
sys('bsub -n 1 -q bigmem -R "rusage[mem=1000000]" -M 1000000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
