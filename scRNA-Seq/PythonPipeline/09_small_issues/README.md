# Small Studies for Particular Concerns

## 1. `CheckCAMLs/`

This folder provides scripts for examination of CAMLs' biological nature.

- `a1_plot_contour_violinbox.py` (submitted by `a_check_3_celltypes.bsub`) to plot contour and box plots as shown in Fig. 1 E-F.
- `a2_subclustering_plot_umap_violinbox.py` (submitted by `a_check_3_celltypes.bsub`) to subcluster CAMLs (by k-means) into 10 subclusters, and plot violin plots as shown in Suppl. Fig. 1D.
- `b_check_doublets.ipynb` examines if CAMLs are likely to be predicted as doublets (also for other cell types in H/B environment in addition), relating to Suppl. Fig. 1E and Suppl. Fig. 3.
- `e1_subclustering_CAMLs_and_pseudobulking.py` and `e2_DESeq2_on_pseodu.R` make sub-clustering of CAMLs into 2 subcluster by **the expression of EPCAM only** and do the DEA between the sub-clusters, as required in revision stage.

## 2. `ComputeCellProportion/`

This folder contains supportive scripts for the filtration of genes based on the expression percentage in a cell type.

- `c1_genes_above_30pct.py` extracts on the level of Tumour & Healthy/Background.
- `c2_genes_above_30pct_tumour.py` extracts on the level of LUAD & LUSC.

The two scripts are organised and submitted to the LSF system by `c_submit.bsub`.

## 3. `ReplotUMAPs/`

This folder contains one Jupyter Notebook to reproduce the UMAPs in a consistent shape for Fig. 1C and Suppl. Fig. 1B-C.

## 4. `STAB1_percentage/`

This folder contains one Jupyter Notebook to examine the association of STAB1+ Mac frequency with respect to patients' cancer stage and cancer subtypes.
