#!/usr/bin/env python
import warnings
warnings.simplefilter("ignore")

import sys

import harmonypy as hm
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors

# IMPORTANT: CHANGE THIS TO THE CLONED FUNCTIONS DIRECTORY CONTAINING THE scRNA_functions CLASS
sys.path.append("/nfs/research1/gerstung/nelson/lung/Functions/")

from scRNA_functions import scRNA_functions
fc = scRNA_functions()

myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', '#DDEFFF', '#000035', '#7B4F4B', '#A1C299', '#300018', '#C2FF99', '#0AA6D8', '#013349',
            '#00846F', '#8CD0FF', '#3B9700', '#04F757', '#C8A1A1', '#1E6E00', '#DFFB71', '#868E7E', '#513A01', '#CCAA35',
            '#800080', '#DAA520', '#1E90FF', '#3CB371', '#9370DB', '#8FBC8F', '#00FF7F', '#0000CD', '#556B2F', '#FF00FF',
            '#CD853F', '#6B8E23', '#008000', '#6495ED', '#00FF00', '#DC143C', '#FFFF00', '#00FFFF', '#FF4500', '#4169E1',
            '#48D1CC', '#191970', '#9ACD32', '#FFA500', '#00FA9A', '#2E8B57', '#40E0D0', '#D2691E', '#66CDAA', '#FFEFD5',
            '#20B2AA', '#FF0000', '#EEE8AA', '#BDB76B', '#E9967A', '#AFEEEE', '#000080', '#FF8C00', '#B22222', '#5F9EA0',
            '#ADFF2F', '#FFE4B5', '#7B68EE', '#7FFFD4', '#0000FF', '#BA55D3', '#90EE90', '#FFDAB9', '#6A5ACD', '#8B0000',
            '#8A2BE2', '#CD5C5C', '#F08080', '#228B22', '#FFD700', '#006400', '#98FB98', '#00CED1', '#00008B', '#9400D3',
            '#9932CC', '#4B0082', '#F0E68C', '#483D8B', '#008B8B', '#8B008B', '#4682B4']

# Define a nice colour map for gene expression
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Inputs and Outputs
# IMPORTANT: THESE MUST BE UPDATED TO SUIT WHERE YOU ARE STORING AND SAVING DATA
outDir = "/nfs/research1/gerstung/nelson/outputs/lung/2_batchCorrection/"
inputs = '/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged/10X_Lung.h5ad' # INPUT IS THE FULLY MERGED H5AD OBJECT
outputs = '/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr/10X_Lung_Healthy_Background.h5ad' # OUTPUT WILL BE EITHER TUMOUR OR HEALTHY+BACKGROUND

def runUMAP(data, n_components=3, pdf=None, name_3D=None): # Three UMAP components by default

    sc.settings.verbosity=3
    print("\n * Computing UMAP")
    sc.tl.umap(data, random_state=10, n_components=n_components, init_pos='random')

    # Plot UMAP
    f, axs = plt.subplots(1,3,figsize=(24,8))
    sns.set(font_scale=1.5)
    sns.set_style("white")

    fc.plotUMAP(data, variable="patient_sample", palette=myColors, components=["1,2"], ax=axs[0], pdf=pdf)
    fc.plotUMAP(data, variable="patient_sample", palette=myColors, components=["1,3"], ax=axs[1], pdf=pdf)
    fc.plotUMAP(data, variable="patient_sample", palette=myColors, components=["2,3"], ax=axs[2], pdf=pdf)

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    if pdf is not None:
        pdf.savefig()
        plt.close()
    else:
        plt.show(block=False)

    # 3D UMAP plot for completenes
    if name_3D is not None:
        groupings = ["patient_sample"]
        for grouping in groupings:
            fc.plot3D(data, group_by=grouping, space="X_umap", palette=myColors, write_to=outDir+name_3D+"_"+grouping)

    f, axs = plt.subplots(1,3,figsize=(24,8))
    sns.set(font_scale=1.5)
    sns.set_style("white")

    fc.plotUMAP(data, variable="n_genes_by_counts", components=["1,2"], ax=axs[0], pdf=pdf)
    fc.plotUMAP(data, variable="total_counts",      components=["1,2"], ax=axs[1], pdf=pdf)
    fc.plotUMAP(data, variable="pct_counts_mt",     components=["1,2"], ax=axs[2], pdf=pdf)

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    if pdf is not None:
        pdf.savefig()
        plt.close()
    else:
        plt.show(block=False)

def main(harmony_off=False):
    pdf = mpdf.PdfPages(outDir+"Lung_batchCorrection_Healthy_Background.pdf")
    # Loading samples
    adata = sc.read_h5ad(inputs)

    # Remove donor 3 -- this sample must always be removed as the patient has a suspected
    # adenocarcinoma
    adata = adata[adata.obs.patient!="Donor 3", :]
    adata.obs["environment"] = np.array([i.split(" ")[0] for i in adata.obs["sample"]])
    # UNCOMMENT ONE OF THE BELOW (SET TO SELECT HEALTHY+BACKGROUND)
    #adata = adata[adata.obs.environment=="Tumour", :] # Tumour only
    adata = adata[adata.obs.environment!="Tumour", :] # Healthy + Background

    # Normalise data appropriately for the HVG selection and batch correction
    # IMPORTANT: Normalisation should always be applied at this point, rather than in the merging
    # script. This is because we need to run on the tumour and healthy_background environments
    # separately when determining the cell type annotations
    adata.layers["counts"] = adata.X
    sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)
    adata.layers["norm"] = np.asarray(adata.X).copy()
    sc.pp.log1p(adata)
    adata.layers["log"] = np.asarray(adata.X.copy())
    adata.raw = adata.copy()
    sc.pp.scale(adata, max_value=10)
    adata.layers["scale"] = np.asarray(adata.X.copy())

    # # 1. Setup and get the HVGs
    # Normalization - log trasformation for HVG detection
    adata.X = adata.layers["log"]

    # Highly Variable Genes calculated considering all batches
    print(" * Highly Variable Genes")
    sc.pp.highly_variable_genes(adata, min_mean=0.00125, max_mean=3, min_disp=0.5, batch_key="batch")

    # HVGs plot
    fc.plotHVGs(adata, pdf=pdf)

    print(" \t* Number of control HVGs: %d\n"%(adata[:, adata.var['highly_variable']].n_vars))

    data = np.array(adata[:, adata.var['highly_variable']].X)
    print(" * Total number of genes: %d"%(data.shape[1]))
    print(" \t-> Min expr=%.1f"%(np.min(data)))
    print(" \t-> Max expr=%.1f"%(np.max(data)))

    if len(np.where(~data.any(axis=1))[0]) != 0:
        print("Warning: some cells in HVGs have not any expressed gene!")


    # # 2. Apply PCA to the detected HVGs
    # Scaling - for PCA
    adata.X = adata.layers["scale"]
    # Dimensionality reduction - PCA
    print(" * Performing PCA on the detected HVGs")
    sc.tl.pca(adata, n_comps=50, svd_solver='auto', use_highly_variable=True)

    # Elbow plot for number of principal components
    fc.PCA_ElbowPlot(adata, width=12, height=9, pdf=pdf)
    n_pca = 15 # Number of principal components put in by hand for now (15 for DS-only samples and 17 for DS + healthy)

    # # 3. Use the PCs to check for batch effects by plotting the space using neightbours an UMAP
    # Analysis with Neighbors + UMAP
    sc.pp.neighbors(adata,
                n_pcs=n_pca,
                use_rep="X_pca",
                knn=True,
                random_state=42,
                method='umap',
                metric='euclidean')

    # Run UMAP and save results to the pdf
    runUMAP(adata, n_components=3, pdf=pdf, name_3D="Lung_Tumour_UMAP_beforeBatch_beforeRegress")

    if not harmony_off:
        # # 4. Analysis with Harmony + Neighbors + UMAP
        # Calling and running Harmony to remove batch effects
        ho = hm.run_harmony(adata.obsm['X_pca'][:, :n_pca],
                        adata.obs,
                        ["batch", "patient", "sample"],
                        max_iter_kmeans=25,
                        max_iter_harmony=500)

        adata.obsm["X_pca_harmonize"] = ho.Z_corr.T

        sc.pp.neighbors(adata,
                    n_pcs=adata.obsm["X_pca_harmonize"].shape[1],
                    use_rep="X_pca_harmonize",
                    knn=True,
                    random_state=42,
                    method='umap',
                    metric='euclidean')

        runUMAP(adata,n_components=3, pdf=pdf, name_3D="Lung_Tumour_UMAP_afterBatch_beforeRegress")

    # # 5. Regress out number of genes and counts and repeat the analysis for batch effects
    adata_reg = adata.copy()
    adata_reg.X = adata_reg.layers["log"]
    sc.pp.regress_out(adata_reg, ['total_counts', 'n_genes_by_counts'])

    # Scaling - for PCA
    adata_reg.X = adata_reg.layers["scale"]
    # Dimensionality reduction - PCA
    print(" * Performing PCA on the detected HVGs")
    sc.tl.pca(adata_reg, n_comps=50, svd_solver='auto', use_highly_variable=True)
    fc.PCA_ElbowPlot(adata_reg, width=12, height=9, pdf=pdf)
    n_pca = 15

    # Checking batch effects
    # Analysis with Neighbors + UMAP
    sc.pp.neighbors(adata_reg,
                n_pcs=n_pca,
                use_rep="X_pca",
                knn=True,
                random_state=42,
                method='umap',
                metric='euclidean')

    runUMAP(adata_reg, n_components=3, pdf=pdf, name_3D="Lung_Healthy_Background_UMAP_beforeBatch_afterRegress")

    if not harmony_off:
        # Analysis with Harmony + Neighbors + UMAP
        ho = hm.run_harmony(adata_reg.obsm['X_pca'][:, :n_pca],
                        adata_reg.obs,
                        ["batch"], # "patient", "sample"],
                        max_iter_kmeans=25,
                        max_iter_harmony=500)

        adata_reg.obsm["X_pca_harmonize"] = ho.Z_corr.T

        sc.pp.neighbors(adata_reg,
                    n_pcs=adata_reg.obsm["X_pca_harmonize"].shape[1],
                    use_rep="X_pca_harmonize",
                    knn=True,
                    random_state=42,
                    method='umap',
                    metric='euclidean')

        runUMAP(adata_reg, n_components=3, pdf=pdf, name_3D="Lung_Healthy_Background_UMAP_afterBatch_afterRegress")

    # Close pdf
    pdf.close()

    # Don't save these large matrices
    adata_reg.X = adata_reg.layers["counts"]
    del adata_reg.layers

    # # 6. Save object
    sparseX  = sparse.csr_matrix(adata_reg.X)
    adata_reg.X = sparseX
    adata_reg.write(outputs, compression='gzip')

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-ho', "--harmony_off", action="store_true", help="Deactivate PyHARMONY")

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
