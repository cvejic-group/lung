# EmptyDrop

The ```run_EmptyDrop.R``` script can be called direct with R, or can be submitted in multiple bounds on a batch system with ```batch_submit.py```, whcih will
loop over all CellRanger data-sets of interest as specified in the list by a user. Example single line command to call ```run_EmptyDrop.R```:

```
Rscript run_EmptyDrop.R cellranger310_count_33701_OT10xRNA8768127_GRCh38-3_0_0
```

which will populate the ```cellranger310_count_33701_OT10xRNA8768127_GRCh38-3_0_0``` directory with an ```outputEmptyDrops``` directory, containing the corrected
gene expression matrix. 
