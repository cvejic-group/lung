import gc
import scanpy as sc
import sys
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf
import pandas as pd
import json

sys.path.append("/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/lung/Functions/")
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

def main(cell_type="B cells"):
    in_dir = "/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/data/lung/scRNAseq/outputs/"
    adata_t = sc.read_h5ad(in_dir+"10X_Lung_Tumour_Annotated_v2.h5ad")
    to_remove = ["Cycling myeloid cells,2,0 (to remove)", "Mast cells,3 (to remove)", "Mast cells,9 (to remove)"]
    adata_t = adata_t[adata_t.obs["sorting"]=="CD235a-", :]
    adata_t = adata_t[~adata_t.obs["Cell types v25"].isin(to_remove), :]
    gc.collect()

    merging_dict = {
        "Cycling T cells": "T cells",
        "Cycling plasma B cells": "B cells",
        "Cycling anti-inflammatory macrophages": "Anti-inflammatory macrophages",
        "Cycling exhausted cytotoxic T cells": "T cells",
        "mo-DC2": "DCs",
        "B cells": "B cells",
        "Downregulated B cells": "B cells",
        "TNF+ B cells": "B cells",
        "LYZ+ B cells": "B cells",
        "Plasma B cells": "B cells",
        "Mast cells": "Mast cells",
        "Cycling mast cells": "Mast cells",
        "STAB1+ anti-inflammatory macrophages": "Anti-inflammatory macrophages",
        "Anti-inflammatory macrophages": "Anti-inflammatory macrophages",
        "Immature myeloid cells": "Immature myeloid cells",
        "Pro-inflammatory macrophages": "Pro-inflammatory macrophages",
        "cDC2": "DCs",
        "Anti-inflammatory alveolar macrophages": "Anti-inflammatory macrophages",
        "Cycling anti-inflammatory alveolar macrophages": "Anti-inflammatory macrophages",
        "CAMLs": "CAMLs",
        "NK cells (higher cytotoxicity)": "NK cells",
        "NK cells (lower cytotoxicity)": "NK cells",
        "Cytotoxic T cells": "T cells",
        "Exhausted cytotoxic T cells": "T cells",
        "Downregulated T cells": "T cells",
        "Naive T cells": "T cells",
        "Tregs": "T cells",
        "Exhausted T cells": "T cells",
        "pDCs": "DCs",
        "AT2 cells": "Stroma",
        "Cycling AT2 cells": "Stroma",
        "Ciliated epithelial cells": "Stroma",
        "Activated adventitial fibroblasts": "Stroma",
        "Lymphatic endothelial cells": "Stroma",
        "Fibroblasts": "Stroma",
        "Cycling epithelial cells": "Stroma",
        "Atypical epithelial cells": "Stroma",
        "Transitioning epithelial cells": "Stroma"
    }

    fc.mergeClusters(adata_t, newName="Broad", labels=merging_dict, cluster="Cell types v25", verbose=True)
    adata_t = adata_t[adata_t.obs["Broad"]==cell_type,:]
    gc.collect()

    prop_dict_t = {}
    prop_dict_t_samples = {}
    for c in adata_t.obs["Cell types v25"].unique():
        prop_dict_t[c] = adata_t[adata_t.obs["Cell types v25"]==c,:].n_obs*100/adata_t.n_obs

        abundances = []
        for p in adata_t.obs["patient"].unique():
            adata_tp = adata_t[adata_t.obs["patient"]==p,:]
            abundances.append(adata_tp[adata_tp.obs["Cell types v25"]==c,:].n_obs*100/adata_tp.n_obs)
        prop_dict_t_samples[c] = abundances

    adata_h = sc.read_h5ad(in_dir+"10X_Lung_Healthy_Background_Annotated_v2.h5ad")
    to_remove = ["Mast cells,7 (to remove)", "Mast cells,9 (to remove)", "8,1"]
    adata_h = adata_h[adata_h.obs["sorting"]=="CD235a-", :]
    adata_h = adata_h[~adata_h.obs["Cell types v12"].isin(to_remove), :]
    gc.collect()

    merging_dict = {
        "Cycling T cells": "T cells",
        "Cycling plasma B cells": "B cells",
        "Cycling anti-inflammatory macrophages": "Anti-inflammatory macrophages",
        "Cycling exhausted cytotoxic T cells": "T cells",
        "Odd immature cells": "Immature myeloid cells",
        "mo-DC2": "DCs",
        "Cycling mo-DC2": "DCs",
        "B cells": "B cells",
        "Downregulated B cells": "B cells",
        "TNF+ B cells": "B cells",
        "LYZ+ B cells": "B cells",
        "Plasma B cells": "B cells",
        "Immature plasma B cells": "B cells",
        "Mast cells": "Mast cells",
        "Cycling mast cells": "Mast cells",
        "STAB1+ anti-inflammatory macrophages": "Anti-inflammatory macrophages",
        "Anti-inflammatory macrophages": "Anti-inflammatory macrophages",
        "Immature myeloid cells": "Immature myeloid cells",
        "Pro-inflammatory macrophages": "Pro-inflammatory macrophages",
        "cDC2": "DCs",
        "Cycling cDC2": "DCs",
        "Anti-inflammatory alveolar macrophages": "Anti-inflammatory macrophages",
        "Cycling anti-inflammatory alveolar macrophages": "Anti-inflammatory macrophages",
        "CAMLs": "CAMLs",
        "NK cells (higher cytotoxicity)": "NK cells",
        "NK cells (lower cytotoxicity)": "NK cells",
        "NKB cells": "B cells", # A type of B cell
        "Cytotoxic T cells": "T cells",
        "Cycling cytotoxic T cells": "T cells",
        "Exhausted cytotoxic T cells": "T cells",
        "Cycling exhausted cytotoxic T cells": "T cells",
        "Downregulated T cells": "T cells",
        "Naive T cells": "T cells",
        "Tregs": "T cells",
        "Exhausted T cells": "T cells",
        "pDCs": "DCs",
        "Gamma delta T cells": "T cells",
        "AT2 cells": "Stroma",
        "Cycling AT2 cells": "Stroma",
        "Ciliated epithelial cells": "Stroma",
        "Activated adventitial fibroblasts": "Stroma",
        "Lymphatic endothelial cells": "Stroma",
        "Fibroblasts": "Stroma",
        "Cycling epithelial cells": "Stroma",
        "Atypical epithelial cells": "Stroma",
        "Transitioning epithelial cells": "Stroma",
        "Club cells": "Stroma"
    }

    fc.mergeClusters(adata_h, newName="Broad", labels=merging_dict, cluster="Cell types v12", verbose=True)
    adata_h = adata_h[adata_h.obs["Broad"]==cell_type,:]
    gc.collect()

    prop_dict_hb = {}
    prop_dict_hb_samples = {}
    for c in adata_h.obs["Cell types v12"].unique():
        prop_dict_hb[c] = adata_h[adata_h.obs["Cell types v12"]==c,:].n_obs*100/adata_h.n_obs

        abundances = []
        for p in adata_h.obs["patient"].unique():
            adata_hp = adata_h[adata_h.obs["patient"]==p,:]
            abundances.append(adata_hp[adata_hp.obs["Cell types v12"]==c,:].n_obs*100/adata_hp.n_obs)
        prop_dict_hb_samples[c] = abundances

    # Let's update these dictionaries accordingly
    for c, _ in prop_dict_hb.items():
        if c not in prop_dict_t.keys():
            prop_dict_t[c] = 0.

    for c, _ in prop_dict_t.items():
        if c not in prop_dict_hb.keys():
            prop_dict_hb[c] = 0.

    # Build DataFrame
    cell_type_name = cell_type.replace(" ", "_")
    out_dir = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/scRNA-seq/abundance_res/"
    plt.rcParams["figure.figsize"] = (15,10)
    pdf = mpdf.PdfPages(out_dir+f"Lung_Cell_Abundances_CD235a_{cell_type_name}.pdf")
    df = pd.DataFrame({c: [prop_dict_t[c], prop_dict_hb[c]] for c, _ in prop_dict_t.items()}, index=["Tumour", "Healthy + Background"])
    df.plot(kind="bar", stacked=True, rot=0)
    plt.title("Cell type abundances [%], CD235a- sorting")
    plt.ylabel("Percentage abundance")
    plt.legend(loc="best")
    pdf.savefig()
    plt.close()
    pdf.close()

    with open(out_dir+f"Abundances_Tumour_{cell_type_name}.json", "w") as js:
        json.dump(prop_dict_t_samples, js)

    with open(out_dir+f"Abundances_Healthy_Background_{cell_type_name}.json", "w") as js:
        json.dump(prop_dict_hb_samples, js)

if __name__=="__main__":
    for ct in ["NK cells", "DCs", "B cells", "T cells", "Immature myeloid cells",
               "Anti-inflammatory macrophages", "Pro-inflammatory macrophages", "Mast cells", "CAMLs", "Stroma"]:
        main(ct)
        gc.collect()
