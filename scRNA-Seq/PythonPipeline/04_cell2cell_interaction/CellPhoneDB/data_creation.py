#!/usr/bin/env python
# coding: utf-8
import warnings
warnings.simplefilter("ignore")
import sys
import os

import matplotlib
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors


from config import lung_config

sns.set_style("white")
# AGAIN, WOUKD NEED UPDATING
in_dir = "/nfs/research/gerstung/nelson/data/lung/scRNAseq/outputs/combined/"

def main(environment="Tumour", stratified=False, integrated=False):
    # # Loading data -- THIS WILL NEED TO BE UPDATED TO REFLECT THE DATA LOCATION IN YOUR DIRECTORY
    inputs = in_dir+ "10X_Lung_{}_Annotated_v2_counts_integrated_norm.h5ad".format(environment) if integrated else in_dir+"10X_Lung_{}_Annotated_v2.h5ad".format(environment)
    out_dir = "/nfs/research/gerstung/zaira/lung/scRNA-Seq/PythonPipeline/CellPhoneDB/"
    adata = sc.read_h5ad(inputs)
    annotations = lung_config[environment]["name"]
    to_remove = lung_config[environment]["remove"]
    adata = adata[~adata.obs[annotations].isin(to_remove),:]
    adata.obs["Cancer type"] = adata.obs["patient"].map(lung_config["Cancer_type"])

    adata = adata.copy()
    counts = adata.X.toarray()
    adata.X = counts
    sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)
    adata.layers["norm"] = adata.X
    if not os.path.exists(out_dir+"Results/Cell_types/"):
        os.makedirs(out_dir+"Results/Cell_types/")

    adatas = list()
    for ps in adata.obs["patient_sample"].unique():
        for ct in adata.obs[annotations].unique():
            adata_t = adata[(adata.obs["patient_sample"] == ps) & (adata.obs[annotations] == ct),:]
            sc.pp.subsample(adata_t, fraction=0.5)
            adatas.append(adata_t)
    adata = adatas[0].concatenate(adatas[1:], join="outer")

    for env in adata.obs["environment"].unique():
        reduced = adata[adata.obs["environment"]==env,:]
        counts = pd.DataFrame(data=reduced.layers["norm"], index=reduced.obs.index.tolist(), columns=reduced.var.index.tolist()).T
        counts.index.name = "Gene"
        counts.to_csv(out_dir+"Results/Cell_types/{0}_counts_original_stratified50.txt".format(env), sep="\t")

        meta = pd.DataFrame(data=reduced.obs[annotations].tolist(), index=reduced.obs.index.tolist(), columns=["Cell type"])
        meta.index.name = "Cell"
        meta.to_csv(out_dir+"Results/Cell_types/{0}_meta_original_stratified50.txt".format(env), sep="\t")

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-e", "--environment", type=str, help="Specifiy data-set and biological conditions",default="Tumour")

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
