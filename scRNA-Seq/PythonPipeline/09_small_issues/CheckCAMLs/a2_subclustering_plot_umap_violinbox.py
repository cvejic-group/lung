import os
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from sklearn.cluster import KMeans

GENES = ["LYZ", "APOE", "CD68", "MRC1", "PTPRC", "EPCAM", "KRT8", "KRT19"]

IN_DIR = "/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/"
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/scRNA-seq/check_CAMLs/"
if not os.path.exists(OUT_DIR):
    print("Creating folder:", OUT_DIR, file=sys.stderr)
    os.mkdir(OUT_DIR)


def transform_counts(adata):
    print("Transforming adata counts...", file=sys.stderr)
    adata.layers["raw"] = adata.X
    # Normalised counts
    sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)
    # Logarithmised counts
    sc.pp.log1p(adata)
    # Scaled counts
    sc.pp.scale(adata, max_value=10)
    return adata

    
if __name__ == "__main__":
    adata = transform_counts(sc.read_h5ad(f"{IN_DIR}10X_Lung_Tumour_Annotated_v2.h5ad"))
    # Parse latest clustering results
    a = [columnName[12:] for columnName in adata.obs.columns if 'Cell types v' in columnName]
    latest_leiden = "Cell types v" + str(max([int(x) for x in a if x.isdigit()]))
    print(f"Use column {latest_leiden} as the latest clustering version.", file=sys.stderr)
    if any(adata.obs[latest_leiden].isin(["CAMLs"])):
        # Select subset for re-clustering
        idx_subset = adata.obs[latest_leiden].isin(["CAMLs"])
        # K-means clustering
        X = np.array(adata[idx_subset, :].obsm["X_umap"])
        labels = [str(x) for x in KMeans(n_clusters=10, random_state=42).fit(X).labels_]
        # Assign labels
        adata.obs["new annot"] = adata.obs[latest_leiden].astype("str")
        adata.obs["new annot"].loc[idx_subset] = adata.obs[latest_leiden].loc[idx_subset].astype("str") + ":" + labels
        print(adata.obs["new annot"].value_counts(), file=sys.stderr)
        adata.obs.to_csv(f"{OUT_DIR}adata_obs_table_Tumour.csv")
        adata.obs["new annot"].loc[~adata.obs[latest_leiden].isin(["CAMLs", "AT2 cells", "Anti-inflammatory macrophages"])] = "_others"
        # Plot UMAPs
        pdf = mpdf.PdfPages(f"{OUT_DIR}umap_Tumour_CAMLs_subclustering.pdf")
        _, axes = plt.subplots(1, 3, figsize=(35, 10))
        sns.set_theme(font_scale=1.5)
        sc.pl.umap(adata, color="new annot", size=150, components="1,2", use_raw=False, ax=axes[0], legend_loc=None)
        sc.pl.umap(adata, color="new annot", size=150, components="1,3", use_raw=False, ax=axes[1], legend_loc=None)
        sc.pl.umap(adata, color="new annot", size=150, components="2,3", use_raw=False, ax=axes[2])
        pdf.savefig()
        pdf.close()
        # Plot violins
        idx_subset = adata.obs[latest_leiden].isin(["CAMLs", "AT2 cells", "Anti-inflammatory macrophages"])
        adata.obs["new annot"].replace(to_replace="Anti-inflammatory macrophages", value="Anti-infl.\nmacrophages", inplace=True)
        gene_counts = pd.DataFrame(adata[idx_subset, GENES].X, 
                                   columns=adata[idx_subset, GENES].var.index, 
                                   index=adata[idx_subset, GENES].obs.index)
        gene_counts = pd.concat([gene_counts, adata[idx_subset, GENES].obs["new annot"]], axis=1)
        pdf = mpdf.PdfPages(f"{OUT_DIR}violinbox_plots_Tumour_CAMLs_10clusters.pdf")
        # Violin plot
        sns.set(rc={"figure.figsize": (20, 18)})
        sns.set_theme(style="white", font_scale=1.5)
        _, axs = plt.subplots(len(GENES), 1, sharex=True, sharey=False)
        for i, g in enumerate(GENES):
            sns.violinplot(data=gene_counts, x="new annot", y=g, saturation=1, ax=axs[i])
            if i < len(GENES) - 1:
                axs[i].set_xlabel(None)
        plt.tight_layout()
        pdf.savefig()
        # Box plot
        sns.set(rc={"figure.figsize": (20, 18)})
        sns.set_theme(style="white", font_scale=1.5)
        _, axs = plt.subplots(len(GENES), 1, sharex=True, sharey=False)
        for i, g in enumerate(GENES):
            sns.boxplot(data=gene_counts, x="new annot", y=g, saturation=1, ax=axs[i])
            if i < len(GENES) - 1:
                axs[i].set_xlabel(None)
        plt.tight_layout()
        pdf.savefig()
        pdf.close()
    else:
        print("ERROR: no CAMLs in the cell type annotation!", file=sys.stderr)
        sys.exit(1)
