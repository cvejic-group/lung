# Label Transfer between Datasets

This folder contains scripts transferring cell type labels bewteen datasets, to ensure the consistency of annotations.

The script `a1_select_HVGs.py` (submitted by `a_submit_subsetGenes.bsub`) selects highly variable genes on the combined tumour and healthy+background data.

The script `b_submit_transfer.bsub` organises the two python scripts below to launch them to the LSF system.

- The script `b1_label_transfer_immune.all.py` does label transfer on the broad cell types.
- The script `b2_label_transfer_analysis.all.ipynb` compares and checks transferred labels with our separate annotations.
