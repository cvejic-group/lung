import gc
import sys
import pandas as pd
import scanpy as sc
import matplotlib.pyplot as plt
import seaborn as sns


CELLTYPES = ["Tregs", "Naive T cells", "Cytotoxic T cells", "Cycling T cells", "Exhausted T cells",
             "Exhausted cytotoxic T cells", "Cycling exhausted cytotoxic T cells",
             "NK cells (higher cytotoxicity)", "NK cells (lower cytotoxicity)",
             "cDC2", "mo-DC2", "Pro-inflammatory macrophages", "Anti-inflammatory macrophages",
             "STAB1+ anti-inflammatory macrophages", "Anti-inflammatory alveolar macrophages",
             "Cycling anti-inflammatory macrophages", "CAMLs", "AT2 cells", "Cycling AT2 cells",
             "Cycling epithelial cells", "Fibroblasts", "Activated adventitial fibroblasts", "Lymphatic endothelial cells"]
GENES = ["CTLA4", "CD80", "CD86", "HAVCR2", "LGALS9", "TIGIT", "CD226", "NECTIN2", "NECTIN3", "CD96", "NECTIN1", "PDCD1", "CD274"]
LUAD_SAMPLES = ["Patient 5", "Patient 6", "Patient 7", "Patient 9", "Patient 10", "Patient 13", "Patient 14", "Patient 15", "Patient 16",
                "Patient 21", "Patient 22", "Patient 24", "Patient 25"]
LUSC_SAMPLES = ["Patient 2", "Patient 3", "Patient 4", "Patient 8", "Patient 11", "Patient 18", "Patient 19", "Patient 20"]


def restructure(adata2restruct):
    # Parse latest clustring results and remove previous versions
    a = [col[12:] for col in adata2restruct.obs.columns if 'Cell types v' in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"Cell types v{str(max_num)}"
    print(f"=====> Using {col_name} as the latest clustering column.", file=sys.stderr)
    adata2restruct.obs.rename(columns={col_name: "latest_annot"}, inplace=True)
    # Only keep relevant columns in observations and variables
    adata2restruct.obs = adata2restruct.obs[["patient", "latest_annot"]]
    print(f"Cell number: {adata2restruct.n_obs}", file=sys.stderr)
    del adata2restruct.var, adata2restruct.obsm
    gc.collect()


if __name__ == "__main__":
    adata = sc.read_h5ad("/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/10X_Lung_Tumour_Annotated_v2.h5ad")
    restructure(adata)
    sc.pp.normalize_total(adata, target_sum=1e4)
    sc.pp.log1p(adata)
    adata = adata[adata.obs["latest_annot"].isin(CELLTYPES) & adata.obs["patient"].isin(LUAD_SAMPLES + LUSC_SAMPLES), :].copy()
    gc.collect()
    adata.obs["latest_annot"] = adata.obs["latest_annot"].cat.reorder_categories(new_categories=CELLTYPES)
    adata.obs["environment"] = "None"
    adata.obs.loc[[x in LUAD_SAMPLES for x in adata.obs["patient"]], "environment"] = "LUAD"
    adata.obs.loc[[x in LUSC_SAMPLES for x in adata.obs["patient"]], "environment"] = "LUSC"
    vmax_both = pd.concat([pd.DataFrame(adata[:, GENES].X.todense(), columns=GENES, index=adata.obs.index),
                           adata.obs[["latest_annot", "environment"]]],
                          axis=1).groupby(["latest_annot", "environment"]).mean().max().max()
    sns.set_theme(style="white", font_scale=1.5)
    _, axes = plt.subplots(2, 1, figsize=(15, 15))
    sc.pl.dotplot(adata[adata.obs["patient"].isin(LUAD_SAMPLES), :], var_names=GENES, groupby="latest_annot", log=False, figsize=(12, 12), 
                  dot_max=1, vmax=vmax_both, ax=axes[0], show=False, swap_axes=False, cmap=sns.color_palette("light:#D95F02", as_cmap=True), title="LUAD")
    sc.pl.dotplot(adata[adata.obs["patient"].isin(LUSC_SAMPLES), :], var_names=GENES, groupby="latest_annot", log=False, figsize=(12, 12),
                  dot_max=1, vmax=vmax_both, ax=axes[1], show=False, swap_axes=False, cmap=sns.color_palette("light:#1B9E77", as_cmap=True), title="LUSC")
    plt.tight_layout()
    plt.savefig("../../../pj_NSCLC/results/scRNA-seq/dots_LUADvsLUSC/dotplots.specLR.pdf", bbox_inches="tight", facecolor="white")
