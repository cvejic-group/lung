#!/usr/bin/env python
# coding: utf-8
import warnings
warnings.simplefilter("ignore")
import sys
import os

import matplotlib
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors

import plotly.express as px
import plotly.offline as py


# PROVIDE A RELEVANT LIST OF SETS OF CELLPHONEDB SAMPLES TO PLOT AND ANALYSE
g1 = [
        "Tumour_CellPhoneDB_original_stratified40_sub30000_30pc",
        "Background_CellPhoneDB_original_stratified40_sub30000_30pc",
        "Healthy_CellPhoneDB_original_all_30pc"
]

dfs_final = []

out_dir = "/nfs/research/gerstung/nelson/lung/scRNA-Seq/PythonPipeline/CellPhoneDB/"
for g in g1:
    mean = pd.read_csv("Results/Cell_types/out/%s/means.txt"%(g), sep='\t')
    pval = pd.read_csv("Results/Cell_types/out/%s/pvalues.txt"%(g), sep='\t')

    # Get the number of cell types
    N_cellTypes = int(np.sqrt(mean.shape[1]-11))

    mean = mean[~(mean["interacting_pair"].str.contains("HLA") | mean["interacting_pair"].str.contains("complex"))]
    mean.reset_index(drop=True, inplace=True)

    pval = pval[~(pval["interacting_pair"].str.contains("HLA") | pval["interacting_pair"].str.contains("complex"))]
    pval.reset_index(drop=True, inplace=True)

    columns = ["interacting_pair"] + mean.columns[-N_cellTypes**2:].tolist()

    df_means = mean[columns]
    df_pvals = pval[columns]

    df_means.sort_values(by="interacting_pair", ascending=True, inplace=True)
    df_means.reset_index(inplace=True, drop=True)

    df_pvals.sort_values(by="interacting_pair", ascending=True, inplace=True)
    df_pvals.reset_index(inplace=True, drop=True)

    keep_idx = list()
    for i in df_pvals["interacting_pair"].tolist():
        tmp = df_pvals[df_pvals["interacting_pair"]==i]
        pvalues = tmp.T[tmp.index.tolist()[0]].tolist()[1:]
        if all(pvalues) > 0.01:
            pass
        else:
            keep_idx.append(tmp.index.tolist()[0])

    df_means = df_means[df_means.index.isin(keep_idx)]
    df_pvals = df_pvals[df_pvals.index.isin(keep_idx)]

    dfs = list()
    for col in df_means.columns[1:]:
        tmp = df_means[["interacting_pair"]]
        tmp["interacting_group"] = col
        tmp["color"] = df_means[col].tolist()
        tmp["size"] = df_pvals[col].tolist()
        dfs.append(tmp)

    df = pd.concat(dfs, axis=0)
    df = df.rename(columns={"interacting_group": "Cell group",
                            "interacting_pair": "L-R pair",
                            "size":"Corrected p-value",
                            "color":"log(1+expression)"})
    corrected = 0.01/len(df) # We will be conservative and apply the Bonferroni correction

    df["log(1+expression)"] = round(np.log1p(df["log(1+expression)"]), 3)
    df = df[df["Corrected p-value"]<=corrected]
    df = df[df["log(1+expression)"]>=1.0]
    df["tissue"] = g
    df["Combination"] = df["L-R pair"]+": "+df["Cell group"]

    dfs_final.append(df)
    df_sorted = df.sort_values(by="log(1+expression)",ascending=False)
    df_sorted.reset_index(drop=True, inplace=True)
    del df_sorted["Combination"]
    df_sorted.to_csv(out_dir+"Results/csvs/{}_filtered_pairs.csv".format(g))

df = pd.concat(dfs_final)
df_tumour = df[df["tissue"].str.contains("Tumour")]
df_other = df[~df["tissue"].str.contains("Tumour")]
combs_tumour, combs_other = df_tumour["Combination"].tolist(), df_other["Combination"].tolist()

combs_diff = [x for x in combs_tumour if x not in combs_other]
df_tumour = df_tumour[df_tumour["Combination"].isin(combs_diff)]
del df_tumour["Combination"]
df_tumour.sort_values(by="log(1+expression)",ascending=False, inplace=True)
df_tumour.reset_index(drop=True, inplace=True)
df_tumour.to_csv("Results/csvs/Tumour_only_Healthy_all_used_{}_filtered_pairs.csv".format("_".join(list(g1)[0].split("_")[1:])))


# # Dotplot
fig = px.scatter(df, x="Cell group", y="L-R pair", color="log(1+expression)",
                    size="Corrected p-value", width=1900, height=800,
                    color_continuous_scale=px.colors.sequential.Aggrnyl,
                    template='plotly_white', marginal_x="histogram", facet_col="tissue", size_max=12)

fig.update_layout(coloraxis_colorbar=dict(
    title=" ",
    len=0.5
))

fig.write_image(out_dir+"Results/plots/CellPhoneDB_Dotplots_Lung_{}_Healthy_all_used.png".format("_".join(list(g1)[0].split("_")[1:])))
