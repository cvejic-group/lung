import scanpy as sc
import libpysal as lps
import esda
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf
import seaborn as sns

inDir = "/nfs/research1/gerstung/nelson/lung/SpatialTranscriptomics/cell2location/AnnData/updated/"
plotDir = "/nfs/research1/gerstung/nelson/lung/SpatialTranscriptomics/cell2location/plots/updated/"
def main(slide="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A_LR_pairs_thresh1p0", single_spot=False):
    adata = sc.read_h5ad(inDir+"{0}.h5ad".format(slide))

    # 1. KNN  weights on the Visium lattice
    points = list(zip(adata.obs["array_row"], adata.obs["array_col"]))
    kd = lps.cg.KDTree(np.array(points))
    weights = lps.weights.KNN(kd, 8) # Try the eight surrounding spots

    # 2. Give weights as a valid contiguity for the downstream statistical analysis
    weights.transform = "r"

    name = "Spatial_Autocorrelations_{0}_single_spot.pdf".format(slide) if single_spot else "Spatial_Autocorrelations_{0}.pdf".format(slide)
    pdf = mpdf.PdfPages(plotDir+name)
    lrs = adata.var["Ligand-Receptor"].unique()
    for lr in lrs:
        layer = "spot" if single_spot else "NN spots"
        adata_t = adata[:, adata.var["Ligand-Receptor"]==lr]
        y = adata_t.layers[layer].flatten()
        ylag = lps.weights.lag_spatial(weights, y)
        np.random.seed(42)
        li = esda.moran.Moran_Local(y, weights, permutations=300)
        gi = esda.moran.Moran(y, weights, permutations=300)
        extra = " (single spot)" if single_spot else ""
        adata.obs["{0} local Moran{1}".format(lr, extra)] = li.Is
        adata.obs["{0} local Moran p-value{1}".format(lr, extra)] = li.p_sim
        adata.obs["{0} local Moran quadrant{1}".format(lr, extra)] = li.q
        adata.obs["{0} global Moran{1}".format(lr, extra)] = gi.I

        # For the spatial plot
        if single_spot:
            adata_t.X = adata_t.layers["spot"]
        adata_t.var.index = adata_t.var["Ligand-Receptor"].values
        with mpl.rc_context({'figure.figsize': [6,7],'axes.facecolor': 'black'}):
            sc.pl.spatial(adata_t,
                        color=[lr],
                        library_id=list(adata_t.uns['spatial'].keys())[0],
                        alpha_img=0,
                        size=1,
                        vmin=0,
                        cmap='magma',
                        show=False,
                        return_fig=True)
            plt.tight_layout()
            pdf.savefig()
            plt.close()

        # Plot relationship between CCI and the local Moran value
        plt.scatter(y, adata.obs["{0} local Moran{1}".format(lr, extra)].values)
        plt.xlabel("CCI")
        plt.ylabel("Local Moran's I")
        plt.title(lr)
        pdf.savefig()
        plt.close()

        # Plot the null and observed L-R coexpression
        summary = list(zip(li.Is, li.p_sim))
        replicants = li.sim.T
        #print(replicants)
        #print(replicants.shape)
        #print("Max: ", np.max(replicants))
        #print("Mean: ", np.mean(replicants))

        sns.kdeplot(gi.sim, shade=True)
        plt.axvline(gi.I, color='r', linestyle='--')
        plt.title(lr)
        plt.xlabel("Global Moran's I")
        pdf.savefig()
        plt.close()

        idx = -1
        for s in summary:
            idx+=1
            if s[1] < 0.01: # Statistically significant p-values
                sns.kdeplot(replicants[idx], shade=True)
                plt.axvline(s[0], color='r', linestyle='--')
                plt.title(lr)
                plt.xlabel("Local Moran's I")
                pdf.savefig()
                plt.close()
    pdf.close()
    adata.write(inDir+"{0}.h5ad".format(slide), compression="gzip")

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-v", "--slide", help="Visium slide", type=str, default="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A_LR_pairs_thresh1p0")
    parser.add_argument("-s", "--single_spot", action="store_true", help="Run on single spot analysis only")

    options = parser.parse_args()
    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
