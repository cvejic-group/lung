import scanpy as sc
import libpysal as lps
import esda
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf
import seaborn as sns

inDir = "/nfs/research1/gerstung/nelson/lung/SpatialTranscriptomics/cell2location/AnnData/updated/"
plotDir = "/nfs/research1/gerstung/nelson/lung/SpatialTranscriptomics/cell2location/plots/updated/"
def main(slide="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A_LR_pairs_thresh1p0", single_spot=False):
    adata = sc.read_h5ad(inDir+"{0}.h5ad".format(slide))

    # Need to pick up the cell2location model outputs
    model = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/annotation_broad/LocationModelLinearDependentWMultiExperiment_8experiments_9clusters_15107locations_2735genes/sp.h5ad"
    model_env = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/environment/LocationModelLinearDependentWMultiExperiment_8experiments_3clusters_15107locations_2735genes/sp.h5ad"

    adata_env = sc.read_h5ad(model_env)
    sample = "_".join(slide.split("_")[:6])
    adata_env_slide = adata_env[adata_env.obs["sample"]==sample, :]

    # Spot factors
    tumour = adata_env_slide.obs["q05_spot_factorsTumour"].values
    bkg = adata_env_slide.obs["q05_spot_factorsBackground"].values
    healthy = adata_env_slide.obs["q05_spot_factorsHealthy"].values
    # Convert to cell type proportions
    tumour_prop = np.array([tumour[i]/(tumour[i]+bkg[i]+healthy[i]) for i in range(len(tumour))])
    bkg_prop = np.array([bkg[i]/(tumour[i]+bkg[i]+healthy[i]) for i in range(len(bkg))])
    healthy_prop = np.array([healthy[i]/(tumour[i]+bkg[i]+healthy[i]) for i in range(len(healthy))])
    name = "Cell_Porportions_{0}_single_spot.pdf".format(slide) if single_spot else "Cell_Proportions_{0}.pdf".format(slide)
    pdf = mpdf.PdfPages(plotDir+name)

    lrs = adata.var["Ligand-Receptor"].unique()
    for lr in lrs:
        print(lr)
        layer = "spot" if single_spot else "NN spots"
        extra = " (single spot)" if single_spot else ""
        adata_t = adata[:, adata.var["Ligand-Receptor"]==lr]
        y = adata_t.layers[layer].flatten()
        y_moran = adata.obs["{0} local Moran{1}".format(lr, extra)].values

        plt.scatter(tumour_prop, y)
        plt.xlabel("Tumour cell proportion")
        plt.ylabel("{0} CCI".format(lr))
        pdf.savefig()
        plt.close()

        plt.scatter(tumour_prop, y_moran)
        plt.xlabel("Tumour cell proportion")
        plt.ylabel("{0} local Moran's I".format(lr))
        pdf.savefig()
        plt.close()

        plt.scatter(bkg_prop, y)
        plt.xlabel("Background cell proportion")
        plt.ylabel("{0} CCI".format(lr))
        pdf.savefig()
        plt.close()

        plt.scatter(bkg_prop, y_moran)
        plt.xlabel("Background cell proportion")
        plt.ylabel("{0} local Moran's I".format(lr))
        pdf.savefig()
        plt.close()

        plt.scatter(healthy_prop, y)
        plt.xlabel("Healthy cell proportion")
        plt.ylabel("{0} CCI".format(lr))
        pdf.savefig()
        plt.close()

        plt.scatter(healthy_prop, y_moran)
        plt.xlabel("Healthy cell proportion")
        plt.ylabel("{0} local Moran's I".format(lr))
        pdf.savefig()
        plt.close()

    pdf.close()

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-v", "--slide", help="Visium slide", type=str, default="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A_LR_pairs_thresh1p0")
    parser.add_argument("-s", "--single_spot", action="store_true", help="Run on single spot analysis only")

    options = parser.parse_args()
    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
