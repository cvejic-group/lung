from os import system as sys

fname = "Lung_BatchCorr_Healthy_Background"
f = open(fname+".sh", 'w')
f.write("cd /nfs/research1/gerstung/nelson/lung/scRNA-Seq/PythonPipeline/2_batchCorrection/\n")
f.write("conda activate minimal_env\n" )
f.write("python run_hvg_pca_harmony.py")
f.close()

# You typically need bigmem allocation to run this job on an LSF
print('bsub -n 1 -q bigmem -R "rusage[mem=2000000]" -M 2000000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
sys('bsub -n 1 -q bigmem -R "rusage[mem=2000000]" -M 2000000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
