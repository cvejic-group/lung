#!/usr/bin/env python
# coding: utf-8
import warnings
warnings.simplefilter("ignore")
import sys
import os

import matplotlib
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors


from config import lung_config

sns.set_style("white")
# AGAIN, WOUKD NEED UPDATING
in_dir = "/nfs/research/gerstung/nelson/data/lung/scRNAseq/outputs/combined/"

def main():
    # # Loading data -- THIS WILL NEED TO BE UPDATED TO REFLECT THE DATA LOCATION IN YOUR DIRECTORY
    inputs = in_dir+ "10X_Lung_Tumour_Annotated_v2.h5ad"
    out_dir = "/nfs/research/gerstung/zaira/lung/scRNA-Seq/PythonPipeline/CellPhoneDB/"
    adata = sc.read_h5ad(inputs)
    annotations = lung_config["Tumour"]["name"]
    to_remove = lung_config["Tumour"]["remove"]
    adata = adata[~adata.obs[annotations].isin(to_remove),:]
    adata.obs["Cancer type"] = adata.obs["patient"].map(lung_config["Cancer_type"])
    adata = adata[~(adata.obs["Cancer type"] == "LC")]

    adata = adata.copy()
    counts = adata.X.toarray()
    adata.X = counts
    sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)
    adata.layers["norm"] = adata.X

    if not os.path.exists(out_dir+"Results/Cell_types/"):
        os.makedirs(out_dir+"Results/Cell_types/")

    # Stratify by patient_sample combination (10 % default)
    for c in adata.obs["Cancer type"].unique(): 
        adatas = list()
        adata_c = adata[adata.obs["Cancer type"] == c]
        for ps in adata_c.obs["patient_sample"].unique():
            for ct in adata_c.obs[annotations].unique():
                adata_t = adata_c[(adata_c.obs["patient_sample"] == ps) & (adata_c.obs[annotations] == ct),:]
                sc.pp.subsample(adata_t, fraction=0.5)
                adatas.append(adata_t)
        adata_c = adatas[0].concatenate(adatas[1:], join="outer")

        counts = pd.DataFrame(data=adata_c.layers["norm"], index=adata_c.obs.index.tolist(), columns=adata_c.var.index.tolist()).T
        counts.index.name = "Gene"
        counts.to_csv(out_dir+f"Results/Cell_types/{c}_counts_original_stratified50.txt", sep="\t")

        meta = pd.DataFrame(data=adata_c.obs[annotations].tolist(), index=adata_c.obs.index.tolist(), columns=["Cell type"])
        meta.index.name = "Cell"
        meta.to_csv(out_dir+f"Results/Cell_types/{c}_meta_original_stratified50.txt", sep="\t")

if __name__=="__main__":
    main()
