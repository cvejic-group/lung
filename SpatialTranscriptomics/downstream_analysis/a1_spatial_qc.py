import os
import sys
import scanpy as sc
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from _functions import select_slide
from _functions import plot_spatial

IN_DIR = "/lustre/scratch126/casm/team-cvejic/nelson/outputs/lung/cell2location/spatial_model/"
ENV_LST = ["Healthy", "Background", "Tumour"]
GENE_COUNT_THRESH = 800
ABD_FIELD = "q05_cell_abundance_w_sf"

OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/STx_results_filtered{GENE_COUNT_THRESH}_q05/"
if not os.path.exists(OUT_DIR):
    print("Creating output folder:", OUT_DIR, file=sys.stderr)
    os.mkdir(OUT_DIR)


def clean_adata(adata2clean, var_qc="total_gene_counts"):
    spot_num = {}
    spot_num["before"] = {sec: sum(adata2clean.obs["sample"].isin([sec]))
                          for sec in adata2clean.obs["sample"].unique()}
    adata2clean = adata2clean[adata2clean.obs[var_qc] >= GENE_COUNT_THRESH, :]
    spot_num["after"] = {sec: sum(adata2clean.obs["sample"].isin([sec]))
                         for sec in adata2clean.obs["sample"].unique()}
    qc_summary = pd.DataFrame.from_dict(spot_num, orient="columns")
    qc_summary["kept percent"] = qc_summary["after"] / qc_summary["before"] * 100
    return adata2clean, qc_summary


def plot_hists(adata2plot, vars=["total_gene_counts", "total_cell_counts"], pdf=None):
    n_subplots = len(vars)
    _, axes = plt.subplots(n_subplots, 1, sharex=False, sharey=False,
                           figsize=(16, n_subplots*5))
    sns.set_theme(font_scale=1.5, style="white")
    for i, var in enumerate(vars):
        ax_twin = axes[i].twinx()
        sns.histplot(adata2plot.obs[var], bins=100, ax=axes[i])
        sns.ecdfplot(adata2plot.obs[var], ax=ax_twin, color="tomato")
        axes[i].set_ylabel("spot count")
    plt.suptitle(f"{adata2plot.obs['sample'].unique()[0]}")
    plt.tight_layout()
    if pdf is not None:
        pdf.savefig()
        plt.close()


if __name__ == "__main__":
    qc_summary_combined = None
    pdf = mpdf.PdfPages(f"{OUT_DIR}qc_plots_persection.pdf")
    for env in ENV_LST:
        adata = sc.read_h5ad(f"{IN_DIR}{env}/sp.h5ad")
        # Estimated cell type abundance by cell2location
        all_cts = adata.uns["mod"]["factor_names"]
        adata.obs[[f"{ct} abd" for ct in all_cts]] = adata.obsm[ABD_FIELD][[f"{ABD_FIELD.replace('_cell', 'cell')}_{ct}"
                                                                            for ct in all_cts]]
        # Expression matrix and QC metrics
        exp_mat = pd.DataFrame.sparse.from_spmatrix(adata.X,
                                                    columns=adata.var.index,
                                                    index=adata.obs.index)
        adata.obs["total_gene_counts"] = exp_mat.sum(axis=1)
        adata.obs["total_cell_counts"] = adata.obsm[ABD_FIELD].sum(axis=1)
        # Clean data
        adata, qc_summary = clean_adata(adata)
        qc_summary["environment"] = env
        qc_summary_combined = pd.concat([qc_summary_combined, qc_summary],
                                        axis=0, ignore_index=True)
        print("shape of adata:", adata.shape, file=sys.stderr)
        print("shape of adata.obs:", adata.obs.shape, file=sys.stderr)
        print("shape of adata.obsm:", adata.obsm[ABD_FIELD].shape, file=sys.stderr)
        adata.write(f"{OUT_DIR}{env}_sp_cleaned.h5ad", compression="gzip")
        # Per section plot
        for sec in adata.obs["sample"].unique():
            adata_sub = select_slide(adata, sec, "sample")
            # QC histogram
            plot_hists(adata_sub, pdf=pdf)
            # Filtered spatial cell type abundancies
            plot_spatial(adata_sub, pdf, color_by=[f"{ct} abd" for ct in all_cts], ncols=4)
    pdf.close()
    qc_summary_combined.sort_values(by="kept percent").to_csv(f"{OUT_DIR}QC_summary.csv")
