import warnings
from numba.core.errors import NumbaDeprecationWarning
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

import pegasus as pg
import matplotlib.pyplot as plt
import seaborn as sns


WK_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/scRNA-seq/combined_foetal_analysis_TandB/"

if __name__ == "__main__":
    # Load data
    data = pg.read_input(f"{WK_DIR}results/a1_combined_adata.h5ad")
    print("Combined data dimension:", data.shape)
    # Processing
    pg.log_norm(data, norm_count=1e4)
    pg.identify_robust_genes(data)
    pg.highly_variable_features(data, batch="sample_batch") 
    data.var.to_csv(f"{WK_DIR}results/a2_tab.var.csv")
    pg.hvfplot(data, top_n=100, panel_size=(9, 5))
    plt.savefig(f"{WK_DIR}figures/a1_HVFplot.pdf", bbox_inches="tight", facecolor="white")
    pg.pca(data, n_components=150, standardize=True, random_state=0)
    pg.elbowplot(data, rep="pca", panel_size=(9, 5))
    plt.savefig(f"{WK_DIR}figures/a2_Elbowplot.pdf", bbox_inches="tight", facecolor="white")
    pg.run_harmony(data, batch="sample_batch", rep="pca", n_comps=data.uns["pca_ncomps"],
                   random_state=0, max_iter_harmony=500)
    print("Harmonsied PCA dimension:", data.obsm["X_pca_harmony"].shape)
    pg.neighbors(data, K=100, rep="pca_harmony", random_state=0, use_cache=False)
    pg.leiden(data, rep="pca_harmony", resolution=None, n_clust=12, random_state=0, class_label="combi_clust")
    pg.write_output(data, f"{WK_DIR}results/a3_combined_adata.harmonised.h5ad", precision=6)
    n_cellgrp = len(data.obs["cell_group"].unique())
    # Composition plots
    # ----- Composition plots grouped by combined clusters
    pg.compo_plot(data, groupby="combi_clust", condition="cell_group", switch_axes=True,
                  panel_size=(9, 5), palette=sns.color_palette("Spectral", n_colors=n_cellgrp),
                  left=0.2, bottom=0.2)
    plt.xticks(rotation=0, ha="center")
    plt.savefig(f"{WK_DIR}figures/a3_compoplot_cellgroup_in_cluster.pdf")
    # ----- Composition plots grouped by cell groups
    data.obs["cell_group"] = data.obs["cell_group"].cat.reorder_categories(["foetal:SPP1_Mac", "tumour:STAB1_Mac", "foetal:Mac",
                                                                            "tumour:AM", "tumour:AIM", "background:AM", "background:AIM",
                                                                            "foetal:DCs", "tumour:DCs", "background:DCs",
                                                                            "foetal:monocytes", "tumour:monocytes", "background:monocytes",
                                                                            "foetal:granulocytes", "tumour:immature", "background:immature", "foetal:HSPCs"])
    pg.compo_plot(data, groupby="cell_group", condition="combi_clust", switch_axes=True,
                  sort_function=["foetal:SPP1_Mac", "tumour:STAB1_Mac", "foetal:Mac",
                                 "tumour:AM", "tumour:AIM", "background:AM", "background:AIM",
                                 "foetal:DCs", "tumour:DCs", "background:DCs",
                                 "foetal:monocytes", "tumour:monocytes", "background:monocytes",
                                 "foetal:granulocytes", "tumour:immature", "background:immature", "foetal:HSPCs"],
                  panel_size=(9, 5), palette=sns.color_palette("Paired"),
                  left=0.2, bottom=0.2)
    plt.xticks(rotation=0, ha="center")
    plt.savefig(f"{WK_DIR}figures/a4_compoplot_cluster_in_cellgroup.pdf")
    # Dendrogram
    pg.dendrogram(data, groupby="cell_type", rep="pca_harmony", panel_size=(9, 7),
                  correlation_method="pearson", affinity="correlation", linkage="complete")
    plt.savefig(f"{WK_DIR}figures/a5_dendrogram.pdf")
    # Compute UMAP
    pg.umap(data, rep="pca_harmony", n_components=2, n_neighbors=15, random_state=0)
    # ----- UMAP per combined clusters
    pg.scatter(data, attrs="combi_clust", basis="umap", panel_size=(7, 7), marker_size=15, show_background=True)
    plt.savefig(f"{WK_DIR}figures/a6_UMAP_scatter_combined-clusters.pdf")
    # ----- UMAP per dataset
    pg.scatter(data, attrs="dataset", basis="umap", panel_size=(7, 7), marker_size=15, show_background=True)
    plt.savefig(f"{WK_DIR}figures/a6-2_UMAP_scatter_datasets.pdf")
    # ----- UMAP per cell type, only for those containing more than 100 cells
    tmp = data.obs["cell_type"].value_counts()
    ctype_lst_above100 = tmp.index[tmp > 100]
    for ct in ctype_lst_above100:
        data.obs[ct] = (data.obs["cell_type"] == ct).astype("category")
    pg.scatter(data, attrs=ctype_lst_above100, basis="umap", marker_size=7, ncols=4, panel_size=(3, 3))
    plt.savefig(f"{WK_DIR}figures/a7_UMAP_scatter_celltype-above100.pdf")
