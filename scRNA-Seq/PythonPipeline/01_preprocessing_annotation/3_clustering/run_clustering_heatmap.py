import warnings
warnings.simplefilter("ignore")

import sys

import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors

# IMPORTANT: CHANGE THIS TO THE CLONED FUNCTIONS DIRECTORY CONTAINING THE scRNA_functions CLASS
sys.path.append("/nfs/research1/gerstung/nelson/downsyndrome/Functions/")

# Get the bespoke analysis functions
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

# Global colour settings
myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', '#DDEFFF', '#000035', '#7B4F4B', '#A1C299', '#300018', '#C2FF99', '#0AA6D8', '#013349',
            '#00846F', '#8CD0FF', '#3B9700', '#04F757', '#C8A1A1', '#1E6E00', '#DFFB71', '#868E7E', '#513A01', '#CCAA35',
            '#800080', '#DAA520', '#1E90FF', '#3CB371', '#9370DB', '#8FBC8F', '#00FF7F', '#0000CD', '#556B2F', '#FF00FF',
            '#CD853F', '#6B8E23', '#008000', '#6495ED', '#00FF00', '#DC143C', '#FFFF00', '#00FFFF', '#FF4500', '#4169E1',
            '#48D1CC', '#191970', '#9ACD32', '#FFA500', '#00FA9A', '#2E8B57', '#40E0D0', '#D2691E', '#66CDAA', '#FFEFD5',
            '#20B2AA', '#FF0000', '#EEE8AA', '#BDB76B', '#E9967A', '#AFEEEE', '#000080', '#FF8C00', '#B22222', '#5F9EA0',
            '#ADFF2F', '#FFE4B5', '#7B68EE', '#7FFFD4', '#0000FF', '#BA55D3', '#90EE90', '#FFDAB9', '#6A5ACD', '#8B0000',
            '#8A2BE2', '#CD5C5C', '#F08080', '#228B22', '#FFD700', '#006400', '#98FB98', '#00CED1', '#00008B', '#9400D3',
            '#9932CC', '#4B0082', '#F0E68C', '#483D8B', '#008B8B', '#8B008B', '#4682B4']
myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']

# Define a nice colour map for gene expression
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Inputs and Outputs
outDir = "/nfs/research1/gerstung/nelson/outputs/lung/3_clustering/"
inputs = '/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr/10X_Lung_Healthy_Background.h5ad'
outputs = '/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Healthy_Background.h5ad'

def main(resolution=1):

    pdf = mpdf.PdfPages(outDir+"Lung_Healthy_Background_clustering.pdf")
    # Loading samples
    adata = sc.read_h5ad(inputs)

    sc.settings.verbosity=3
    print("\n * Computing UMAP")
    sc.tl.umap(adata, random_state=10, n_components=3, init_pos='random')

    # # 1. Clustering
    # Run graphical clustering/connected community detection with the Leiden algorithm
    sc.tl.leiden(adata, resolution=resolution, key_added='leiden')
    adata.obs["leiden"].cat.categories

    # # 2. UMAP plots
    f, axs = plt.subplots(1,3,figsize=(60,16))
    sns.set(font_scale=1.5)
    sns.set_style("white")

    fc.plotUMAP(adata, variable="leiden", palette=myColors, components=["1,2"], ax=axs[0], pdf=pdf)
    fc.plotUMAP(adata, variable="leiden", palette=myColors, components=["1,3"], ax=axs[1], pdf=pdf)
    fc.plotUMAP(adata, variable="leiden", palette=myColors, components=["2,3"], ax=axs[2], pdf=pdf)

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    pdf.savefig()
    plt.close()

    fc.plot3D(adata, group_by="leiden", space="X_umap", palette=myColors, write_to=outDir+"Lung_Healthy_Background_UMAP_Leiden")

    # # 3. Top 100 marker genes
    sc.tl.rank_genes_groups(adata,
                        groupby='leiden',
                        n_genes=100,
                        method='wilcoxon',
                        key_added="rank_genes_groups_leiden",
                        corr_method='bonferroni')


    # Marker genes filtering (in at least 30% of cells)
    sc.tl.filter_rank_genes_groups(adata, # min_fold_change=3,
                               min_in_group_fraction=0.3,
                               min_fold_change=0.0,
                               key="rank_genes_groups_leiden",
                               key_added="rank_genes_groups_leiden_filtered",
                               max_out_group_fraction=1)

    plt.rcParams["figure.figsize"]=16,8
    fc.plotGenesRankGroups(adata, n_genes=20, key="rank_genes_groups_leiden_filtered", pdf=pdf)

    # # 4. Heatmaps
    kwargs = dict()
    kwargs['vmin'] = -3
    kwargs['vmax'] = 3

    # Make and save a gene expression heatmap
    sc.pl.rank_genes_groups_heatmap(adata,
                                n_genes=30,
                                use_raw=False,
                                swap_axes=True,
                                dendrogram=False,
                                show_gene_labels=True,
                                cmap=cm.plasma,
                                figsize=(300,250),
                                save="Lung_Healthy_Background_clustering_Heatmap.png",
                                key="rank_genes_groups_leiden_filtered",
                                **kwargs)


    # Save information on the known marker genes from the Leiden clustering
    # YOU WILL NEED TO SPECIFY WHERE YOU WANT TO SAVE THE MARKER GENES DATA
    file = "/nfs/research1/gerstung/nelson/lung/scRNA-Seq/MetaData/Lung_Healthy_Background_marker_genes_leiden.csv"
    pd.DataFrame(adata.uns["rank_genes_groups_leiden_filtered"]["names"]).to_csv(file)

    # # 5. Save object
    adata.write(outputs, compression='gzip')

    # We can make additional studies of genes using Violin and Ridge plots
    pdf = mpdf.PdfPages(outDir+"Healthy_Background_Violin_Leiden.pdf")
    rpdf = mpdf.PdfPages(outDir+"Healthy_Background_Ridge_Leiden.pdf")

    df = pd.DataFrame(adata.uns["rank_genes_groups_leiden_filtered"]["names"])
    dict_markers = {}
    for col in df.columns:
        list_genes = df[col].tolist()
        list_genes = list(filter(lambda a: a != "nan", list_genes))
        dict_markers[col] = list_genes[:15]

    genes = list()
    for key,value in dict_markers.items():
        for v in value:
            genes.append(v.replace(" ", ""))

    for gene in genes:
        fc.checkMarkerGenesPerCluster(adata, [gene], group_by="leiden")
        fc.ridge_plot(adata, genes=[gene], group_by="leiden", height=0.4, palette=myColors, pdf=rpdf)
        f, axs = plt.subplots(1,2,figsize=(32,8))
        sns.set(font_scale=1.5)
        sns.set_style("white")
        fc.plotViolinVariableGroup(adata, gene, pointSize=0, group_by="leiden", ax=axs[0], use_raw=True, rotation=90, pdf=pdf)
        fc.plotUMAP(adata, variable=gene, colormap=mymap, ax=axs[1], components=["2,3"], use_raw=True, pdf=pdf)
        plt.tight_layout()
        pdf.savefig()
        plt.close("all")

    pdf.close()
    rpdf.close()

    # Call ScanPy to generat a dotplot of relevant genes
    import matplotlib
    matplotlib.rcdefaults()
    matplotlib.rcParams.update({'font.size': 11})
    ax = sc.pl.dotplot(adata,
                   dict_markers,
                   groupby="leiden",
                   standard_scale='var',
                   smallest_dot=0.0,
                   use_raw=True,
                   dot_min=None,
                   dot_max=None,
                   color_map='Reds',
                   dendrogram=False,
                   figsize=(120, 15), # (10, 4)
                   save="Lung_Healthy_Background_Leiden_Ranked_Genes_Dotplot.png",
                   linewidths=2)

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-r", "--resolution", type=float, help="Specify resolution parameter in the Leiden clustering",default=1.0)

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
