import pandas as pd
import numpy as np
import collections
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

sns.set_style("white")

environment = ["Tumour_only", "Tumour", "Background", "Healthy"]
environment = ["Tumour_only"]
out_dir = "/nfs/research/gerstung/nelson/lung/scRNA-Seq/PythonPipeline/CellPhoneDB/Results/"

for env in environment:
    pdf = mpdf.PdfPages(out_dir+"plots/{}_Subsampling_proportions_30pc.pdf".format(env))
    pdf_w = mpdf.PdfPages(out_dir+"plots/{}_Subsampling_weighted_proportions_30pc.pdf".format(env))
    name = "Healthy_CellPhoneDB_original_all_30pc_filtered_pairs.csv" if env=="Healthy" else "{}_CellPhoneDB_original_stratified40_sub30000_30pc_filtered_pairs.csv".format(env)
    if env=="Tumour_only":
        name = "Tumour_only_Healthy_all_used_CellPhoneDB_original_stratified40_sub30000_30pc_filtered_pairs.csv"
    df = pd.read_csv(out_dir+"csvs/{}".format(name))
    groups = [[cg.split("|")[0], cg.split("|")[1]] for cg in df["Cell group"].values]

    types = list(set(list(cg.split("|")[0] for cg in df["Cell group"].values) + list(cg.split("|")[1] for cg in df["Cell group"].values)))

    for t in types:
        print(t)
        groups_c = groups.copy()
        remaining = []
        store_index = []
        index = -1
        for g in groups_c:
            index+=1
            g_c = g.copy()
            if t in g_c:
                g_c.remove(t)
                store_index.append(index)
            else:
                g_c = []
            remaining.append(g_c)
        # Now put the counts together
        remaining_flat = np.array([x for x in remaining if len(x)>0]).flatten()
        remaining_expr = df.loc[store_index]["log(1+expression)"].values
        weighted_counts = [(remaining_flat[i], remaining_expr[i]) for i in range(len(remaining_flat))]
        dict_weighted_counts = {}
        for value in weighted_counts:
            if value[0] in dict_weighted_counts:
                dict_weighted_counts[value[0]] += value[1]
            else:
                dict_weighted_counts[value[0]] = value[1]
        # Simplistic plot based on the unweighted counts (as a percentage)
        counts = collections.Counter(remaining_flat)
        normed = [x*100/sum(counts.values()) for x in list(counts.values())]
        sns.barplot(x=list(counts.keys()), y=normed, color="b")
        plt.xticks(rotation=90)
        plt.xlabel("Cell types")
        plt.ylabel("Interactions [%]")
        plt.title(f"{env} {t}")
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        sns.barplot(x=list(dict_weighted_counts.keys()), y=list(dict_weighted_counts.values()), color="b")
        plt.xticks(rotation=90)
        plt.xlabel("Cell types")
        plt.ylabel("Weighted interactions")
        plt.title(f"{env} {t}")
        plt.tight_layout()
        pdf_w.savefig()
        plt.close()

    pdf.close()
    pdf_w.close()
