import warnings
warnings.simplefilter("ignore")

import sys

import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
#import scrublet as scr
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

import matplotlib
matplotlib.use("Agg")
from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors
from scipy.sparse import issparse
from sklearn.utils import shuffle
from scipy.spatial import distance
from sklearn.preprocessing import normalize
from sklearn.model_selection import train_test_split

sys.path.append("/nfs/research1/gerstung/nelson/downsyndrome/Functions/")
# Get the global settings
#from global_settings import global_settings
#global_settings()

# Get the bespoke analysis functions
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

# Global colour settings
myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', '#DDEFFF', '#000035', '#7B4F4B', '#A1C299', '#300018', '#C2FF99', '#0AA6D8', '#013349',
            '#00846F', '#8CD0FF', '#3B9700', '#04F757', '#C8A1A1', '#1E6E00', '#DFFB71', '#868E7E', '#513A01', '#CCAA35',
            '#800080', '#DAA520', '#1E90FF', '#3CB371', '#9370DB', '#8FBC8F', '#00FF7F', '#0000CD', '#556B2F', '#FF00FF',
            '#CD853F', '#6B8E23', '#008000', '#6495ED', '#00FF00', '#DC143C', '#FFFF00', '#00FFFF', '#FF4500', '#4169E1',
            '#48D1CC', '#191970', '#9ACD32', '#FFA500', '#00FA9A', '#2E8B57', '#40E0D0', '#D2691E', '#66CDAA', '#FFEFD5',
            '#20B2AA', '#FF0000', '#EEE8AA', '#BDB76B', '#E9967A', '#AFEEEE', '#000080', '#FF8C00', '#B22222', '#5F9EA0',
            '#ADFF2F', '#FFE4B5', '#7B68EE', '#7FFFD4', '#0000FF', '#BA55D3', '#90EE90', '#FFDAB9', '#6A5ACD', '#8B0000',
            '#8A2BE2', '#CD5C5C', '#F08080', '#228B22', '#FFD700', '#006400', '#98FB98', '#00CED1', '#00008B', '#9400D3',
            '#9932CC', '#4B0082', '#F0E68C', '#483D8B', '#008B8B', '#8B008B', '#4682B4']
myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']

# Define a nice colour map for gene expression
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Inputs and Outputs
outDir = "/nfs/research1/gerstung/nelson/outputs/lung/3_clustering/new/"
#inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Healthy_Background.h5ad"
#outputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/subtypes/10X_Lung_Healthy_Background_Myeloid_cells.h5ad"
#inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Tumour.h5ad"
#outputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Tumour_v1.h5ad"
#inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Healthy_Background.h5ad"
#outputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/subtypes/10X_Lung_Healthy_Background_Stroma_v1.h5ad"
#inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Healthy_Background.h5ad"
#outputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/subtypes/10X_Lung_Healthy_Background_B_cells.h5ad"

#inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Tumour_split.h5ad"
inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Healthy_Background.h5ad"
#inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Healthy_Background.h5ad"
#inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/10X_Lung_Tumour.h5ad"
#inputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/subtypes/new/10X_Lung_Tumour_T_cells.h5ad"
def main():

    # Loading samples
    adata = sc.read_h5ad(inputs)

    # If we wish to run merging on the cell types
    from merging_dict import merging_dict
    fc.mergeClusters(adata, newName='leiden_new_broad_v1', labels=merging_dict, cluster='leiden_split_2', verbose=True)

    adata = adata[adata.obs["leiden_new_broad_v3"]=="Mast cells", :]
    # If we wish to perform kNN subclustering on the data
    adata = fc.run_subclustering(adata, algorithm="kmeans",restrict_to=("leiden_new_broad_v3",["Mast cells"]), space="X_umap", n_clusters=[10], key_added="leiden_new_mastcells_v1")

    fc.plot3D(adata, group_by="leiden_new_mastcells_v1", space="X_umap", palette=myColors, write_to=outDir+"UMAP_Lung_Healthy_Background_Mast_cells_Leiden_v1")

    # Marker genes and heatmaps
    sc.tl.rank_genes_groups(adata,
                        groupby='leiden_new_mastcells_v1',
                        n_genes=100,
                        method='wilcoxon',
                        key_added="rank_genes_groups_annotations",
                        corr_method='bonferroni')


    # Marker genes filtering (in at least 30% of cells)
    sc.tl.filter_rank_genes_groups(adata, # min_fold_change=3,
                               min_in_group_fraction=0.3,
                               min_fold_change=0.0,
                               key="rank_genes_groups_annotations",
                               key_added="rank_genes_groups_annotations_filtered",
                               max_out_group_fraction=1)

    plt.rcParams["figure.figsize"]=16,8
    fc.plotGenesRankGroups(adata, n_genes=20, key="rank_genes_groups_annotations_filtered", pdf=None)

    kwargs = dict()
    kwargs['vmin'] = -3
    kwargs['vmax'] = 3

    # Make and save a gene expression heatmap
    sc.pl.rank_genes_groups_heatmap(adata,
                                n_genes=30,
                                use_raw=False,
                                swap_axes=True,
                                dendrogram=False,
                                show_gene_labels=True,
                                cmap=cm.plasma,
                                figsize=(300,250),
                                save="Lung_Healthy_Background_Mast_cells_clustering_Heatmap_Leiden_v1.png",
                                #save="Lung_Tumour_Mast_cells_clustering_Heatmap_Leiden_v1.png",
                                #save="Lung_Tumour_Cycling_myeloid_cells_clustering_Heatmap_Leiden_v1.png",
                                #save="Lung_Healthy_Background_clustering_Heatmap_Leiden_Sub_Myeloid_cells_v4.png",
                                key="rank_genes_groups_annotations_filtered",
                                **kwargs)


    # Save information on the known marker genes from the Leiden clustering
    file = "/nfs/research1/gerstung/nelson/lung/scRNA-Seq/MetaData/Lung_Healthy_Background_Mast_cells_marker_genes_leiden_v1.csv"
    pd.DataFrame(adata.uns["rank_genes_groups_annotations_filtered"]["names"]).to_csv(file)
    df = pd.DataFrame(adata.uns["rank_genes_groups_annotations_filtered"]["names"])

    outputs = "/nfs/research1/gerstung/nelson/data/lung/ScanpyObjects/merged_batchcorr_clustered/subtypes/new/10X_Lung_Healthy_Background_Mast_cells.h5ad"
    adata.write(outputs, compression='gzip')

if __name__=="__main__":
    main()
