import os
import sys
import pandas as pd
import scanpy as sc
import matplotlib.backends.backend_pdf as mpdf

from scipy.stats import mannwhitneyu
from statsmodels.stats.multitest import fdrcorrection

from _functions import select_slide
from _functions import plot_boxes

ABD_FIELD = "q05_cell_abundance_w_sf"
ENV_LST = ["Healthy", "Background", "Tumour"]
GENE_COUNT_THRESH = 800

OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/STx_results_filtered{GENE_COUNT_THRESH}_q05/"


def summarise_frequency(adata):
    freq_env = {}
    for sec in adata.obs["sample"].unique():
        adata_sub = select_slide(adata, sec, "sample")
        freq_env[sec] = {ct: adata_sub.obsm[ABD_FIELD][f"{ABD_FIELD.replace('_cell', 'cell')}_{ct}"].sum()
                         for ct in adata_sub.uns["mod"]["factor_names"]}
    freq_env = pd.DataFrame.from_dict(freq_env, orient="index")
    freq_env = freq_env.reset_index().rename(columns={"index": "sample"})
    return freq_env


if __name__ == "__main__":
    # Summarise cell type frequency table for both environments
    freq_allsecs = None
    for env in ENV_LST:
        print(env, file=sys.stderr)
        adata = sc.read_h5ad(f"{OUT_DIR}{env}_sp_cleaned.h5ad")
        freq_env = summarise_frequency(adata)
        freq_env = freq_env.melt(id_vars="sample",
                                 var_name="cell type",
                                 value_name="abundance")
        freq_env["environment"] = env
        freq_allsecs = pd.concat([freq_allsecs, freq_env],
                                 axis=0, ignore_index=True)
    freq_allsecs.to_csv(f"{OUT_DIR}cell_type_frequencies.csv", index=False)
    # Print table size for checking
    print("Cell type frequency table size:", freq_allsecs.shape, file=sys.stderr)
    for env in ENV_LST:
        print("\t-", env, freq_allsecs.loc[freq_allsecs["environment"] == env, :].shape,
              file=sys.stderr)
    # Wilcoxon rank sum test for cell type frequency per spot
    pvals = {}
    for ct in freq_allsecs["cell type"].unique():
        x = freq_allsecs.loc[(freq_allsecs["cell type"] == ct) & (freq_allsecs["environment"] == ENV_LST[0]),
                             "abundance"].tolist()
        y = freq_allsecs.loc[(freq_allsecs["cell type"] == ct) & (freq_allsecs["environment"] == ENV_LST[1]),
                             "abundance"].tolist()
        if x and y:  # to exclude cell types in single environment
            _, pvals[ct] = mannwhitneyu(x, y, method="exact")
    pval_res = pd.DataFrame.from_dict(pvals, orient="index", columns=["p.raw"])
    pval_res["p.adj (FDR)"] = fdrcorrection(pval_res["p.raw"], alpha=0.05,
                                            method="indep", is_sorted=False)[1]
    pval_res.sort_values(by=["p.adj (FDR)", "p.raw"], inplace=True)
    pval_res.to_csv(f"{OUT_DIR}wilcoxon_pvalues_on_celltype_frequency.csv")
    # Plot frequency per spot for each cell type
    pdf = mpdf.PdfPages(f"{OUT_DIR}boxplots_celltype_frequency_by_environment.pdf")
    plot_boxes(df=freq_allsecs, x="abundance", y="cell type",
               y_order=list(pval_res.index) + list(freq_allsecs["cell type"].loc[~freq_allsecs["cell type"].isin(pval_res.index)].unique()),
               groupby="environment", pdf=pdf)
    pdf.close()
