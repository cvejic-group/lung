from visium_qc_and_visualisation import read_and_qc
import numpy as np
import scanpy as sc
from scipy.spatial import cKDTree
from anndata import AnnData
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf
from matplotlib.colors import LogNorm
import seaborn as sns
from sklearn.preprocessing import normalize

sns.set_style("white")
outDir = "/nfs/research1/gerstung/nelson/lung/SpatialTranscriptomics/cell2location/"
model = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/annotation_broad/LocationModelLinearDependentWMultiExperiment_8experiments_9clusters_15107locations_2735genes/sp.h5ad"
model_env = "/nfs/research1/gerstung/nelson/outputs/lung/cell2location/environment/LocationModelLinearDependentWMultiExperiment_8experiments_3clusters_15107locations_2735genes/sp.h5ad"

def main(slide="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A", threshold=0.001):

    threshold_name = str(threshold).replace(".", "p")
    adata_lr = sc.read_h5ad(outDir+"AnnData/updated/"+aslide+"_LR_pairs_thresh{0}.h5ad".format(threshold_name))
    cci_count = adata_lr.X
    cci_count_spot = adata_lr.layers["spot"]
    # Construct the cell type -- LR matrix
    spot_factors = [
                    'q05_spot_factorsB cells', 'q05_spot_factorsCilia cells', 'q05_spot_factorsDendritic cells',
                    'q05_spot_factorsEndothelial cells', 'q05_spot_factorsEpithelial cells', 'q05_spot_factorsMast cells',
                    'q05_spot_factorsMyeloid cells', 'q05_spot_factorsNK cells'
    ]
    adata_cell_types = sc.read_h5ad(model)
    adata_cell_types_t = adata_cell_types[adata_cell_types.obs['sample']==slide, :]
    cell_types = adata_cell_types_t.obs[spot_factors].to_numpy()
    # Normalise the spot factors so that the sum per spot is 1
    cell_types = normalize(cell_types, norm="l1")
    # Cells x LR matrix
    cells_LR = np.matmul(cell_types.T, cci_count)
    cells_LR_raw = np.matmul(cell_types.T, cci_count_raw)

    # Save matrices
    pdf = mpdf.PdfPages(outDir+"matrices/updated/"+"Cell_Type_LR_Matrices_{0}_thresh{1}.pdf".format(slide, threshold_name))

    log_norm = LogNorm(vmin=0.0001, vmax=cells_LR.max().max())
    log_norm_raw = LogNorm(vmin=0.0001, vmax=cells_LR_raw.max().max())

    x_axis_labels = np.array([l.replace("q05_spot_factors", "") for l in spot_factors])
    y_axis_labels = adata_lr.var["Ligand-Receptor"].values

    ax = sns.heatmap(cells_LR.T, norm=log_norm, cmap="RdPu", xticklabels=x_axis_labels, yticklabels=y_axis_labels, cbar_kws={'label': 'Co-expression'})
    plt.xlabel("Cell types")
    plt.ylabel("Ligand-receptor")
    plt.tight_layout()
    pdf.savefig()
    plt.close()

    ax = sns.heatmap(cells_LR_raw.T, norm=log_norm_raw, cmap="RdPu", xticklabels=x_axis_labels, yticklabels=y_axis_labels, cbar_kws={'label': 'Raw co-expression'})
    plt.xlabel("Cell types")
    plt.ylabel("Ligand-receptor")
    plt.tight_layout()
    pdf.savefig()
    plt.close()

    # Do the same for the three environments: tumour, healthy, background
    spot_factors = [
                    'q05_spot_factorsTumour', 'q05_spot_factorsBackground', 'q05_spot_factorsHealthy'
    ]
    adata_cell_env = sc.read_h5ad(model_env)
    adata_cell_env_t = adata_cell_env[adata_cell_env.obs['sample']==slide, :]
    cell_env = adata_cell_env_t.obs[spot_factors].to_numpy()
    # Normalise the spot factors so that the sum per spot is 1
    cell_env = normalize(cell_env, norm="l1")
    # Cells x LR matrix
    cells_LR = np.matmul(cell_env.T, cci_count)
    cells_LR_raw = np.matmul(cell_env.T, cci_count_raw)

    log_norm = LogNorm(vmin=0.0001, vmax=cells_LR.max().max())
    log_norm_raw = LogNorm(vmin=0.0001, vmax=cells_LR_raw.max().max())

    x_axis_labels = np.array([l.replace("q05_spot_factors", "") for l in spot_factors])
    y_axis_labels = adata_lr.var["Ligand-Receptor"].values

    ax = sns.heatmap(cells_LR.T, norm=log_norm, cmap="RdPu", xticklabels=x_axis_labels, yticklabels=y_axis_labels, cbar_kws={'label': 'Co-expression'})
    plt.xlabel("Cell environment")
    plt.ylabel("Ligand-receptor")
    plt.tight_layout()
    pdf.savefig()
    plt.close()

    ax = sns.heatmap(cells_LR_raw.T, norm=log_norm_raw, cmap="RdPu", xticklabels=x_axis_labels, yticklabels=y_axis_labels, cbar_kws={'label': 'Raw co-expression'})
    plt.xlabel("Cell environment")
    plt.ylabel("Ligand-receptor")
    plt.tight_layout()
    pdf.savefig()
    plt.close()

    pdf.close()

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-s", "--slide", help="Visium slide", type=str, default="spaceranger110_count_36209_OTAR_LNGsp9476038_GRCh38-2020-A")
    parser.add_argument("-t", "--threshold", help="Expression threshold", type=float, default=0.001)

    options = parser.parse_args()
    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
