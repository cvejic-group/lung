import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf
sns.set_style("white")

def get_ranks(df, size=50):
    print(df)
    df["Combination"] = df["L-R pair"] + ":" + df["Cell group"]
    return " ".join(df["Combination"].values[:size].tolist())

def cosine_sim_vectors(vec1, vec2):
     vec1 = vec1.reshape(1,-1)
     vec2 = vec2.reshape(1,-1)
     return cosine_similarity(vec1,vec2)[0][0]

# UPDATE THESE TO DIRECTORIES CONTAINING OUTPUT CSVS AND PLOTS
in_dir = "/nfs/research/gerstung/nelson/lung/scRNA-Seq/PythonPipeline/CellPhoneDB/Results/csvs/"
out_dir = "/nfs/research/gerstung/nelson/lung/scRNA-Seq/PythonPipeline/CellPhoneDB/Results/plots/"
def main(reference="stratified20_sub20000", environment="Tumour"):

    data = [
            f"{environment}_CellPhoneDB_original_stratified10_sub15000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified10_sub20000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified10_sub30000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified20_sub15000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified20_sub20000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified20_sub30000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified30_sub15000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified30_sub20000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified30_sub30000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified40_sub15000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified40_sub20000_30pc_filtered_pairs",
            f"{environment}_CellPhoneDB_original_stratified40_sub30000_30pc_filtered_pairs"
    ]

    if environment=="Healthy":
        data.append("Healthy_CellPhoneDB_original_all_30pc_filtered_pairs")

    # Pick up the correct reference and remove it from te initial datq list
    reference = f"{environment}_CellPhoneDB_original_{reference}_30pc_filtered_pairs"
    data.remove(reference)

    df_ref = pd.read_csv(in_dir+reference+".csv")
    ref_ranks = get_ranks(df_ref, size=50)
    dict_summary = dict()
    for d in data:
        df = pd.read_csv(in_dir+d+".csv")
        vectors = CountVectorizer().fit_transform([ref_ranks, get_ranks(df, size=50)]).toarray()
        dict_summary["_".join(d.split("_")[3:5])] = cosine_sim_vectors(vectors[0], vectors[1])

    pdf = mpdf.PdfPages(out_dir+f"{environment}_Subsampling_similarity_30pc_size50_updated.pdf")
    df_summary = pd.DataFrame.from_dict(dict_summary, orient="index",columns=["Cosine similarity"])
    sns.pointplot(x=df_summary.index, y="Cosine similarity", data=df_summary, join=False)
    plt.xticks(rotation=90)
    plt.xlabel("Subsampling strategy")
    plt.ylabel("Cosine similarity")
    plt.title(f"{environment}")
    plt.tight_layout()
    pdf.savefig()
    plt.close()
    pdf.close()

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-e", "--environment", type=str, help="Specifiy data-set and biological conditions",default="Tumour")
    parser.add_argument("-r", "--reference", type=str, help="Specifiy the reference subsampling strategy",default="stratified20_sub20000")

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
