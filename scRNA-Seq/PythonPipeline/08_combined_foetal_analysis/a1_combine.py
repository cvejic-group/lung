import gc
import pandas as pd
import scanpy as sc


DATA_DIR = "/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/"
WK_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/scRNA-seq/combined_foetal_analysis_TandB/"
GROUPBY_DICT = pd.read_csv("celltype_dict.csv", index_col=0)["new_name"].to_dict()


def restructure(adata2restruct):
    # Parse latest clustring results and remove previous versions
    a = [col[12:] for col in adata2restruct.obs.columns if 'Cell types v' in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"Cell types v{str(max_num)}"
    print(f"=====> Using {col_name} as the latest clustering column.")
    adata2restruct.obs["batch"] = adata2restruct.obs["batch"].astype("str")
    adata2restruct.obs.rename(columns={col_name: "latest_annot", "batch": "sample batch"},
                              inplace=True)
    # Only keep relevant columns in observations and variables
    adata2restruct.obs.drop([col
                             for col in adata2restruct.obs.columns
                             if "Cell types" in col],
                            axis=1, inplace=True)
    adata2restruct.obs.drop([col
                             for col in adata2restruct.obs.columns
                             if "leiden" in col],
                            axis=1, inplace=True)
    print(f"Cell number: {adata2restruct.n_obs}")


if __name__ == "__main__":
    # Foetal dataset: data downloaded from https://cellxgene.cziscience.com/collections/2d2e2acd-dade-489f-a2da-6c11aa654028
    print("Loading foetal data")
    foetal_adata = sc.read_h5ad(f"{WK_DIR}local.h5ad") # Foetal lung atlas data
    foetal_adata.X = foetal_adata.raw.X.copy()
    foetal_adata = foetal_adata[foetal_adata.obs["batch"] != "5891STDY9030808", :].copy() # to remove batch with only one cell
    print(foetal_adata.X[foetal_adata.X > 10])
    foetal_adata.var.set_index("gene_symbols", inplace=True)
    foetal_adata.obs = foetal_adata.obs[["batch",
                                         "new_celltype"]].rename(columns={"batch": "sample_batch",
                                                                          "new_celltype": "cell_type"})
    foetal_adata.obs["cell_type"] = "foetal:" + foetal_adata.obs["cell_type"].astype("str")
    print(foetal_adata.obs["cell_type"].value_counts())
    del foetal_adata.uns, foetal_adata.obsm
    gc.collect()

    # Tumour dataset
    print("Loading tumour data")
    t_adata = sc.read_h5ad(f"{DATA_DIR}10X_Lung_Tumour_Annotated_v2.h5ad") # Our data
    restructure(t_adata)
    print(t_adata.X[t_adata.X > 10])
    t_adata.obs = t_adata.obs[["patient_sample",
                               "latest_annot"]].rename(columns={"patient_sample": "sample_batch",
                                                                "latest_annot": "cell_type"})
    t_adata.obs["cell_type"] = "tumour:" + t_adata.obs["cell_type"].astype("str")
    t_adata = t_adata[t_adata.obs["cell_type"].isin(GROUPBY_DICT.keys()), :].copy()
    print(t_adata.obs["cell_type"].value_counts())
    del t_adata.uns, t_adata.obsm
    gc.collect()
    
    # Background dataset from B/H
    print("Loading B/H data first, then select Background only")
    b_adata = sc.read_h5ad(f"{DATA_DIR}10X_Lung_Healthy_Background_Annotated_v2.h5ad") # Our data
    restructure(b_adata)
    print(b_adata.X[b_adata.X > 10])
    b_adata = b_adata[b_adata.obs["environment"] == "Background", :].copy()
    gc.collect()
    b_adata.obs = b_adata.obs[["patient_sample",
                               "latest_annot"]].rename(columns={"patient_sample": "sample_batch",
                                                                "latest_annot": "cell_type"})
    b_adata.obs["cell_type"] = "background:" + b_adata.obs["cell_type"].astype("str")
    b_adata = b_adata[b_adata.obs["cell_type"].isin(GROUPBY_DICT.keys()), :].copy()
    print(b_adata.obs["cell_type"].value_counts())
    del b_adata.uns, b_adata.obsm
    gc.collect()

    # Combine dataset
    combined_adata = t_adata.concatenate([b_adata, foetal_adata], batch_categories=["Tumour", "Background", "Foetal"],
                                         join="inner", index_unique="-", batch_key="dataset")
    combined_adata.obs["sample_batch"] = combined_adata.obs["sample_batch"].astype("category")
    combined_adata.obs["dataset"] = combined_adata.obs["dataset"].astype("category")
    combined_adata.obs["cell_group"] = combined_adata.obs["cell_type"].map(GROUPBY_DICT)
    del t_adata, foetal_adata
    sc.pp.filter_genes(combined_adata, min_cells=10)
    combined_adata.var = combined_adata.var[["n_cells"]]
    combined_adata.raw = combined_adata
    gc.collect()
    combined_adata.write_h5ad(f"{WK_DIR}results/a1_combined_adata.h5ad")
