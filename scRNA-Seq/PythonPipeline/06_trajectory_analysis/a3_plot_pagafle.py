import gc
import sys
import scanpy as sc
import scvelo as scv
import matplotlib.pyplot as plt

IN_DIR = "/lustre/scratch126/casm/team-cvejic/nelson/data/lung/scRNAseq/outputs/"
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/scRNA-seq/trajectory_res/"
CTYPE_DIC = {"myeloids": {"Pro-inflammatory macrophages": "#b15928",
                          "Anti-inflammatory alveolar macrophages": "#1f78b4",
                          "Cycling anti-inflammatory alveolar macrophages": "#a6cee3",
                          "Anti-inflammatory macrophages": "#b2df8a",
                          "Cycling anti-inflammatory macrophages": "#33a02c",
                          "STAB1+ anti-inflammatory macrophages": "#e31a1c",
                          "CAMLs": "#fdbf6f", "mo-DC2": "#ff7f00", "Cycling mo-DC2": "#fdbf6f",
                          "Immature myeloid cells": "#6a3d9a", "Odd immature cells": "#cab2d6"},
             "stroma": {"AT2 cells": "#3E54AC", "Cycling AT2 cells": "#93BFCF",
                        "Fibroblasts": "#FCC2FC",
                        "Transitioning epithelial cells": "#BFDB38",
                        "Ciliated epithelial cells": "#CCD5AE",
                        "Atypical epithelial cells": "#7DB9B6",
                        "Cycling epithelial cells": "#617143"}}


if __name__ == "__main__":
    env = "Tumour"
    for grp, ctype_dict in CTYPE_DIC.items():
        adata_env = sc.read_h5ad(f"{OUT_DIR}{env}_{grp}.fle.h5ad")
        print(adata_env.obs["latest_annot"].unique().tolist(), file=sys.stderr)
        scv.pl.paga(adata_env, basis="fle", color="latest_annot", size=2, title=f"Tumour {grp}", alpha=0.5,
                    node_size_scale=0.5, edge_width_scale=1, threshold=0.05, legend_loc="right", fontoutline=1,
                    pos=None, palette=ctype_dict, show=False, figsize=(6, 6))
        plt.savefig(f"{OUT_DIR}{env}_{grp}.paga.fle2.png",
                    dpi=600, bbox_inches="tight", facecolor="white")
        del adata_env
        gc.collect()
