import os
import gc
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from sklearn.metrics import matthews_corrcoef
from _functions import select_slide

ABD_FIELD = "means_cell_abundance_w_sf"
ENV_LST = ["Healthy", "Background", "LUAD", "LUSC"]
GENE_COUNT_THRESH = 800
OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_NSCLC/results/STx_results_{GENE_COUNT_THRESH}/LRres/"
LR_LST = [("CTLA4", "CD86"), ("LGALS9", "HAVCR2"), ("NECTIN2", "TIGIT"),
          ("NECTIN2", "CD226"), ("CD96", "NECTIN1"), ("PDCD1", "CD274"),
          ("HLA-F", "LILRB1"), ("HLA-F", "LILRB2"), ("FLT1", "VEGFA"),
          ("FLT1", "VEGFB"), ("KDR", "VEGFA"), ("NRP1", "VEGFA"),
          ("NRP1", "VEGFB"), ("NRP2", "VEGFA"), ("EGFR", "EREG"),
          ("EGFR", "AREG"), ("EGFR", "HBEGF"), ("EGFR", "MIF"),
          ("CD40", "CD40LG"), ("CD2", "CD58"), ("CD28", "CD86"),
          ("CCL21", "CCR7"), ("TNFRSF13B", "TNFSF13B"), ("TNFRSF13C", "TNFSF13B")]

if __name__ == "__main__":
    # Make patient dictionary from metadata
    metadata = pd.read_csv("../../Metadata/spatial_metadata.csv")
    patient_dict = metadata.set_index("SpaceRanger")["Patient"]
    # Load MCC results
    mcc = pd.read_csv(f"{OUT_DIR}LRcolocalisation_MCC.csv", index_col=0)
    # Analyse by environment
    for env in ENV_LST:
        print(f"=====>{env}", file=sys.stderr)
        ## Result PDF figure
        pdf = mpdf.PdfPages(f"{OUT_DIR}LRcolocalisation_bysec_{env}.pdf")
        ## Analyse by section
        for sec in sorted(os.listdir(f"{OUT_DIR}obj_by_smp/")):
            if sec.startswith(env):
                patient = patient_dict["_".join(sec.strip(".h5ad").split("_")[1:])]
                print(patient, sec, file=sys.stderr)
                adata_sec = sc.read_h5ad(f"{OUT_DIR}obj_by_smp/{sec}")
                adata_sec.obs.rename(columns={f"{l}_{r} status": f"{patient}:{l}_{r} MCC={round(mcc[f'{l}_{r}'][sec.strip('.h5ad')], 5)}"
                                              for l, r in LR_LST}, inplace=True)
                with mpl.rc_context({"figure.figsize": [5, 5], "axes.facecolor": "white"}):
                    sc.pl.spatial(adata_sec, color=[col for col in adata_sec.obs.columns if "MCC=" in col],
                                  palette={"True_True": "red", "True_False": "black",
                                           "False_True": "black", "False_False": "black"},
                                  library_id=list(adata_sec.uns["spatial"].keys())[0],
                                  size=1.3, ncols=3, img_key="hires")
                    plt.tight_layout()
                pdf.savefig()
                plt.close()
        pdf.close()
